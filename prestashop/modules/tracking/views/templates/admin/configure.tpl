{*
* 2007-2022 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2022 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<html>
<head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">{literal}
        google.charts.load('current', {packages: ['corechart', 'line']});
        google.charts.setOnLoadCallback(drawBasic);
        function drawBasic() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'date');
            data.addColumn('number', 'loading time');
            {/literal}
            {foreach from=$tracking item=v}
            data.addRows([
                ["{$v["DATE"]}", {$v["avg_duration"]}],
            ]);
            {/foreach}
            var options = {
                hAxis: {
                    title: 'date'
                },
                vAxis: {
                    title: 'loading-time(s)'
                }
            };
            var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
            chart.draw(data, options);
        }</script>


</head>

<body>
<div>
    <div id="chart_div">
    </div>


    <div class=" d-flex">
        <div class="col-sm-4">
            <table class="table table-bordered table-sm">
                <thead>
                <tr>
                    <th colspan="2"
                        style="text-align:center;font-weight: 700">{l s ='TEMPS DE CHARGEMENT DES HOOKS' mod='tracking'}</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td style="text-align:center;font-weight: 600;background-color: violet">HOOK</td>
                    <td style="text-align:center;font-weight: 600 ;background-color: pink"> {l s='DUREE DE CHARGEMENT(sec)' mod='tracking'}</td>
                </tr>
                {foreach from=$hooks item=x}
                    <tr>

                        <td class="left"> {$x["hook_name"]}</td>
                        <td class="right"> {$x["avg_duration"]}</td>


                    </tr>
                {/foreach}
                </tbody>
            </table>
        </div>
        <div class="col-sm-4">
            <table class="table table-bordered table-sm">
                <thead>
                <tr>
                    <th colspan="2"
                        style="text-align:center;font-weight: 700">{l s ='TEMPS DE CHARGEMENT DES PAGES' mod='tracking'}</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td style="text-align:center;font-weight: 600;background-color: violet">URL</td>
                    <td style="text-align:center;font-weight: 600 ;background-color: pink"> {l s='DUREE DE CHARGEMENT(sec)' mod='tracking'}</td>
                </tr>
                {foreach from=$listUrl item=v}
                    <tr>

                        <td class="left"> {$v["url"]}</td>
                        <td class="right"> {$v["avg_duration"]}
                            <a href="{$home}&url={$v["url"]}">{l s='en savoir plus' mod='tracking'}</a>


                    </tr>
                {/foreach}

                </tbody>
            </table>

            <a href="{$home}">{l s='temps de chargement général' mod='tracking'}</a>


        </div>
    </div>

</body>
</html>


<style>


    tr th {

        background-color: rgba(217, 221, 146, 0.2);
    }

    tr td {

    }

    .one {

    }

    .right {

        display: flex;
        justify-content: space-between;
        background: pink;
        padding-inline: 10px;
        padding-block: 10px;
    }

    .left {

        background: white;
        padding-inline: 10px;
        padding-block: 10px;
        overflow: auto;
        max-width: 800px !important;
    }

</style>
