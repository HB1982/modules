<?php
/**
 * 2007-2022 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2022 PrestaShop SA
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class Tracking extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'tracking';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'HeleneB';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('tracking');
        $this->description = $this->l('calcule le temps de chargement de chaque hook');

        $this->ps_versions_compliancy = array('min' => '1.5', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        $install = parent::install();
        

        include dirname(__FILE__)."sql/install.php";
        if (_PS_VERSION_ == '1.6.1.23' || _PS_VERSION_ == '1.5.6.3') $this->registerHook('displayFooter');

        $this->registerHook('header');
        $this->registerHook('backOfficeHeader');
        $this->registerHook('displayHeader');

        $this->registerHook('displayFooterAfter');
        $this->registerHook('displayDashboardTop');
        return $install;


    }

    public function uninstall()
    {
        include "sql/uninstall.php";
        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */

        $this->context->smarty->assign('module_dir', $this->_path);


        $urls = $this->getTrackByUrl();
        $trackings = $this->showTrakings();
        $hooks = $this->showHooksByUrl('');


        $home = $this->currentUrl();
        if (isset($_GET['url'])) {
            $hooks = $this->showHooksByUrl($_GET['url']);
        } else {

        }

        if (isset($_GET['url'])) {
            $trackings = $this->showTrakingByUrl($_GET['url']);
        }

        $this->context->smarty->assign('hooks', $hooks);
        $this->context->smarty->assign('tracking', $trackings);
        $this->context->smarty->assign('listUrl', $urls);
        $this->context->smarty->assign('home', $home);
        return $this->context->smarty->fetch($this->local_path . 'views/templates/admin/configure.tpl');


    }


    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be loaded in the BO.
     */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path . 'views/js/back.js');
            $this->context->controller->addCSS($this->_path . 'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path . '/views/js/front.js');
        $this->context->controller->addCSS($this->_path . '/views/css/front.css');
    }

    public function trackStart()
    {
        return microtime(true);
    }

    public function trackStartPage()
    {

        $start = microtime(true);
        Configuration::updateValue('start_track_page', $start);
        $startHook = Configuration::get('start_track_page');


        echo 'debut' . $start;

    }

    public function trackEnd($hook_name = "", $timeStart = 0)
    {

        $endHook = microtime(true);
        $url = $_SERVER['REQUEST_URI'];
        $date = date('Y-m-d H:i:s');
        $duration = $endHook - $timeStart;
        $sql = "INSERT INTO `" . _DB_PREFIX_ . "tracking` (`date`,`url`,`hook_name`,`timeA`,`timeB`,`duration`) VALUES ('$date','$url','$hook_name',$timeStart,$endHook,$duration)";
        if (strpos($url, 'Admin') === false || strpos($url, 'token') === false) {
            Db::getInstance()->execute($sql);
        }
    }


    public function trackEndPage($hook_name = "")
    {

        $end = microtime(true);
        $url = $_SERVER['REQUEST_URI'];
        $start = Configuration::get('start_track_page');
        $date = date('Y-m-d H:i:s');
        $duration = $end - $start;
        echo'duration:'.$duration;
        $sql = "INSERT INTO `" . _DB_PREFIX_ . "tracking` (`date`,`url`,`hook_name`,`timeA`,`timeB`,`duration`) VALUES ('$date','$url','$hook_name',$start,$end,$duration)";
        if (strpos($url, 'Admin') !== false || strpos($url, 'token') !== false) {
        } else {

            $req = Db::getInstance()->execute($sql);
        }

        return $duration;
    }

    public function showTrakings()
    {
        $query = "SELECT DATE,AVG(duration) AS avg_duration FROM `" . _DB_PREFIX_ . 'tracking` where hook_name="" GROUP BY DATE';

        $data = Db::getInstance()->Executes($query);

        return ($data);


    }

    public function getTrackByUrl()
    {
        $query = "SELECT url,AVG(duration) AS avg_duration FROM `" . _DB_PREFIX_ . 'tracking`where hook_name="" GROUP BY URL';

        $url_list = Db::getInstance()->Executes($query);

        return ($url_list);

    }

    public function showTrakingByUrl($url = "")
    {

        $query = "SELECT DATE,AVG(duration) AS avg_duration FROM `" . _DB_PREFIX_ . "tracking` where url='$url' and hook_name='' GROUP BY DATE";

        $data = Db::getInstance()->Executes($query);

        return ($data);
    }

    public function currentUrl()
    {
        $urlCourante = $_SERVER["REQUEST_URI"];
        $urlGet = explode("&url", $urlCourante);
        return $urlGet[0];
    }

    public function showHooksByUrl($url)
    {
        $query = "SELECT hook_name, AVG(duration) AS avg_duration FROM `" . _DB_PREFIX_ . "tracking` where url='$url' group by hook_name, url";
        $data = Db::getInstance()->Executes($query);
        return $data;
    }

    public function hookdisplayHeader()
    {
        $this->trackStartPage();

    }

    public function hookdisplayFooterAfter()
    {
        $this->trackEndPage();
    }


    public function hookdisplayFooter()
    {
      return  $this->trackEndPage();
    }

}








