<?php
require_once dirname(__FILE__) . "/../../modules/tracking/tracking.php";
$version = _PS_VERSION_;


if ($version == '1.5.6.3') {


    class Hook extends HooKcore
    {
        /*
        * module: tracking
        * date: 2022-06-27 13:27:51
        * version: 1.0.0
        */
        public static function exec($hook_name, $hook_args = array(), $id_module = null, $array_return = false, $check_exceptions = true)
        {
            $Tracking = new Tracking();
            $timestart = $Tracking->trackstart();
            $outpout = parent::exec($hook_name, $hook_args, $id_module, $array_return, $check_exceptions);

            $Tracking->trackEnd($hook_name, $timestart);
            return $outpout;
        }
    }
}
if ($version == '1.6.1.23') {
    class Hook extends HooKcore
    {
        /*
    * module: tracking
    * date: 2022-06-27 13:27:51
    * version: 1.0.0
    */
        public static function exec(
            $hook_name,
            $hook_args = array(),
            $id_module = null,
            $array_return = false,
            $check_exceptions = true,
            $use_push = false,
            $id_shop = null
        )
        {

            $Tracking = new Tracking();

            $timestart = $Tracking->trackstart();
            $outpout = parent::exec($hook_name, $hook_args, $id_module, $array_return, $check_exceptions, $use_push, $id_shop);
            $Tracking->trackEnd($hook_name, $timestart);
            return $outpout;
        }
    }
}
if ($version == '1.7.8.6') {
    class Hook extends HooKcore
    {
        public static function exec (
            $hook_name,
            $hook_args = array(),
            $id_module = null,
            $array_return = false,
            $check_exceptions = true,
            $use_push = false,
            $id_shop = null,
            $chain = false
        ) {

            $Tracking = new Tracking();

            $timestart = $Tracking->trackstart();
            $outpout = parent::exec($hook_name, $hook_args, $id_module, $array_return , $check_exceptions , $use_push , $id_shop , $chain );
            $Tracking->trackEnd($hook_name, $timestart);
            return $outpout;
        }
    }}
?>