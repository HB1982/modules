<?php
/**
 * 2007-2022 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2022 PrestaShop SA
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class Gd_export extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'gd_export';
        $this->tab = 'content_management';
        $this->version = '1.7.0';
        $this->author = 'Jeremy';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Export de produit');
        $this->description = $this->l('Exportation de mes produits');

        $this->confirmUninstall = $this->l('');
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('GD_EXPORT_LIVE_MODE', false);

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader');
    }

    public function uninstall()
    {
        Configuration::deleteByName('GD_EXPORT_LIVE_MODE');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitGd_exportModule')) == true) {
            $this->postProcess();
        }

        $clients = $this->getSellers();
        $this->context->smarty->assign('module_dir', $this->_path);
        $this->context->smarty->assign('clients', $clients);


        $output = $this->context->smarty->fetch($this->local_path . 'views/templates/admin/configure.tpl');

        return $output . $this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitGd_exportModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Live mode'),
                        'name' => 'GD_EXPORT_LIVE_MODE',
                        'is_bool' => true,
                        'desc' => $this->l('Use this module in live mode'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-envelope"></i>',
                        'desc' => $this->l('Enter a valid email address'),
                        'name' => 'GD_EXPORT_ACCOUNT_EMAIL',
                        'label' => $this->l('Email'),
                    ),
                    array(
                        'type' => 'password',
                        'name' => 'GD_EXPORT_ACCOUNT_PASSWORD',
                        'label' => $this->l('Password'),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'GD_EXPORT_LIVE_MODE' => Configuration::get('GD_EXPORT_LIVE_MODE', true),
            'GD_EXPORT_ACCOUNT_EMAIL' => Configuration::get('GD_EXPORT_ACCOUNT_EMAIL', 'contact@prestashop.com'),
            'GD_EXPORT_ACCOUNT_PASSWORD' => Configuration::get('GD_EXPORT_ACCOUNT_PASSWORD', null),
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be loaded in the BO.
     */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path . 'views/js/back.js');
            $this->context->controller->addCSS($this->_path . 'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path . '/views/js/front.js');
        $this->context->controller->addCSS($this->_path . '/views/css/front.css');
    }


    public function exportProduct()
    {
        $context = Context::getContext(); //initialise le controleur
        $context->controller = new FrontController();
        $products = new ProductCore();
        $product = $products->getProducts($this->context->language->id, 0, 0, 'id_product', 'asc');
        foreach ($product as $k => $v) {
            $images = $this->exportImg($v['id_product']);
            $product[$k]['images'] = $images;
            $product[$k]['quantity'] = 1;
        }
        return json_encode($product);
    }

/*
    public function ($id_product){
        $stocks = Db::getInstance()->executeS('SELECT * FROM `' . _DB_PREFIX_ . 'stock_available` WHERE `id_product` = ' . (int)$id_product);
        $product = new ProductCore($id_product);
        foreach ($stocks as $stock_available){
            var_dump($stock_available['quantity'] = 0);
        }
    }*/

    public function exportImg($id_product)
    {
        $images = Db::getInstance()->executeS('SELECT * FROM `' . _DB_PREFIX_ . 'image_shop` WHERE `id_product` = ' . (int)$id_product . ' ORDER BY `cover` ASC');

        $product = new ProductCore($id_product);
        $cover = Image::getCover($id_product);

        $link = new Link();//because getImageLInk is not static function
        /*
        $imagePath[] = @$link->getImageLink($image['id_image'] . "-cart_default", $image['id_image']);
        $imagePath[] = @$link->getImageLink($image['id_image'] . "-home_default", $image['id_image']);
        $imagePath[] = @$link->getImageLink($image['id_image'] . "-large_default", $image['id_image']);
        $imagePath[] = @$link->getImageLink($image['id_image'] . "-medium_default", $image['id_image']);
        $imagePath[] = @$link->getImageLink($image['id_image'] . "-small_default", $image['id_image']);
        */

        $imagePath[] = @$link->getImageLink($product->link_rewrite[$this->context->language->id], $cover['id_image']);
        foreach ($images as $image) {
            $imagePath[] = @$link->getImageLink($product->link_rewrite[$this->context->language->id], $image['id_image']);
        }
        return $imagePath;
    }

    /*
        public function generateURLS()
        {
            $urls = Db::GetInstance()->executeS('SELECT `id_seller`, `id_customer`, `date_add` FROM `'. _DB_PREFIX_ .'seller`');
            foreach ($urls as $url) {
                $str = implode("-", $url);
                $encode = base64_encode($str);
                echo $encode;
                die();
            }
            return $urls;
        }
    */
    public function getSellers()
    {
        $clients = Db::GetInstance()->executeS('SELECT * FROM `' . _DB_PREFIX_ . 'seller`');
        foreach ($clients as &$client) {
            $client['token'] = base64_encode($client['id_seller'] . "-" . $client['id_customer'] . "-" . $client['date_add']);
        }
        return $clients;
    }

    public function getURLS()
    {
        $product = new ProductCore();

        $url = $_GET['token'];
        $decode = base64_decode($url);
        $explode = explode("-", $decode);
        $id = Db::GetInstance()->executeS('SELECT * FROM `' . _DB_PREFIX_ . 'seller_product`');
        foreach ($id as $id_product) {
            if ($id_product['id_seller'] == $explode[0]) {
                $id_prod = $id_product['id_product'];
                var_dump($id_prod);
            }
        }
        return $id;
    }
}