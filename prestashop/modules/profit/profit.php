<?php
/**
 * 2007-2022 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2022 PrestaShop SA
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class Profit extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'profit';
        $this->tab = 'analytics_stats';
        $this->version = '1.0.0';
        $this->author = 'HeleneB';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('profit');
        $this->description = $this->l('permettra de calculer le bénéfice brut');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {

        return parent::install() && $this->registerHook('displayDashboardTop');


    }

    public function uninstall()
    {


        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {


        if (empty($_POST['debut']) || empty($_POST['fin'])) {


            $_POST['debut'] = date('Y-m-d');

            $_POST['fin'] = date('Y-m-d');
        }


        $this->context->smarty->assign('module_dir', $this->_path);
        $profits = $this->selectPeriod($_POST['debut'], $_POST['fin']);
        $this->context->smarty->assign('profits', number_format($profits["profits"], 2, ",", ""));
        $this->context->smarty->assign('cost', number_format($profits["cost"], 2, ",", ""));
        $this->context->smarty->assign('totalCA', number_format($profits["totalCA"], 2, ",", ""));
        $this->context->smarty->assign('orders', $profits["totalOrders"]);
        $this->context->smarty->assign('debut', $_POST["debut"]);
        $this->context->smarty->assign('fin', $_POST["fin"]);
        return $this->context->smarty->fetch($this->local_path . 'views/templates/admin/configure.tpl');

        /** return $output.$this->renderForm();**/
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitDatesModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Live mode'),
                        'name' => 'DATES_LIVE_MODE',
                        'is_bool' => true,
                        'desc' => $this->l('Use this module in live mode'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'datetime',
                        'prefix' => '<i class="icon icon-envelope"></i>',
                        'desc' => $this->l('Enter a valid email address'),
                        'name' => 'DATES_DEBUT',
                        'label' => $this->l('datedepart'),
                    ),
                    array(
                        'type' => 'password',
                        'name' => 'DATES_ACCOUNT_PASSWORD',
                        'label' => $this->l('Password'),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    protected function getConfigFormValues()
    {
        return array(
            'DATES_LIVE_MODE' => Configuration::get('DATES_LIVE_MODE', true),
            'DATES_DEBUT' => Configuration::get('DATES_DEBUT', 'contact@prestashop.com'),
            'DATES_ACCOUNT_PASSWORD' => Configuration::get('DATES_ACCOUNT_PASSWORD', null),
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {


    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */


    public function selectPeriod($start, $end)
    {
        $orders = Db::getInstance()->executeS("SELECT *
        FROM   " . _DB_PREFIX_ . "order_detail as OD ," . _DB_PREFIX_ . "orders as O
       WHERE O.date_add  BETWEEN '$start' AND '$end'
        AND O.id_order =OD.id_order AND O.current_state IN (2,3,4,5,9,12,15,16,17,18,19,24,26,29,30,31,33,34,35)");
        $profits = 0;
        $totalCA = 0;
        $cost = 0;


        foreach ($orders as $order) {

            $profitByorder = $order["unit_price_tax_excl"] * $order["product_quantity"] - ($order["product_quantity"] * $order["original_wholesale_price"]);
            $profits += $profitByorder;
            $totalOrders++;

            $CAByOrder = $order["total_price_tax_excl"] ;
            $totalCA += $CAByOrder;

            $costByOrder = $order["original_wholesale_price"] * $order["product_quantity"];
            $cost += $costByOrder;


        }

        return ["profits" => $profits, "cost" => $cost, "totalCA" => $totalCA];
    }


    public function hookdisplayDashboardTop($params)
    {
        $result = $this->selectPeriod(date("Y-m-d", strtotime("-30 days")), date("Y-m-d"));
        $html = '
        <div class="list-orders">
        <p><strong>Bénéfice brut réalisé sur la période: </strong></p>
        <p>' . $result['profits'] . '</p>
        <p><strong>Chiffre d\'affaire réalisé: </strong></p>
        <p> ' . $result['totalCA'] . '€</p>
        <p><strong>Coût Total: </strong></p>
        <p>' . $result['cost'] . '  €</p>
        </div>
        ';
        $url =$_SERVER['REQUEST_URI'] ;
        return $html;}

}


?>