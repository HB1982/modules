{*
* 2007-2022 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2022 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<div class="list-orders">


    <p><strong>Bénéfice brut réalisé sur la période : </strong></p>
    <p>{$profits} €</p>
    <p><strong>Chiffre d'affaire réalisé: </strong></p>
    <p>{$totalCA} €</p>
    <p><strong>Coût Total: </strong></p>
    <p>{$cost} €</p>


</div>
<div class="panel">
    <h3><i class="icon icon-credit-card"></i> {l s='profit' mod='profit'}</h3>
    <form class="defaultForm form-horizontal" method="POST" action="">
        <div class="form-group">
            <label class="control-label col-lg-4" for="debut">Rentrez la date de debut</label>
            <input value='{$debut}' name="debut" type="date" required>
        </div>

        <div class="form-group">
            <label class="control-label col-lg-4">Rentrez la date de fin</label>
            <input value='{$fin}' name="fin" type="date" required>
        </div>
        <input type="submit">
    </form>

</div>


