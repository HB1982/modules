<?php
/**
 * 2007-2022 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2022 PrestaShop SA
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class Gdmetalrefresh extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'gdmetalrefresh';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'gtl digital';
        $this->need_instance = 1;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Digital Metal Refresh');
        $this->description = $this->l('Digital Metal Refresh');

        $this->confirmUninstall = $this->l('');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('GDMETALREFRESH_LIVE_MODE', false);

        include(dirname(__FILE__) . '/sql/install.php');

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('displayTop') &&
            $this->registerHook('displayAdminProductsPriceStepBottom') &&
            $this->registerHook('actionProductUpdate') &&
            $this->registerHook('actionProductSave');

    }

    public function uninstall()
    {
        Configuration::deleteByName('GDMETALREFRESH_LIVE_MODE');
        include(dirname(__FILE__) . '/sql/uninstall.php');

        return parent::uninstall();
    }

    public function getContent()
    {

        if (((bool)Tools::isSubmit('submitGdmetalrefreshModule')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign('url_website', "https://" . $_SERVER['HTTP_HOST']);
        return $this->context->smarty->fetch($this->local_path . 'views/templates/admin/configure.tpl');
    }


    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path . 'views/js/back.js');
            $this->context->controller->addCSS($this->_path . 'views/css/back.css');
        }
    }

    public function hookHeader()
    {
        $lastExecuteCRON = file_get_contents(__DIR__ . "/lastExecuteCron.txt");
        $nextExecutionCRON = strtotime("+15 minutes", $lastExecuteCRON) - strtotime("now");
        $this->context->smarty->assign([
            "nextExcecution" => $nextExecutionCRON
        ]);
        $this->context->controller->addJS($this->_path . '/views/js/front.js');
        $this->context->controller->addCSS($this->_path . '/views/css/front.css');
        return $this->display(__FILE__, 'views/templates/front/banniere.tpl');

    }


    public function hookDisplayAdminProductsPriceStepBottom($params)
    {

        $this->context->smarty->assign([
            "data" => $this->getDatasProduct($params["id_product"]),
            "reference_price_gold" => $this->getCurrentPriceMetal("or"),
            "reference_price_silver" => $this->getCurrentPriceMetal("argent")
        ]);
        return $this->display(__FILE__, 'views/templates/admin/productform.tpl');
    }

    public function hookActionProductSave()
    {
        if (isset($_POST['metalrefresh'])) {
            //update or add data indexation price
            $params['or'] = ($_POST["metalrefresh"]["indexation"] == "or") ? 1 : 0;
            $params['argent'] = ($_POST["metalrefresh"]["indexation"] == "argent") ? 1 : 0;
            $params['product_price'] = $_POST["metalrefresh"]["price_base"];
            $params['reference_price'] = $_POST["metalrefresh"]["price_reference"];
            $params['id_product'] = (int)$_POST["id_product"];
            if (empty($this->getDatasProduct($_POST["id_product"]))) {
                Db::getInstance()->insert('gdmetalrefresh_product', $params);
            } else {
                Db::getInstance()->update('gdmetalrefresh_product', $params, "id_product =" . (int)$_POST['id_product']);
            }
            //update price product
            $this->updateNewPriceProduct($_POST['id_product']);
        }
    }

    public function getDatasProduct($id_product)
    {
        return Db::getInstance()->getRow("select * FROM " . _DB_PREFIX_ . "gdmetalrefresh_product WHERE id_product = " . (int)$id_product);
    }

    public function updateNewPriceProduct($id_product)
    {
        //calcul new price
        $data = $this->getDatasProduct($id_product);
        if (!empty($data)) {
            $metal = ($data["or"]) ? "or" : (($data["argent"]) ? "argent" : 0);
            if ($metal) {
                $base_price = $data["product_price"];
                $MetalPriceReference = $data["reference_price"];
                $currentMetalPrice = $this->getCurrentPriceMetal($metal);
                $newPrice = ($base_price * $currentMetalPrice) / $MetalPriceReference;
                if ($newPrice < $base_price) $newPrice = $base_price;
                //update new price
                $price = (float)number_format($newPrice, 6);
                $sql = "UPDATE `ps_product_shop` SET `price` = " . $price . " WHERE `id_product` = " . (int)$id_product;
                Db::getInstance()->execute($sql);
            }
        }
    }

    public function getCurrentPriceMetal($metal = 0)
    {
        if ($metal == "or") $metal = "XAU";
        if ($metal == "argent") $metal = "XAG";
        if (!$metal) return 0;
        //don't call it every time in dev
        //if (!file_exists(__DIR__ . "/priceMetal.json")) {
            $endpoint = 'latest';
            $access_key = 'yzy10c48vclk06wy2a6ntf26q0e30p8eykl3rl8cy78k9dr9lcvv1c5em757';
            $ch = curl_init('https://www.metals-api.com/api/' . $endpoint . '?access_key=' . $access_key . '&base=EUR');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $json = curl_exec($ch);
            curl_close($ch);
            file_put_contents(__DIR__ . "/priceMetal.json", $json);
        /**} else {
         $json = file_get_contents(__DIR__ . "/priceMetal.json");
        }**/
        return json_decode($json)->rates->$metal;
    }

    public function updatePricesCRON()
    {
        $i = 0;
        $context = Context::getContext();
        $context->controller = new FrontController();
        $products = ProductCore::getProducts($this->context->language->id, 0, 0, "id_product", "asc");
        foreach ($products as $product) {
            if (!$data = $this->getDatasProduct($product['id_product'])) continue;
            if ((int)$data["or"] == 0 && (int)$data['argent'] == 0) continue;
            $this->updateNewPriceProduct($product['id_product']);
            $i++;
        }
        file_put_contents(__DIR__ . "/lastExecuteCron.txt", strtotime('now'));
        echo $i . " updated product prices on " . count($products) . " product(s)";
        return true;
    }

}
