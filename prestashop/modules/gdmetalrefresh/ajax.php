<?php
//updatePrices
require dirname(__FILE__) . '/../../config/config.inc.php';
require_once dirname(__FILE__) . '/../../init.php';
include __DIR__ . '/gdmetalrefresh.php';
if(isset($_GET['product']) && is_int($_GET['product'])) die();
$product = new ProductCore($_GET['product']);
echo number_format($product::getPriceStatic($_GET['product']), 2, ',', '')." €";
