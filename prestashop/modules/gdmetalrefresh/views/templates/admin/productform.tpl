<div class="col-md-12">
    <div class="row">
        <div class="col-md-12">
            <h2>
                Indexation des prix
            </h2>
            <div class="radio form-check form-check-radio">
                <label class="form-check-label"><input type="radio" id="form_step_metalrefresh_argent" name="metalrefresh[indexation]" value="" checked><i class="form-check-round"></i>Ne pas indexer le prix</label><br>
                <label class="form-check-label"><input type="radio" id="form_step_metalrefresh_or" name="metalrefresh[indexation]" value="or" {if $data['or']} checked{/if}><i class="form-check-round"></i>Indexer les prix sur l'or</label><br>
                <label class="form-check-label"><input type="radio" id="form_step_metalrefresh_argent" name="metalrefresh[indexation]" value="argent" {if $data['argent']} checked{/if}><i class="form-check-round"></i>Indexer les prix sur l'argent</label><br>
            </div>
            <div class="col-xl-2 col-lg-3 form-group">
                <label class="form-control-label">Prix de base</label>
                <div class="input-group money-type">
                    <input type="text" id="form_step2_price" name="metalrefresh[price_base]" class="form-control" value="{$data['product_price']}">
                    <div class="input-group-append"><span class="input-group-text"> €</span></div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 form-group referencePrice">
                <label class="form-control-label" data-gold="{if $data['or']} Cours de l'or de référence {else}Cours actuel de l'or{/if}" data-silver="{if $data['argent']} Cours de l'argent de référence {else}Cours actuel de l'argent{/if}"></label>
                <div class="input-group money-type">
                    <input type="text" id="form_step2_price" name="metalrefresh[price_reference]" class="form-control" data-gold="{if $data['or']}{$data['reference_price']}{else}{$reference_price_gold}{/if}" data-silver="{if $data['argent']}{$data['reference_price']}{else}{$reference_price_silver}{/if}" value="">
                    <div class="input-group-append"><span class="input-group-text"> €</span></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function initReferencielPrice(){
        if($('input[name="metalrefresh[indexation]"]:checked').val() == ""){
            $('.referencePrice.form-group').addClass("hide");
        }else{
            $('.referencePrice.form-group').removeClass("hide");
            let data = $('input[name="metalrefresh[indexation]"]:checked').val();
            if(data == "or"){
                $('.referencePrice.form-group label').text($('.referencePrice.form-group label').data("gold"));
                $('.referencePrice.form-group input').val($('.referencePrice.form-group input').data("gold"));
            }
            if(data == "argent"){
                $('.referencePrice.form-group label').text($('.referencePrice.form-group label').data("silver"));
                $('.referencePrice.form-group input').val($('.referencePrice.form-group input').data("silver"));
            }
        }
    }
    initReferencielPrice();
    $('input[ name="metalrefresh[indexation]"]').on('change', function () {
        initReferencielPrice();
    });
</script>