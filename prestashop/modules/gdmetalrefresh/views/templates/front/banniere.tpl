
{if $nextExcecution < 15*60+3}
<section class="metalrefresh-alert">
    <p>les prix sont garanties durant <span id="nextExecution"></span> <span id="metalRefreshUnitee"></span></p>
</section>
<script>
    let timeleft = {$nextExcecution} + 3;
        setInterval(function () {
            timeleft = timeleft - 1;
            if (timeleft < 0) {
                timeleft = 15 * 60 + 3;
                //prices update
                if ($("input#product_page_product_id").attr("value")) {
                    $.ajax({
                        url: "/modules/gdmetalrefresh/ajax.php?product=" + ($("input#product_page_product_id").attr("value")),
                    }).done(function (data) {
                        $(".current-price-value").text(data);
                    });
                }
                $.post("/module/ps_shoppingcart/ajax").then(function (resp) {
                    var html = $('<div />').append($.parseHTML(resp.preview));
                    $('.blockcart').replaceWith($(resp.preview).find('.blockcart'));
                });
                /**$.ajax({
                url: "/panier?action=show",
            }).done(function (data) {
                $('.cart-grid.row').replaceWith($(data).find('.cart-grid.row'));
            });**/
                if ($('.cart-grid.row').length) location.reload();

            }
            if (timeleft > 59) {
                $('#metalRefreshUnitee').text("minute(s)");
                $("#nextExecution").text(Math.ceil(timeleft / 60));
            }
            if (timeleft < 60) {
                $('#metalRefreshUnitee').text("seconde(s)");
                if (timeleft < 60) $("#nextExecution").text(timeleft);
            }
        }, 1000);
</script>
{/if}