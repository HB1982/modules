<?php
/**
 *
 * OVERRIDE FrontControllerCore
 */
class FrontController extends FrontControllerCore
{
    public static function getCategoryName($id)
    {
        $category = new Category($id, Context::getContext()->language->id);
        return $category->name;
    }
}