<!doctype html>
<html lang="en">
<head>
    <link href="style.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx"
          crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,200;1,200&display=swap" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa"
            crossorigin="anonymous"></script>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<?php
include 'Allowances.php';
error_reporting(E_ALL);
ini_set("display_errors", 1);
session_start();
?>

<a href="index.php">ACCUEIL</a>
<form class='mail' method="post" action="">
    <label>Inviter un utilisateur</label><br><br>
    <input class="email" type="text" placeholder="exemple@mail.com" name="mail">
    <input class='submit' type="submit">
</form>
<?php


if (empty($_POST['mail'])) {
    $_POST['mail'] = "";
}
$retour = mail($_POST['mail'], 'Envoi depuis la page Contact', $message, $headers);
if ($retour) {
    echo '<p>Votre message a bien été envoyé.</p>';

}
?>