<!doctype html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>

    <link href="style.css" rel="stylesheet">
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body >


<?php
include 'Allowances.php';
error_reporting(E_ALL);
ini_set("display_errors", 1);
session_start();
$myAllowance = new Allowances();
$users = $myAllowance->getAllUsers();

if (!isset($_GET['date'])) $_GET['date'] =
$_GET['date'] = date('Y-m');


$timemin = strtotime(date('Y-m-d', strtotime('first day of '.$_GET['date'])));


$timemax=strtotime(date('Y-m-d', strtotime('last day of '.$_GET['date']))) ;

?>
<nav class="navbar navbar-expand-md  navbar-dark">
    <a class="navbar-brand" href="#">I.K</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="index.php">Accueil</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="quickstart.php">Voir mes déplacements professionnels</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="myEvents.php">Calculez mes indemnités kilométriques</a>
            </li>
        </ul>
    </div>
</nav>

<?php
if (($_SESSION['name']) == 'helenebeury82@gmail.com') {
    ?>
    <a class="admin" href="users.php">Accès administrateur</a>
    <a class="admin" href="send.php">Inviter un utilisateur</a>
    <h2>Période du <?= date('d-m-y', strtotime('first day of ' . $_GET['date'])) ?>
        au <?= date('d-m-y', strtotime('last day of ' . $_GET['date'])) ?> </h2>    <?php
}
?>

<section class="container">
    <a href="users.php?date=<?= date('Y-m-d',strtotime('-1 month',strtotime($_GET['date']))) ?>&user=<?= $_GET['user'] ?>"
       title="C'est GET">Mois précédent</a>
    <a href="users.php?date=<?=date('Y-m-d',strtotime('+1 month',strtotime($_GET['date']))) ?>&user=<?= $_GET['user'] ?>"
       title="C'est GET">Mois suivant</a>
<table class="calendar">
    <thead>
    <tr>
        <th colspan="2">Liste des utilisateurs</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>User</td>

    </tr>
<?php
foreach ($users as $user) {

    ?>
    <tr>
        <td><a href="users.php?user=<?= $user['user'] ?>">voir les indemnités kilométriques</a><?= $user['user'] ?></td>

    </tr>

<?php }
if(empty($_GET['user'])){
    echo'Merci de sélectionner un utilisateur';
}
else {
if ($_SESSION["name"] = 'helenebeury82@gmail.com') {

    $UserRoutes = $myAllowance->getRouteByUser($_GET['user'], date('c', $timemin), date("c",$timemax));
    ?>
    <table class="calendar">
        <thead>
        <tr>
            <th colspan="5">trajets du mois</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>User</td>
            <td>Date</td>
            <td>Evenement</td>
            <td>Distance A/R</td>
        </tr>
        <?php

        $totalUser = 0;
        foreach ($UserRoutes as $route) {

            $totalUser += $route['distance'];
            $allowances = round($totalUser * 0.575, 2);
            ?>
            <tr>
                <td><?= $route['user'] ?></td>
                <td><?= $route['date'] ?></td>
                <td><?= $route['event'] . ':' . $route['city'] ?></td>
                <td><?= $route['distance'] ?> km</td>

            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
    <br>
    <a href="https://www.urssaf.fr/portail/home/taux-et-baremes/indemnites-kilometriques/voiture.html">tableau
        URSSAF</a>
    <p class="distance"> Distance parcourue: <?= (empty($totalUser) ? "0" : $totalUser) ?>km</p><br>
    <p class="ik"> Indemnités kilométriques: <?= (empty($allowances) ? "0" : $allowances) ?>€</p><br>
</section>
</body>
</html>


<?php
} else {
    echo 'pas de rendez vous';
}};