<?php
include 'Allowances.php';
require_once __DIR__ . '/vendor/autoload.php';
error_reporting(E_ALL);
ini_set("display_errors", 1);
session_start(); ?>

<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="style.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS only -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,200;1,200&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>

    <title>Document</title>
    </head>


    <body>
    <section class="'container">
    <nav class="navbar navbar-expand-md  navbar-dark">
        <a class="navbar-brand" href="#">I.K</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="index.php">Accueil</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="quickstart.php">Voir mes déplacements professionnels</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="myEvents.php">Calculez mes indemnités kilométriques</a>
                </li>

            </ul>
        </div>
    </nav>

    <?php
    if (($_SESSION['name']) == 'helenebeury82@gmail.com') {
        ?>
        <a class="admin" href="users.php">Accès administrateur</a>
        <a class="admin" href="send.php">Inviter un utilisateur</a>
        <?php
    }
    ?>


<h1 style="text-align: center">Calculez vos indemnités kilométriques depuis votre agenda</h1>
<div class="index">
    <div>
   <p> Etape 1 : Déclarer vos déplacements professionnels</p>
        <a href="quickstart.php"><img src="img/data.png" style="width:300px;height: 300px" alt="rdv"></a>
    </div>
    <div>
        <p>Etape 2 : Calculer mes indemnités kilométriques</p>

        <a href="myEvents.php"><img src="img/ik.jpg" style="width:300px;height: 300px;border-radius:100%;" alt="ik"></a>
    </div>
</div>
    </section>
</body>
</html>