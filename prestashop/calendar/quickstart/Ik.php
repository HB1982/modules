<?php
use Google\Client;
use Google\Service\Calendar;
class Ik
{

    public function getClient($auth = "")
    {
        $client = new Client();
        $client->addScope(Google_Service_Calendar::CALENDAR);
        $client->addScope(Google_Service_Oauth2::USERINFO_PROFILE);
        $client->addScope(Google_Service_Oauth2::USERINFO_EMAIL);
        $client->setApplicationName('Google Calendar API PHP Quickstart');
        $client->setScopes('https://www.googleapis.com/auth/calendar.events.readonly');
        $client->setAuthConfig('credentials.json');
        $client->setAccessType('offline');
        $client->setPrompt('select_account consent');
        $tokenPath = 'token.json';
        if (file_exists($tokenPath)) {
            $accessToken = $auth;
            $client->setAccessToken($accessToken);
        }
        if ($client->isAccessTokenExpired()) {
            // Refresh the token if possible, else fetch a new one.
            if ($client->getRefreshToken()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            } else {
                // Request authorization from the user.
                $authUrl = $client->createAuthUrl();
                printf("Open the following link in your browser:\n%s\n", $authUrl);
                print 'Enter verification code: ';
                $authCode = trim(fgets(STDIN));

                // Exchange authorization code for an access token.
                $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
                $client->setAccessToken($accessToken);

                // Check to see if there was an error.
                if (array_key_exists('error', $accessToken)) {
                    throw new Exception(join(', ', $accessToken));
                }
            }
            // Save the token to a file.
            if (!file_exists(dirname($tokenPath))) {
                mkdir(dirname($tokenPath), 0700, true);
            }
            file_put_contents($tokenPath, json_encode($client->getAccessToken()));
        }
        return $client;
    }



    public function ifLogout()
    {
        $client = new Google_Client();
        if (isset($_GET['logout'])) {
            $client->revokeToken($_SESSION['access_token']);
            unset($_SESSION);
            unset($_COOKIE);
            header('Location: /calendar/quickstart/quickstart.php');
        }
    }

    public function ckeckPost()
    {
        if (isset($_POST) && !empty($_POST)) {
            $headers = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $message = '

            <h1>' . $_SESSION['name'] . ' a validé ses déplacements professionnels pour la période du ' . date('d-m-y', strtotime('first day of ' . $_GET['date'])) . ' au ' . date('d-m-y', strtotime('last day of ' . $_GET['date'])) . ' </h1>
          ';
            $retour = mail('helenebeury82@gmail.com', 'Envoi depuis la page Contact', $message, $headers);
            if ($retour) {

            }
        }

    }





}




