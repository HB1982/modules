{**
* @package   Powerful Product contact
* @author    Cyril Nicodème <contact@prestaddons.net>
* @copyright Copyright (C) June 2014 cnicodeme.com <@email:contact@prestaddons.net>. All rights reserved.
* @since     2014-09-19
* @version   1.9.0
* @license   Nicodème Cyril
*}

{if isset($successSent)}
	{if isset($successMessage) && !empty($successMessage)}
	<div class="alert alert-info">{$successMessage|escape:'quotes':'UTF-8'}</div>
	{else}
	<p class="alert alert-info">{l s='Your message has been successfully sent to our team.' mod='powerfulproductcontact'}</p>
	{/if}
{else}

	<form action="{$request_uri|escape:'quotes':'UTF-8'}" method="post" class="std ppc-forms" enctype="multipart/form-data" id="contact">
		<fieldset>
			<h3>{$title|escape:'quotes':'UTF-8'}</h3>

			{include file="$tpl_dir./errors.tpl"}

			{if isset($message)}
			{$message|escape:'quotes':'UTF-8'}
			{/if}

			{foreach from=$fields item=field}
			{if $field.type == 'recaptcha'}
			<p class="recpatcha">
				{$field.element|escape:'quotes':'UTF-8'}
			</p>
			{elseif $field.type != 'hidden'}
			<p class="{$field.type|escape:'html':'UTF-8'}">
				<label for="{$field.id|escape:'quotes':'UTF-8'}">{$field.label|escape:'quotes':'UTF-8'}{if $field.required} <em class="required">*</em>{/if}</label>
				{$field.element|escape:'quotes':'UTF-8'}
			</p>
			{else}
			{$field.element|escape:'quotes':'UTF-8'}
			{/if}
			{/foreach}
			<p class="submit">
				<input type="submit" name="submitMessage" id="submitMessage" value="{l s='Send' mod='powerfulproductcontact'}" {if $isv16}class="button btn btn-default button-medium" style="padding: 10px 20px"{else}class="button_large"{/if} />
			</p>
		</fieldset>
	</form>
{/if}
