{**
* @package   Powerful Product contact
* @author    Cyril Nicodème <contact@prestaddons.net>
* @copyright Copyright (C) June 2014 cnicodeme.com <@email:contact@prestaddons.net>. All rights reserved.
* @since     2014-09-19
* @version   1.9.0
* @license   Nicodème Cyril
*}

{if isset($successSent)}
	{if isset($successMessage) && !empty($successMessage)}
	<div class="alert alert-info">{$successMessage nofilter}{* HTML CONTENT *}</div>
	{else}
	<p class="alert alert-info">{l s='Your message has been successfully sent to our team.' d='Modules.powerfulproductcontact' mod='powerfulproductcontact'}</p>
	{/if}
{else}
	<form action="{$request_uri nofilter}{* HTML CONTENT *}" method="post" class="ppc-forms" enctype="multipart/form-data">
		<fieldset>
			<h3>{$title|escape:'quotes':'UTF-8'}</h3>

			{if isset($errors) && $errors}
				<div class="alert alert-danger">
					<p>{if $errors|@count > 1}{l s='There are %d errors' sprintf=array($errors|@count) d='Modules.powerfulproductcontact' mod='powerfulproductcontact'}{else}{l s='There is %d error' sprintf=array($errors|@count) d='Modules.powerfulproductcontact' mod='powerfulproductcontact'}{/if}</p>
					<ol>
						{foreach from=$errors key=k item=error}<li>{$error}</li>{/foreach}
					</ol>
				</div>
			{/if}

			{if isset($message)}
				{$message nofilter}{* HTML CONTENT *}
			{/if}

			{foreach from=$fields item=field}
				{if $field.type == 'recaptcha'}
					<div class="form-group row {$field.type|escape:'html':'UTF-8'}">
						<label for="{$field.id|escape:'htmlall':'UTF-8'}" class="col-md-3 form-control-label">Captcha <em class="required">*</em></label>
						<div class="col-md-9">
							{$field.element nofilter}{* HTML CONTENT *}
						</div>
					</div>
				{elseif $field.type != 'hidden'}
					<div class="form-group row {$field.type|escape:'html':'UTF-8'}">
						<label for="{$field.id|escape:'htmlall':'UTF-8'}" class="col-md-3 form-control-label">{$field.label|escape:'htmlall':'UTF-8'}{if $field.required} <em class="required">*</em>{/if}</label>
						<div class="col-md-9">
							{$field.element nofilter}{* HTML CONTENT *}
						</div>
					</div>
				{else}
					{$field.element nofilter}{* HTML CONTENT *}
				{/if}
			{/foreach}
			<div class="submit row">
				<input type="submit" name="submitMessage" class="btn btn-primary" value="{l s='Send' d='Modules.powerfulproductcontact' mod='powerfulproductcontact'}" />
			</div>
		</fieldset>
	</form>
{/if}
