{**
 * @package   Powerful Product contact
 * @author    Cyril Nicodème <contact@prestaddons.net>
 * @copyright Copyright (C) June 2014 cnicodeme.com <@email:contact@prestaddons.net>. All rights reserved.
 * @since     2014-09-19
 * @version   1.9.0
 * @license   Nicodème Cyril
 *}

{if isset($warning)}
<div class="alert alert-warning">{$warning|escape:'html':'UTF-8'}</div>
{/if}

<ul class="nav nav-tabs" role="tablist" id="ppc-tabs-list">
  <li{if $active == 'settings'} class="active"{/if}><a href="{$settingsUrl|escape:'quotes':'UTF-8'}" title="{l s='See and edit the settings' mod='powerfulproductcontact'}">{l s='Settings' mod='powerfulproductcontact'}</a></li>
  <li{if $active == 'fields'} class="active"{/if}><a href="{$fieldsUrl|escape:'quotes':'UTF-8'}" title="{l s='Manage the displayed fields' mod='powerfulproductcontact'}">{l s='Fields' mod='powerfulproductcontact'}</a></li>
</ul>
