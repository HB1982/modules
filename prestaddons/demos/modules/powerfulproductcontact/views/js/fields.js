/**
 * @package   modules
 * @author    Cyril Nicodème <contact@prestaddons.net>
 * @copyright Copyright (C) June 2014 cnicodeme.com <@email:contact@prestaddons.net>. All rights reserved.
 * @since     2014-09-19
 * @version   1.9.0
 * @license   Nicodème Cyril
 */

jQuery(function ($) {
	$('#ppc-field-select-types').on('change', function () {
		var val = $(this).val();

		$('.field-select-items').hide();
		if (val === 'checkbox' || val === 'radio' || val === 'select' || val === 'file') {
			$('.field-select-items').parent().find('.field-' + val).show();
			$('.ppc-fields-values').prop('disabled', false);
		} else {
			$('.ppc-fields-values').prop('disabled', true);
		}

	}).trigger('change');
});
