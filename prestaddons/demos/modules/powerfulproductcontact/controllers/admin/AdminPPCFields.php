<?php
/**
 * Powerful Product Contact
 *
 * Let your customers contact you for any questions by filling a form on any product page you want.
 *
 * If you find errors, bugs or if you want to share some improvments, feel free to contact at contact@prestaddons.net ! :)
 * Si vous trouvez des erreurs, des bugs ou si vous souhaitez tout simplement partager un conseil ou une amélioration,
 * n'hésitez pas à me contacter à contact@prestaddons.net
 *
 * @author    Cyril Nicodème <contact@prestaddons.net>
 * @copyright Copyright (C) June 2014 cnicodeme.com <@email:contact@prestaddons.net>. All rights reserved.
 * @license   Nicodème Cyril
 * @package   modules
 * @version   1.9.0
 * @since     2014-09-19
 */

include_once(dirname(__FILE__).'/../../classes/models/PPCFieldModel.php');
class AdminPPCFieldsController extends AdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = 'ppcontacts_fields';
        $this->className = 'PPCFieldModel';

        $this->identifier = 'id_ppcontacts_fields';
        $this->position_identifier = 'id_ppcontacts_fields';
        $this->toolbar_title = $this->_trans('Configure Powerful Product Contact module : Fields');

        $this->lang = true;
        $this->addRowAction('edit');
        $this->addRowAction('delete');
        $this->bulk_actions = array('delete' => array('text' => $this->_trans('Delete selected'), 'confirm' => $this->_trans('Delete selected items?')));

        $this->_defaultOrderBy = 'position';

        $this->fields_list = array(
            'id_ppcontacts_fields'  => array('title' => $this->_trans('ID'), 'align' => 'center', 'width' => 30, 'search' => false, 'orderby' => false),
            'label'     => array('title' => $this->_trans('Label'),'width' => 'auto', 'align' => 'left', 'orderby' => false, 'search' => false),
            'name'      => array('title' => $this->_trans('Name'), 'align' => 'left', 'width' => 'auto', 'orderby' => false, 'search' => false),
            'type'      => array('title' => $this->_trans('Type'), 'align' => 'center', 'width' => 'auto', 'orderby' => false, 'search' => false, 'callback' => 'ucfirstTypes'),
            'required'  => array('title' => $this->_trans('Required'),'width' => 40, 'activeVisu' => 'www', 'align' => 'center', 'type' => 'bool', 'orderby' => false, 'search' => false),
            'position'  => array('title' => $this->_trans('Position'), 'align' => 'center', 'position' => 'position', 'filter_key' => 'position', 'search' => false),
        );

        parent::__construct();

        $this->token = Tools::getAdminTokenLite('AdminModules');
        self::$currentIndex = self::$currentIndex.'&configure=awesomeproductcontact';

        $this->tabAccess = array (
            'view'   => '1',
            'add'    => '1',
            'edit'   => '1',
            'delete' => '1'
        );
    }


    /**
     * Used for backward compatibility between 1.7 Prestashop version and older
     */
    private function _trans($string, $class = null, $addslashes = false, $htmlentities = true)
    {
        if ($class === null || $class == 'AdminTab') {
            $class = Tools::substr(get_class($this), 0, -10);
        } elseif (Tools::strtolower(Tools::substr($class, -10)) == 'controller') {
            /* classname has changed, from AdminXXX to AdminXXXController, so we remove 10 characters and we keep same keys */
            $class = Tools::substr($class, 0, -10);
        }

        if (version_compare(Tools::substr(_PS_VERSION_, 0, 7), '1.7.0.3', '>=')) {
            return Translate::getAdminTranslation($string, $class, $addslashes, $htmlentities);
        } else {
            return $this->l($string, $class, $addslashes, $htmlentities);
        }
    }

    public function ajaxProcessUpdatePositions()
    {
        if (Tools::getIsset('ppcontacts_fields')) {
            $positions = Tools::getValue('ppcontacts_fields');
            foreach ($positions as $key => $value) {
                $pos = explode('_', $value);
                if (isset($pos[2])) {
                    $obj = new PPCFieldModel((int) $pos[2]);
                    if (!$obj->updateNewPosition(((int) $key + 1))) {
                        die('{"hasError": true, errors: "Cannot update position"}');
                    }
                }
            }

            die;
        }
    }

    public function setMedia($isNewTheme = false)
    {
        if (version_compare(Tools::substr(_PS_VERSION_, 0, 3), '1.7', '>=')) {
            parent::setMedia($isNewTheme);
        }
        else {
            parent::setMedia();
        }
        $this->context->controller->addJS(__PS_BASE_URI__.'/modules/powerfulproductcontact/views/js/fields.js');
    }

    public function ucfirstTypes($value, $model)
    {
        if (empty($value)) {
            return null;
        }

        return Tools::ucfirst($value);
    }

    public function initBreadcrumbs($tab_id = null, $tabs = null)
    {
        parent::initBreadcrumbs($tab_id, $tabs);
        $this->breadcrumbs = array($this->_trans('Fields'));
    }

    public function renderList()
    {
        $this->context->smarty->assign(array (
            'active' => 'fields',
            'settingsUrl' => Context::getContext()->link->getAdminLink('AdminModules', true).'&configure=powerfulproductcontact',
            'fieldsUrl' => Context::getContext()->link->getAdminLink('AdminPPCFields', true)
        ));

        $output = $this->context->smarty->fetch(dirname(__FILE__).'/../../views/templates/admin/header.tpl');
        $output .= parent::renderList();

        return $output;
    }

    public function renderForm($token = null)
    {
        if (!($obj = $this->loadObject(true))) {
            return;
        }

        if (Validate::isLoadedObject($this->object)) {
            $this->display = 'edit';
            $title = $this->_trans('Update a field');
        } else {
            $this->display = 'add';
            $title = $this->_trans('Add a new field');
        }

        $context = Context::getContext();
        $context->controller->addJS(array(
            _PS_JS_DIR_.'tiny_mce/tiny_mce.js',
            _PS_JS_DIR_.'tinymce.inc.js'
        ));


        $field_types = array (
            array ('value' => 'text',        'name' => $this->_trans('Text')),
            array ('value' => 'email',       'name' => $this->_trans('Customer email')),
            array ('value' => 'attributes',  'name' => $this->_trans('Product attributes list')),
            array ('value' => 'url',         'name' => $this->_trans('URL')),
            array ('value' => 'textarea',    'name' => $this->_trans('Textarea')),
            array ('value' => 'select',      'name' => $this->_trans('Select')),
            array ('value' => 'radio',       'name' => $this->_trans('Radio')),
            array ('value' => 'checkbox',    'name' => $this->_trans('Checkbox')),
        );

        if ($this->isGdEnabled()) {
            $field_types[] = array ('value' => 'captcha',  'name' => $this->_trans('Captcha'));
        } else {
            $this->warnings[] = $this->_trans('Missing GD library with jpeg support. Captcha will not work.');
        }

        $this->fields_form = array(
            'tinymce' => false,
            'legend' => array(
                'title' => $title,
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->_trans('Label:'),
                    'name' => 'label',
                    'lang' => true,
                    'required' => true,
                    'class' => 'fixed-width-xl',
                    'size' => 50
                ),
                array(
                    'type' => 'text',
                    'label' => $this->_trans('Name:'),
                    'name' => 'name',
                    'required' => true,
                    'class' => 'fixed-width-xl',
                    'size' => 50
                ),
                array(
                    'type' => 'select',
                    'label' => $this->_trans('Type:'),
                    'id' => 'ppc-field-select-types',
                    'name' => 'type',
                    'options' => array(
                        'query' => $field_types,
                        'id' => 'value',
                        'name' => 'name'
                    ),
                    'desc' => $this->_trans('Type of field.'),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->_trans('Values:'),
                    'name' => 'values',
                    'lang' => true,
                    'required' => false,
                    'class' => 'fixed-width-xl ppc-fields-values',
                    'desc' => html_entity_decode($this->_trans("<span class='field-select-items field-select field-radio'>Comma separated list of options.</span><span class='field-select-items field-checkbox'>Message to display at the right of the checkbox.</span>", 'powerfulproductcontact', false, false)),
                    'size' => 50
                ),
                array(
                    'type' => 'text',
                    'label' => $this->_trans('Extra :'),
                    'name' => 'extra',
                    'class' => 'fixed-width-xl',
                    'size' => 50,
                    'lang' => false
                ),
                array(
                    'type' => 'radio',
                    'label' => $this->_trans('Required:'),
                    'name' => 'required',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->_trans('Yes')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->_trans('No')
                        )
                    ),
                )
            ),
            'submit' => array(
                'title' => $this->_trans('Save'),
                'class' => 'btn btn-default pull-right'
            )
        );

        $this->tpl_form_vars = array(
            'required' => $this->object->required,
            'PS_ALLOW_ACCENTED_CHARS_URL', (int)Configuration::get('PS_ALLOW_ACCENTED_CHARS_URL')
        );

        return parent::renderForm();
    }

    /**
     * Returns true weither the GD library is enabled or not
     *
     * @return boolean
     */
    private function isGdEnabled()
    {
        return (function_exists('ImageCreate') && function_exists('ImageJpeg'));
    }

    protected function beforeAdd($object)
    {
        $object->position = (int)PPCFieldModel::getNextAvailablePosition();
    }

    public function getDisplay()
    {
        return $this->display;
    }

    public function getAction()
    {
        return $this->action;
    }

    public function _childValidation()
    {
        if (!preg_match('/^[a-z0-9_\[\]]+$/', Tools::getValue('name'))) {
            $this->errors[] = $this->_trans('Please use only alphanumerical (a-z, 0-9) and underscore ("_") caracters for the name field.');
        }

        // We check that the name does not exists already :
        if (PPCFieldModel::isNameAlreadyTaken(Tools::getValue('name'), (Tools::isSubmit('id_ppcontacts_fields') ? Tools::getValue('id_ppcontacts_fields') : null))) {
            $this->errors[] = $this->_trans('This name is already taken.');
        }

        if (Tools::getValue('type') === 'email') {
            if (PPCFieldModel::isEmailAlreadyPresent(Tools::getValue('id_ppcontacts_fields', null))) {
                $this->errors[] = $this->_trans('You cannot have more than one email field.');
            }
        }

        // if select || radio : values required
        if (in_array(Tools::getValue('type'), array('select', 'radio', 'checkbox'))) {
            $languages = Language::getLanguages(true);
            foreach ($languages as $language) {
                $value = Tools::getValue('values_'.$language['id_lang']);
                if (empty($value)) {
                    $this->errors[] = sprintf($this->_trans('You must indicates at least one value for "%s".'), $language['name']);
                }
            }
        }
    }
}
