SET FOREIGN_KEY_CHECKS=0;

-- --------------------------------------------------------

DROP TABLE IF EXISTS `ps_ppcontacts_fields`;
DROP TABLE IF EXISTS `ps_ppcontacts_fields_lang`;
DROP TABLE IF EXISTS `ps_ppcontacts_fields_shop`;

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS=1;
