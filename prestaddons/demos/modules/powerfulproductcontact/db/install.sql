--
-- Structure de la table `ps_ppcontacts_fields`
--

DROP TABLE IF EXISTS `ps_ppcontacts_fields`;
CREATE TABLE IF NOT EXISTS `ps_ppcontacts_fields` (
  `id_ppcontacts_fields` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `required` tinyint(1) NOT NULL,
  `position` tinyint(4) DEFAULT NULL DEFAULT '0',
  `extra` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_ppcontacts_fields`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_ppcontacts_fields_lang`
--

DROP TABLE IF EXISTS `ps_ppcontacts_fields_lang`;
CREATE TABLE IF NOT EXISTS `ps_ppcontacts_fields_lang` (
  `id_ppcontacts_fields_lang` int(11) NOT NULL AUTO_INCREMENT,
  `id_ppcontacts_fields` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `label` varchar(255) NOT NULL,
  `values` varchar(255) NOT NULL,
  PRIMARY KEY (`id_ppcontacts_fields_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ps_ppcontacts_fields_shop`
--

DROP TABLE IF EXISTS `ps_ppcontacts_fields_shop`;
CREATE TABLE IF NOT EXISTS `ps_ppcontacts_fields_shop` (
  `id_ppcontacts_fields` int(11) NOT NULL,
  `id_shop` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
