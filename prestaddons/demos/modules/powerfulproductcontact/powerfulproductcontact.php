<?php
/**
 * Powerful Product Contact
 *
 * Let your customers contact you for any questions by filling a form on any product page you want.
 *
 * If you find errors, bugs or if you want to share some improvments, feel free to contact at contact@prestaddons.net ! :)
 * Si vous trouvez des erreurs, des bugs ou si vous souhaitez tout simplement partager un conseil ou une amélioration,
 * n'hésitez pas à me contacter à contact@prestaddons.net
 *
 * @author    Cyril Nicodème <contact@prestaddons.net>
 * @copyright Copyright (C) June 2014 cnicodeme.com <@email:contact@prestaddons.net>. All rights reserved.
 * @license   Nicodème Cyril
 * @package   modules
 * @version   1.9.0
 * @since     2014-09-19
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

/*
 * TODO :
 *      Mettre en place des séparateurs entre les champs
 *      Ajouter un champs de type Date
 *      Checkbox à choix multiple (1 checkbox, plusieurs réponse possibles)
 *      Be able to create multiple forms
 */

require_once(dirname(__FILE__).'/classes/PPCRenderer.php');
require_once(dirname(__FILE__).'/classes/models/PPCFieldModel.php');
require_once(dirname(__FILE__).'/controllers/admin/AdminPPCFields.php');
class Powerfulproductcontact extends Module
{
    const INSTALL_SQL_FILE = 'install.sql';
    const UNINSTALL_SQL_FILE = 'uninstall.sql';

    public function __construct()
    {
        $this->name = 'powerfulproductcontact';
        $this->tab = 'front_office_features';
        $this->version = '1.9.0';
        $this->author = 'Cyril Nicodeme';
        $this->need_instance = 0;
        $this->module_key = 'e5331ab847235e2ceba94a666fa53620';
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->_trans('Powerful Product Contact');
        $this->description = $this->_trans('Let your customers contact you for any questions by filling a form on any product page you want.');

        $this->confirmUninstall = $this->_trans('Are you sure you want to uninstall?');

        if (is_null(Configuration::get('POWERFULPC_categories', null))) {
            $this->warning = $this->_trans('You need to indicate the categories where to show the forms.');
        } elseif (is_null(Configuration::get('POWERFULPC_title', null))) {
            $this->warning = $this->_trans('Please provide a title.');
        }
    }

    /**
     * Used for backward compatibility between 1.7 Prestashop version and older
     */
    private function _trans($string, $class = null, $addslashes = false, $htmlentities = true)
    {
        if ($class === null || $class == 'AdminTab') {
            $class = Tools::substr(get_class($this), 0, -10);
        } elseif (Tools::strtolower(Tools::substr($class, -10)) == 'controller') {
            /* classname has changed, from AdminXXX to AdminXXXController, so we remove 10 characters and we keep same keys */
            $class = Tools::substr($class, 0, -10);
        }

        if (version_compare(Tools::substr(_PS_VERSION_, 0, 7), '1.7.0.3', '>=')) {
            return Translate::getAdminTranslation($string, $class, $addslashes, $htmlentities);
        } else {
            return $this->l($string, $class, $addslashes, $htmlentities);
        }
    }

    public function install()
    {
        Configuration::updateValue('POWERFULPC_position', 'right');
        Configuration::updateValue('POWERFULPC_title', null); // TODO : set Title
        Configuration::updateValue('POWERFULPC_message', null);
        Configuration::updateValue('POWERFULPC_thanks', null);
        Configuration::updateValue('POWERFULPC_contact', null); // TODO : set default contact to first one
        Configuration::updateValue('POWERFULPC_categories', null);

        Configuration::updateValue('POWERFULPC_recaptcha_public', null);
        Configuration::updateValue('POWERFULPC_recaptcha_private', null);

        if (!$this->installTable()) {
            return false;
        }
        if (!parent::install()) {
            return false;
        }
        if (!$this->installModuleTab('AdminPPCFields', 'Powerful Product Contact Fields')) {
            return false;
        }

        $initial = new PPCFieldModel();
        $initial->type = 'email';
        $initial->name = 'email';
        $initial->required = true;
        $initial->position = 1;

        $languages = Language::getLanguages(true);
        foreach ($languages as $language) {
            $initial->label[$language['id_lang']] = 'Email';
        }

        $initial->add();

        return $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('displayHeader') &&
            $this->registerHook('displayFooterProduct') &&
            $this->registerHook('displayRightColumnProduct') &&
            $this->registerHook('displayLeftColumnProduct') &&
            $this->registerHook('displayPPCForm');
    }

    public function uninstall()
    {
        Configuration::deleteByName('POWERFULPC_position');
        Configuration::deleteByName('POWERFULPC_title');
        Configuration::deleteByName('POWERFULPC_message');
        Configuration::deleteByName('POWERFULPC_thanks');
        Configuration::deleteByName('POWERFULPC_contact');
        Configuration::deleteByName('POWERFULPC_categories');

        Configuration::deleteByName('POWERFULPC_recaptcha_public');
        Configuration::deleteByName('POWERFULPC_recaptcha_private');

        if (!$this->uninstallTable()) {
            return false;
        }
        if (!$this->uninstallModuleTab('AdminPPCFields')) {
            return false;
        }

        $hooks = $this->unregisterHook('header') &&
            $this->unregisterHook('backOfficeHeader') &&
            $this->unregisterHook('displayHeader') &&
            $this->unregisterHook('displayFooterProduct') &&
            $this->unregisterHook('displayRightColumnProduct') &&
            $this->unregisterHook('displayLeftColumnProduct') &&
            $this->unregisterHook('displayPPCForm');

        if (!$hooks) {
            return false;
        }

        return parent::uninstall();
    }

    /**
    * Install Table
    */
    public function installTable()
    {
        if (!file_exists(dirname(__FILE__).'/db/'.self::INSTALL_SQL_FILE)) {
            return false;
        } elseif (!$sql = Tools::file_get_contents(dirname(__FILE__).'/db/'.self::INSTALL_SQL_FILE)) {
            return false;
        }

        $sql = str_replace('ps_', _DB_PREFIX_, $sql);
        $sql = preg_split("/;\s*[\r\n]+/", $sql);
        foreach ($sql as $query) {
            $query = trim($query);
            if (empty($query)) {
                continue;
            }

            if (!Db::getInstance()->Execute($query)) {
                return false;
            }
        }

        return true;
    }

    /**
    * Uninstall Table
    */
    public function uninstallTable()
    {
        if (!file_exists(dirname(__FILE__).'/db/'.self::UNINSTALL_SQL_FILE)) {
            return false;
        } elseif (!$sql = Tools::file_get_contents(dirname(__FILE__).'/db/'.self::UNINSTALL_SQL_FILE)) {
            return false;
        }

        $sql = str_replace('ps_', _DB_PREFIX_, $sql);
        $sql = preg_split("/;\s*[\r\n]+/", $sql);

        foreach ($sql as $query) {
            $query = trim($query);
            if (empty($query)) {
                continue;
            }

            if (!Db::getInstance()->Execute($query)) {
                return false;
            }
        }

        return true;
    }

    private function installModuleTab($tabClass, $title, $tabParent = 'AdminParentModules')
    {
        $idTabParent = Tab::getIdFromClassName($tabParent);

        $languages = Language::getLanguages(true);
        $names = array();
        foreach ($languages as $language) {
            $names[$language['id_lang']] = $title;
        }

        $tab = new Tab();
        $tab->name = $names;
        $tab->class_name = $tabClass;
        $tab->module = $this->name;
        $tab->id_parent = $idTabParent;
        $tab->active = false;

        if (!$tab->save()) {
            return false;
        }

        return true;
    }

    private function uninstallModuleTab($tabClass)
    {
        $idTab = Tab::getIdFromClassName($tabClass);

        if ($idTab != 0) {
            $tab = new Tab($idTab);
            $tab->delete();
            return true;
        }

        return false;
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        $output = '';

        if (Tools::isSubmit('submitSettings')) {
            $output .= $this->_postProcess();
        }

        $this->context->smarty->assign(array (
            'active' => 'settings',
            'settingsUrl' => Context::getContext()->link->getAdminLink('AdminModules', true).'&configure='.$this->name,
            'fieldsUrl' => Context::getContext()->link->getAdminLink('AdminPPCFields', false).'&token='.Tools::getAdminTokenLite('AdminModules'),
            'warning' => $this->warning
        ));
        $output .= $this->context->smarty->fetch($this->local_path.'views/templates/admin/header.tpl');

        $output .= $this->renderForm();

        return $output;
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitSettings';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $messages = Configuration::get('POWERFULPC_message', null);
        if (!is_null($messages) && !empty($messages)) {
            $messages = array_map('html_entity_decode', @unserialize($messages));
        }

        $thanks = Configuration::get('POWERFULPC_thanks', null);
        if (!is_null($thanks) && !empty($thanks)) {
            $thanks = array_map('html_entity_decode', @unserialize($thanks));
        }

        $title = Configuration::get('POWERFULPC_title', null);
        if (!is_null($title) && !empty($title)) {
            $title = @unserialize($title);
        }

        $helper->tpl_vars = array(
            'fields_value' => array(
                'position'          => Configuration::get('POWERFULPC_position', 'right'),
                'title'             => $title,
                'message'           => $messages,
                'thanks'            => $thanks,
                'contact'           => Configuration::get('POWERFULPC_contact', null),
                'recaptcha_public'  => Configuration::get('POWERFULPC_recaptcha_public'),
                'recaptcha_private' => Configuration::get('POWERFULPC_recaptcha_private')
            ),
            'languages' => Context::getContext()->controller->getLanguages(),
            'id_language' => Context::getContext()->language->id,
        );

        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;

        $selectedCategories = @unserialize(Configuration::get('POWERFULPC_categories', null));
        if (Tools::isSubmit('submitSettings') && Tools::isSubmit('categoryBox')) {
            $selectedCategories = Tools::getValue('categoryBox');
        }
        if (!is_array($selectedCategories)) {
            $selectedCategories = array();
        }

        $contacts = array();
        foreach (Contact::getContacts($this->context->language->id) as $contact) {
            $contacts[$contact['id_contact']] = array (
                'value' => $contact['id_contact'],
                'name' => $contact['name'].' ('.$contact['email'].')'
            );
        }

        $form_structure = array(
            array(
                'form' => array(
                    'tinymce' => false,
                    'input' => array(
                        array(
                            'type' => 'select',
                            'label' => $this->_trans('Contact:'),
                            'name' => 'contact',
                            'options' => array(
                                'query' => $contacts,
                                'id' => 'value',
                                'name' => 'name'
                            ),
                            'desc' => $this->_trans('Contact to send the submissions to.'),
                            'class' => 'fixed-width-xxl'
                        ),
                        array(
                            'type' => 'select',
                            'label' => $this->_trans('Position:'),
                            'name' => 'position',
                            'options' => array(
                                'query' => array (
                                    array ('value' => 'left', 'name' => $this->_trans('Left column (Before the print link).')),
                                    array ('value' => 'right', 'name' => $this->_trans('Right column (after the Add to cart).')),
                                    array ('value' => 'footer', 'name' => $this->_trans('Footer (Before the tabs, after the description).')),
                                    array ('value' => 'customer', 'name' => $this->_trans('Custom (Place this hook wherever you want: displayPPCForm).')),
                                ),
                                'id' => 'value',
                                'name' => 'name'
                            ),
                            'desc' => $this->_trans('Position of the form in the product page.'),
                            'class' => 'fixed-width-xxl'
                        ),
                        array(
                            'type' => 'text',
                            'label' => $this->_trans('Title:'),
                            'name' => 'title',
                            'lang' => true,
                            'required' => true,
                            'size' => 50,
                            'desc' => $this->_trans('Title of the form displayed in the product page.')
                        ),
                        array(
                            'type' => 'textarea',
                            'label' => $this->_trans('Message:'),
                            'name' => 'message',
                            'autoload_rte' => true,
                            'lang' => true,
                            'required' => false,
                            'rows' => '5',
                            'cols' => '48',
                            'desc' => $this->_trans('Message to display before the form.'),
                        ),
                        array(
                            'type' => 'textarea',
                            'label' => $this->_trans('Thanks message:'),
                            'name' => 'thanks',
                            'autoload_rte' => true,
                            'lang' => true,
                            'required' => false,
                            'rows' => '5',
                            'cols' => '48',
                            'desc' => $this->_trans('Message to display once the form has been submitted.'),
                        ),
                        array(
                            'type'     => 'text',
                            'label'    => $this->_trans('Google RE:Captcha Website key :'),
                            'name'     => 'recaptcha_public',
                            'required' => false,
                            'class'    => 'fixed-width-xl',
                            'desc'     => $this->_trans('Your public website key from Google Re:Captcha'),
                            'size'     => 50
                        ),
                        array(
                            'type'     => 'text',
                            'label'    => $this->_trans('Google RE:Captcha Private key :'),
                            'name'     => 'recaptcha_private',
                            'required' => false,
                            'class'    => 'fixed-width-xl',
                            'desc'     => $this->_trans('Your private key from Google Re:Captcha'),
                            'size'     => 50
                        )
                    ),
                    'submit' => array(
                        'title' => $this->_trans('Save'),
                    ),
                ),
            )
        );

        if (class_exists('HelperTreeCategories')) {
            $root = Category::getRootCategory();
            $categoryTree = new HelperTreeCategories('categories-tree');
            $categoryTree->setUseCheckBox(false);
            $categoryTree->setAttribute('is_category_filter', $root->id);
            $categoryTree->setUseCheckBox(true);
            $categoryTree->setRootCategory($root->id);
            $categoryTree->setSelectedCategories($selectedCategories);
            $categoryTree->setUseSearch(true);
            $categoryTree->setInputName('categoryBox');

            $form_structure[0]['form']['input'][] = array(
                'type'  => 'categories_select',
                'label' => $this->_trans('Associated categories:'),
                'desc'  => $this->_trans('Select on which categories this form will be available.'),
                'name'  => 'categoryBox[]',
                'category_tree'  => $categoryTree->render()
            );
        } else {
            $form_structure[0]['form']['input'][] = array(
                'type'  => 'categories',
                'name'  => 'categoryBox',
                'label' => $this->_trans('Associated categories:'),
                'tree'  => array(
                    'id'                  => 'categories-tree',
                    'selected_categories' => $selectedCategories,
                    'use_search'          => true,
                    'use_checkbox'        => true
                ),
                'desc' => $this->_trans('Select on which categories this form will be available.'),
                'values' => array (
                    'selected_cat' => $selectedCategories,
                    'input_name'   => 'categoryBox[]',
                    'use_radio'    => false,
                    'use_search'   => true,
                    'top_category' => Category::getTopCategory(),
                    'use_context'  => true,
                )
            );
        }

        return $helper->generateForm($form_structure);
    }


    /**
     * Save form data.
     */
    protected function _postProcess()
    {

        $languages = Language::getLanguages(true);

        $titles = array();
        $messages = array();
        $thanks = array();

        $output = '';

        foreach ($languages as $language) {
            $value = Tools::getValue('title_'.$language['id_lang']);
            if (empty($value)) {
                $output .= $this->displayError(sprintf($this->_trans('You must indicates the title value for "%s".'), $language['name']));
            } else {
                $titles[$language['id_lang']] = $value;
            }

            $value = Tools::getValue('message_'.$language['id_lang']);
            $messages[$language['id_lang']] = htmlentities($value);

            $value = Tools::getValue('thanks_'.$language['id_lang']);
            $thanks[$language['id_lang']] = htmlentities($value);
        }

        if (!empty($output)) {
            return $output;
        }

        Configuration::updateValue('POWERFULPC_contact', Tools::getValue('contact'));
        Configuration::updateValue('POWERFULPC_position', Tools::getValue('position'));
        Configuration::updateValue('POWERFULPC_title', serialize($titles));
        Configuration::updateValue('POWERFULPC_message', serialize($messages));
        Configuration::updateValue('POWERFULPC_thanks', serialize($thanks));
        Configuration::updateValue('POWERFULPC_categories', serialize(Tools::getValue('categoryBox')));
        Configuration::updateValue('POWERFULPC_recaptcha_public', Tools::getValue('recaptcha_public'));
        Configuration::updateValue('POWERFULPC_recaptcha_private', Tools::getValue('recaptcha_private'));

        return $this->displayConfirmation($this->_trans('Settings updated'));
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::substr(_PS_VERSION_, 0, 3) === '1.5') {
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add required files for the form to be displayed correctly.
     */
    public function hookDisplayHeader()
    {
        if (version_compare(Tools::substr(_PS_VERSION_, 0, 3), '1.7', '>=')) {
            Context::getContext()->controller->addCSS(__PS_BASE_URI__.'modules/powerfulproductcontact/views/css/contact-form-17.css');
        }
    }

    public function hookDisplayLeftColumnProduct()
    {
        if (Configuration::get('POWERFULPC_position') !== 'left') {
            return;
        }

        return $this->getFrontendForm();
    }

    public function hookDisplayRightColumnProduct()
    {
        if (Configuration::get('POWERFULPC_position') !== 'right') {
            return;
        }

        return $this->getFrontendForm();
    }

    public function hookDisplayFooterProduct()
    {
        if (Configuration::get('POWERFULPC_position') !== 'footer') {
            return;
        }

        return $this->getFrontendForm();
    }

    public function hookDisplayPPCForm()
    {
        return $this->getFrontendForm();
    }

    protected function getFrontendForm()
    {
        $categories = @unserialize(Configuration::get('POWERFULPC_categories', null));
        $categoriesProduct = array();
        foreach ($categories as $categoryId) {
            $categoriesProduct[] = array('id_category' => $categoryId);
        }

        if (!Product::idIsOnCategoryId(Tools::getValue('id_product'), $categoriesProduct)) {
            return;
        }

        $product = new Product(Tools::getValue('id_product'));
        $renderer = new PPCRenderer($product);

        $title = @unserialize(Configuration::get('POWERFULPC_title', null));
        $message = @unserialize(Configuration::get('POWERFULPC_message', null));
        $thanks = @unserialize(Configuration::get('POWERFULPC_thanks', null));

        $smartyAssigns = array (
            'title'          => Tools::stripslashes($title[$this->context->language->id]),
            'message'        => Tools::stripslashes(html_entity_decode($message[$this->context->language->id])),
            'successMessage' => Tools::stripslashes(html_entity_decode($thanks[$this->context->language->id])),
            'isv16'          => Tools::stripslashes((Tools::substr(_PS_VERSION_, 0, 3) === '1.6')),
        );

        if (Tools::isSubmit('submitMessage')) {
            $results = array ();

            $renderer->validateReCaptcha();

            $customerEmail = null;
            foreach ($renderer->getFields() as $field) {
                if ($field['type'] === 'email') {
                    $customerEmail = $renderer->validateField($field);
                } else {
                    $results[$field['name']] = $renderer->validateField($field);
                }
            }

            if (count($renderer->getErrors()) === 0) {
                $smartyAssigns['successSent'] = $this->createNewSav($product, $renderer->generateMessageFromResults($results), $customerEmail);
                $this->context->smarty->assign($smartyAssigns);

                if (version_compare(Tools::substr(_PS_VERSION_, 0, 3), '1.7', '>=')) {
                    return $this->context->smarty->fetch($this->local_path.'views/templates/front/form17.tpl');
                } else {
                    return $this->context->smarty->fetch($this->local_path.'views/templates/front/form.tpl');
                }
            }
        }

        $smartyAssigns['fields'] = $renderer->generateFields();
        $smartyAssigns['errors'] = $renderer->getErrors();
        $this->context->smarty->assign($smartyAssigns);

        if (version_compare(Tools::substr(_PS_VERSION_, 0, 3), '1.7', '>=')) {
            return $this->context->smarty->fetch($this->local_path.'views/templates/front/form17.tpl');
        } else {
            return $this->context->smarty->fetch($this->local_path.'views/templates/front/form.tpl');
        }
    }

    public function createNewSav($product, $messageClient, $email = null)
    {
        $contact = new Contact(Configuration::get('POWERFULPC_contact'));

        // $productLink = $link->getProductLink($product);

        if (version_compare(Tools::substr(_PS_VERSION_, 0, 7), '1.7.5.1', '>=')) {
            $productLink = Context::getContext()->link->getProductLink($product);
        } else {
            $link = new Link();
            $categoryLink = Category::getLinkRewrite((int)$product->id_category_default, (int)$this->context->language->id);
            $productLink = $link->getProductLink((int)$product->id, $product->link_rewrite, $categoryLink, $product->ean13, $this->context->language->id);
        }

        $message = "\r\n".$this->_trans('Product link').' : '.$productLink."\r\n";
        $message .= $this->_trans('Customer language').' : '.Context::getContext()->language->name."\r\n";
        $message .= $messageClient;

        if (Shop::getContext() === Shop::CONTEXT_ALL) {
            $id_shop = null;
        } else {
            $id_shop = (int)Context::getContext()->shop->id;
        }

        $ct = new CustomerThread();
        $ct->id_shop = $id_shop;
        $ct->id_product = (int)Tools::getValue('id_product');
        $ct->id_lang = (int)Context::getContext()->language->id;
        $ct->id_contact = $contact->id;
        $ct->email = $email;
        $ct->status = 'open';
        $ct->token = Tools::passwdGen(12);
        $ct->add();

        $cm = new CustomerMessage();
        $cm->id_customer_thread = $ct->id;
        $cm->message = $message;
        $cm->ip_address = ip2long(Tools::getRemoteAddr());
        $cm->user_agent = $_SERVER['HTTP_USER_AGENT'];
        $cm->add();

        DB::getInstance()->execute('UPDATE '._DB_PREFIX_.'customer_message SET message = "'.pSQL($message, true).'" WHERE id_customer_message = '.(int)$cm->id);

        $var_list = array(
            '{order_name}' => '-',
            '{attached_file}' => '-',
            '{message}' => Tools::nl2br(Tools::stripslashes($message)),
            '{email}' =>  $email,
            '{product_name}' => $product->name[Context::getContext()->language->id]
        );

        return Mail::Send($this->context->language->id, 'contact', Mail::l('Message from contact form'), $var_list, $contact->email, $contact->name[Context::getContext()->language->id], $email, null, null);
    }
}
