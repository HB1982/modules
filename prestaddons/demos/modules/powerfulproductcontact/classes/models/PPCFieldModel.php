<?php
/**
 * Powerful Product Contact
 *
 * Let your customers contact you for any questions by filling a form on any product page you want.
 *
 * If you find errors, bugs or if you want to share some improvments, feel free to contact at contact@prestaddons.net ! :)
 * Si vous trouvez des erreurs, des bugs ou si vous souhaitez tout simplement partager un conseil ou une amélioration,
 * n'hésitez pas à me contacter à contact@prestaddons.net
 *
 * @author    Cyril Nicodème <contact@prestaddons.net>
 * @copyright Copyright (C) June 2014 cnicodeme.com <@email:contact@prestaddons.net>. All rights reserved.
 * @license   Nicodème Cyril
 * @package   modules
 * @version   1.9.0
 * @since     2014-09-19
 */

if (!defined('_CAN_LOAD_FILES_')) {
    exit;
}

if (!class_exists('PPCFieldModel')) {
    /**
     * Class ContactFormFieldModel
     */
    class PPCFieldModel extends ObjectModel
    {
        public $type;
        public $name;
        public $label;
        public $values;
        public $required;
        public $position;
        public $extra;

        public static $definition = array (
            'table' => 'ppcontacts_fields',
            'primary' => 'id_ppcontacts_fields',
            'multilang' => true,
            'multishop' => true,
            'fields' => array (
                'type'            => array ('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 255, 'required' => true),
                'name'            => array ('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isString', 'size' => 255, 'required' => true),
                'label'           => array ('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString', 'size' => 255, 'required' => true),
                'values'          => array ('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString', 'size' => 255, 'required' => false),
                'required'        => array ('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true),
                'position'        => array ('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
                'extra'           => array ('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 255),
            ),
        );

        /**
         * @param $way Sens du changement de position (0 si l'élément monte, 1 s'il descend)
         * @param $position La position qu'aura l'élément _après_ ledit mouvement
         *
         * @return Booléen indiquant la réussite ou l'échec de l'opération
         */
        public function updatePosition($way, $new_position)
        {
            $db = Db::getInstance();
            $count = $db->getValue('SELECT COUNT(*) FROM '._DB_PREFIX_.self::$definition['table'], false);
            if (($new_position >= 1) && ($new_position <= $count)) {
                $old_position = $way ? ($new_position - 1) : ($new_position + 1);

                if (($old_position >= 1) && ($old_position <= $count)) {
                    $sql = implode(
                        ';',
                        array(
                            'UPDATE '._DB_PREFIX_.self::$definition['table'].' SET position = 0 WHERE position = '.(int)$new_position,
                            'UPDATE '._DB_PREFIX_.self::$definition['table'].' SET position = '.(int)$new_position.' WHERE position = '.(int)$old_position,
                            'UPDATE '._DB_PREFIX_.self::$definition['table'].' SET position = '.(int)$old_position.' WHERE position = 0'
                        )
                    );

                    // L'ancienne et la nouvelle position sont valides, on les intervertit
                    return $db->execute($sql);
                }
            }

            return false;
        }

        /**
         * @param $way Sens du changement de position (0 si l'élément monte, 1 s'il descend)
         * @param $position La position qu'aura l'élément _après_ ledit mouvement
         *
         * @return Booléen indiquant la réussite ou l'échec de l'opération
         */
        public function updateNewPosition($new_position)
        {
            $db = Db::getInstance();
            $count = $db->getValue('SELECT COUNT(*) FROM '._DB_PREFIX_.self::$definition['table'], false);
            if (($new_position >= 1) && ($new_position <= $count)) {
                $old_position = $this->position;

                if (($old_position >= 1) && ($old_position <= $count)) {
                    $sql = implode(
                        ';',
                        array(
                            'UPDATE '._DB_PREFIX_.self::$definition['table'].' SET position = 0 WHERE position = '.(int)$new_position,
                            'UPDATE '._DB_PREFIX_.self::$definition['table'].' SET position = '.(int)$new_position.' WHERE position = '.(int)$old_position,
                            'UPDATE '._DB_PREFIX_.self::$definition['table'].' SET position = '.(int)$old_position.' WHERE position = 0'
                        )
                    );

                    // L'ancienne et la nouvelle position sont valides, on les intervertit
                    return $db->execute($sql);
                }
            }

            return false;
        }

        public function delete()
        {
            if ($this->type === 'email') {
                return false;
            }

            return parent::delete();
        }

        public static function findFields()
        {
            return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS(
                'SELECT f.id_ppcontacts_fields, f.type, f.name, f.required, fl.values, fl.label, f.position, f.extra
                FROM `'._DB_PREFIX_.self::$definition['table'].'` f
                LEFT JOIN `'._DB_PREFIX_.self::$definition['table'].'_lang` fl ON f.id_ppcontacts_fields = fl.id_ppcontacts_fields
                WHERE fl.id_lang = '.Context::getContext()->language->id.' ORDER BY f.position'
            );
        }

        public static function getNextAvailablePosition()
        {
            $sql = 'SELECT position FROM '._DB_PREFIX_.self::$definition['table'].' ORDER BY position DESC';

            $position = (int)Db::getInstance()->getValue($sql, false);
            return $position + 1;
        }

        public static function isEmailAlreadyPresent($id_field = null)
        {
            $sql = 'SELECT name FROM '._DB_PREFIX_.self::$definition['table'].' WHERE type="email"';
            if (!is_null($id_field)) {
                $sql .= ' AND id_ppcontacts_fields != '.(int)$id_field.';';
            }

            return (Db::getInstance()->getValue($sql, false) !== false);
        }

        public static function isNameAlreadyTaken($name, $id_field = null)
        {
            if ($id_field) {
                $query = 'SELECT name FROM `'._DB_PREFIX_.self::$definition['table'].'` WHERE id_ppcontacts_fields != '.(int)$id_field.' AND name = "'.$name.'"';
            } else {
                $query = 'SELECT name FROM `'._DB_PREFIX_.self::$definition['table'].'` WHERE name = "'.$name.'"';
            }

            return count(Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($query)) > 0;
        }
    }
}
