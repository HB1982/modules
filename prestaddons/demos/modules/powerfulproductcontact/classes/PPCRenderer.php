<?php
/**
 * Powerful Product Contact
 *
 * Let your customers contact you for any questions by filling a form on any product page you want.
 *
 * If you find errors, bugs or if you want to share some improvments, feel free to contact at contact@prestaddons.net ! :)
 * Si vous trouvez des erreurs, des bugs ou si vous souhaitez tout simplement partager un conseil ou une amélioration,
 * n'hésitez pas à me contacter à contact@prestaddons.net
 *
 * @author    Cyril Nicodème <contact@prestaddons.net>
 * @copyright Copyright (C) June 2014 cnicodeme.com <@email:contact@prestaddons.net>. All rights reserved.
 * @license   Nicodème Cyril
 * @package   modules
 * @version   1.9.0
 * @since     2014-09-19
 */

require_once(dirname(__FILE__).'/models/PPCFieldModel.php');
class PPCRenderer
{
    private $product;
    private $errors = array();

    private $fields;

    public function __construct($product)
    {
        $this->fields = PPCFieldModel::findFields();
        if (count($this->fields) == 0) {
            throw new Exception('No field were found in this form.');
        }

        $this->product = $product;
    }

    public function generateFields()
    {
        $form_fields = array();
        foreach ($this->fields as $field) {
            $type = 'text';
            $element = '';
            switch ($field['type']) {
                case 'text':
                case 'email':
                case 'url':
                case 'file':
                    $element = '<input type="'.$field['type'].'" ';
                    if (Tools::isSubmit($field['name'])) {
                        $element .= 'value="'.Tools::getValue($field['name']).'" ';
                    }
                    $element = $this->addAttributes($element, $field);
                    $element .= '/>';
                    break;
                case 'textarea':
                    $type = 'textarea';
                    $element = '<textarea rows="5" cols="10" ';
                    $element = $this->addAttributes($element, $field);
                    $element .= '>';

                    $element .= Tools::getValue($field['name']);

                    $element .= '</textarea>';
                    break;
                case 'select':
                case 'attributes':
                    $type = 'select';
                    $valuePost = Tools::getValue($field['name']);

                    $element = '<select ';
                    $element = $this->addAttributes($element, $field);
                    $element .= '>';
                    if ($field['type'] === 'attributes') {
                        $attributes = $this->product->getAttributesResume(Context::getContext()->language->id);
                        if (count($attributes) === 1) {
                            $type = 'hidden';
                            $element = '<input type="hidden" ';
                            $field['required'] = 0;
                            $field['extra'] = null;
                            $element = $this->addAttributes($element, $field, true);
                            $element .= 'value="'.$attributes[0]['id_product_attribute'].'" ';
                            $element .= '/>';
                            break;
                        }

                        foreach ($attributes as $attribute) {
                            $element .= '<option value="'.$attribute['id_product_attribute'].'"';
                            if ($valuePost === $attribute['id_product_attribute']) {
                                $element .= ' selected';
                            }

                            $element .= '>'.Tools::stripslashes($attribute['attribute_designation']).'</option>';
                        }
                    } elseif (!empty($field['values'])) {
                        $values = explode(',', $field['values']);
                        foreach ($values as $value) {
                            $value = trim($value);
                            $element .= '<option value="'.$value.'"';
                            if ($valuePost === $value) {
                                $element .= ' selected';
                            }

                            $element .= '>'.Tools::stripslashes($value).'</option>';
                        }
                    }
                    $element .= '</select>';
                    break;
                case 'radio':
                    $type = 'radio';
                    $valuePost = Tools::getValue($field['name']);

                    if (!empty($field['values'])) {
                        $values = explode(',', $field['values']);
                        $element = '<span>';
                        foreach ($values as $value) {
                            $value = trim($value);
                            if (Tools::substr(_PS_VERSION_, 0, 3) === '1.6') {
                                $element .= '<label class="checkbox">';
                            } else {
                                $element .= '<label class="input">';
                            }
                            $element .= '<input type="radio" value="'.$value.'" ';
                            if ($valuePost === $value) {
                                $element .= 'checked ';
                            }

                            $element = $this->addAttributes($element, $field, false);
                            $element .= '/> '.Tools::stripslashes($value).'</label>';
                        }
                        $element .= '</span>';
                    }

                    break;
                case 'checkbox':
                    $type = 'checkbox';
                    if (Tools::substr(_PS_VERSION_, 0, 3) === '1.6') {
                        $element = '<label class="checkbox"><input type="checkbox" value="true" ';
                    } else {
                        $element = '<label class="input"><input type="checkbox" value="true" ';
                    }
                    if (Tools::getValue($field['name']) === 'true') {
                        $element .= 'checked ';
                    }
                    $element = $this->addAttributes($element, $field);
                    $element .= '/> '.Tools::stripslashes($field['values']).'</label>';
                    break;
                case 'captcha':
                    $element = '<label class="checkbox"><img src="'.__PS_BASE_URI__.
                                    'modules/powerfulproductcontact/controllers/front/captcha.php?t='.time().'" alt="Captcha value" height="50"></label>';
                    $element .= '<input type="'.$field['type'].'" ';
                    $element = $this->addAttributes($element, $field);
                    $element .= '/>';
                    break;
            }

            if (empty($element)) {
                continue;
            }

            $form_fields[] = array (
                'type' => $type,
                'name' => $field['name'],
                'label' => Tools::stripslashes($field['label']),
                'element' => $element,
                'id' => 'field_'.$field['name'],
                'required' => ($field['required'] === '1') ? true : false
            );
        }

        $recaptcha = array(
            'public' => Configuration::get('POWERFULPC_recaptcha_public'),
            'private' => Configuration::get('POWERFULPC_recaptcha_private')
        );

        if (!empty($recaptcha['public']) && !empty($recaptcha['private'])) {
            $form_fields[] = array (
                'type' => 'recaptcha',
                'name' => 'recaptcha',
                'label' => null,
                'element' => '<script src="https://www.google.com/recaptcha/api.js" async defer></script><div class="g-recaptcha" data-sitekey="'.$recaptcha['public'].'"></div>',
                'id' => 'field_recaptcha',
                'required' => true
            );
        }

        return $form_fields;
    }

    private function l($message, $sprintf = array())
    {
        return Translate::getModuleTranslation('powerfulproductcontact', $message, __CLASS__, $sprintf);
    }

    private function addAttributes($element, $field, $ignoreId = false)
    {
        //  f.name, f.required, f.class, f.style, f.extra

        if (isset($field['extra']) && strpos($field['extra'], 'multiple') !== false) {
            $element .= 'name="'.$field['name'].'[]" ';
        } else {
            $element .= 'name="'.$field['name'].'" ';
        }

        if (!$ignoreId) {
            $element .= 'id="field_'.$field['name'].'" ';
        }

        if ($field['required'] === '1') {
            $element .= 'required ';
        }

        if (Tools::substr(_PS_VERSION_, 0, 3) === '1.6') {
            $element .= 'class="form-control" ';
        }

        if (isset($field['extra']) && !empty($field['extra'])) {
            $element .= $field['extra'].' ';
        }

        return $element;
    }

    public function validateReCaptcha()
    {
        $recaptcha = array(
            'public' => Configuration::get('POWERFULPC_recaptcha_public'),
            'private' => Configuration::get('POWERFULPC_recaptcha_private')
        );

        if (!empty($recaptcha['public']) && !empty($recaptcha['private'])) {
            $google_result = Tools::getValue('g-recaptcha-response');
            if (empty($google_result)) {
                $this->errors[] = $this->l('Invalid captcha value.');
                return null;
            }

            $ch = curl_init();
            curl_setopt_array($ch, array(
                CURLOPT_URL => 'https://www.google.com/recaptcha/api/siteverify',
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => array (
                    'secret' => Configuration::get('POWERFULPC_recaptcha_private'),
                    'response' => $google_result,
                    'remoteip' => $_SERVER['REMOTE_ADDR']
                ),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_TIMEOUT => 3
            ));

            //execute post
            $result = Tools::jsonDecode(curl_exec($ch), true);
            curl_close($ch);

            if (is_null($result) || $result['success'] === false) {
                $this->errors[] = $this->l('Invalid captcha value.');
            }
        }
    }

    public function validateField($field)
    {
        $value = Tools::getValue($field['name']);

        if ($field['required'] === '1' && empty($value) && !in_array($field['type'], array('file', 'recaptcha'))) {
            $this->errors[] = $this->l('The field %s is required.', array ($field['label']));
        }

        if ($field['required'] !== '1' && empty($value)) {
            return $value;
        }

        if ($field['type'] === 'email') {
            if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                $this->errors[] = $this->l('The field %s must be a valid email.', array ($field['label']));
            }
        } elseif ($field['type'] === 'url') {
            if (!filter_var($value, FILTER_VALIDATE_URL)) {
                $this->errors[] = $this->l('The field %s must be a valid URL.', array ($field['label']));
            }
        } elseif (in_array($field['type'], array('select', 'radio'))) {
            $values = array_map('trim', explode(',', $field['values']));
            if (!in_array($value, $values)) {
                $this->errors[] = $this->l('Invalid entry given for %s.', array ($field['label']));
            }
        } elseif ($field['type'] === 'attributes') {
            $productList = $this->product->getAttributesResume(Context::getContext()->language->id);
            if (is_array($value)) {
                $arrayValue = array();
                foreach ($value as $attributeValue) {
                    foreach ($productList as $attribute) {
                        if ($attribute['id_product_attribute'] === (string)$attributeValue) {
                            $arrayValue[] = $attribute['attribute_designation'];
                            continue;
                        }
                    }
                }

                $value = '<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.implode('<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', $arrayValue);
            } else {
                foreach ($productList as $attribute) {
                    if ($attribute['id_product_attribute'] === (string)$value) {
                        $value = $attribute['attribute_designation'];
                        continue;
                    }
                }
            }
        } elseif ($field['type'] === 'captcha') {
            if ($value !== Context::getContext()->cookie->pfg_captcha_string) {
                $this->errors[] = $this->l('Invalid captcha value.', array ($field['label']));
            }

            unset(Context::getContext()->cookie->pfg_captcha_string);
            return null;
        }

        return $value;
    }

    public function generateMessageFromResults($results)
    {
        $message_txt = '';
        foreach ($this->fields as $field) {
            if (!isset($results[$field['name']])) {
                continue;
            }

            if ($field['type'] === 'captcha') {
                continue;
            }

            $value = $results[$field['name']];

            if ('true' === $value) {
                $value = $this->l('Yes');
            }
            if ('false' === $value) {
                $value = $this->l('No');
            }

            $message_txt .= $field['label'].' : ';

            if (is_array($value)) {
                $message_txt .= implode(', ', $value)."\r\n";
            } else {
                $message_txt .= $value."\r\n";
            }
        }

        return $message_txt;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function getFields()
    {
        return $this->fields;
    }
}
