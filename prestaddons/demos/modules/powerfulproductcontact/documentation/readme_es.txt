Instalación
============

La instalación de este módulo es bastante fácil.
Todo lo que tienes que hacer es ir en su perfil módulos en su backoffice Prestashop, haga clic en "Agregar un nuevo módulo", seleccione el archivo y voilà! :)

Una vez instalado, usted será capaz de configurarlo.
Normalmente, debe estar en la página de configuración del módulo de la derecha después de que sea la instalación.
Si ese no es el caso, basta con ir en los módulos, la búsqueda de nuestro módulo y haga clic en "Configurar".

Configuración
=============

La página de configuración está dividido en dos categorías: Ajustes y campos.

Ajustes
--------

Los ajustes están aquí para administrar cómo funciona el formulario.
Para eso, usted será capaz de definir diversas informaciones, entre ellas:

 * El contacto quien recibirá las notificaciones. (Usted puede manejar contactos directamente en Prestashop, en Clientes> Contactos)
 * El título de su formulario
 * Un mensaje para que aparezca justo antes de los campos (optionnal)
 * Un mensaje de agradecimiento (optionnal)
 * La posición del formulario en la página del producto, usted puede elegir entre:
    * La columna izquierda de la página del producto (Antes de que el botón "Imprimir")
    * La columna de la derecha (Después de que el bloque de "Añadir a la carta")
    * El pie de página (después de la descripción, antes de que las pestañas.)
 * Las categorías en las que la forma estarán disponibles.


La gestión de los campos
-------------------

Al crear / upadting un campo, debe llenar varios valores.

Usted necesita saber los "valores" de campo específico, que sólo se utiliza en este tipo de campo:

 * Seleccionar: Los valores de campo será una lista de valores separados por comas que se relacionan con cada entrada en el campo de selección.
 * Radio: Los valores de campo será una lista de valores separados por comas que se mostrará como cada butons de radio
 * Archivo: Los valores de campo será una lista de valores separados por comas que representan los formatos de archivo permitidos, sin el punto.

La parte "extra" está aquí para que pueda personalizar el campo mediante la adición de más atributos.
Por ejemplo, puede establecer el extra para "múltiples" para añadir una selecta elección "múltiple".


Contacto
=======

Usted encuentra un error, usted tiene algunos problemas de configurar / instalar / hacer este módulo obras?
No dude en contactar conmigo en https://addons.prestashop.com/contact-community.php?id_product=17761

Voy a hacer mi mejor esfuerzo para ayudarle.


Gracias !
========

Usted compró mi módulo, y para eso, yo quería darle las gracias!
Espero que encuentre lo que necesita.
