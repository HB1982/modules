<?php
/**
 * Total Shopping Exporter
 *
 * This modules export your products catalog to many comparison websites like Google shopping, totalshoppingexporter, Shopzilla and many others.
 *
 * If you find errors, bugs or if you want to share some improvments, feel free to contact at contact@prestaddons.net ! :)
 * Si vous trouvez des erreurs, des bugs ou si vous souhaitez tout simplement partager un conseil ou une amélioration,
 * n'hésitez pas à me contacter à contact@prestaddons.net
 *
 * @author    Cyril Nicodème <contact@prestaddons.net>
 * @copyright Copyright (C) June 2014 cnicodeme.com <email:contact@prestaddons.net>. All rights reserved.
 * @license   Nicodème Cyril
 * @since     2014-06-18
 * @package   modules
 * @version   2.6.5
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

/**
 * Upgrade the settings to the appropriate new version
 */
function upgrade_module_2_6_2($object)
{
    return ($object->registerHook('actionValidateOrder') && $object->registerHook('displayFooter'));
}
