<?php
/**
 * Total Shopping Exporter
 *
 * This modules export your products catalog to many comparison websites like Google shopping, totalshoppingexporter, Shopzilla and many others.
 *
 * If you find errors, bugs or if you want to share some improvments, feel free to contact at contact@prestaddons.net ! :)
 * Si vous trouvez des erreurs, des bugs ou si vous souhaitez tout simplement partager un conseil ou une amélioration,
 * n'hésitez pas à me contacter à contact@prestaddons.net
 *
 * @author    Cyril Nicodème <contact@prestaddons.net>
 * @copyright Copyright (C) June 2014 cnicodeme.com <email:contact@prestaddons.net>. All rights reserved.
 * @license   Nicodème Cyril
 * @since     2014-06-18
 * @package   modules
 * @version   2.6.5
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

/**
 * Upgrade the settings to the appropriate new version
 */
function upgrade_module_2_2_8($object)
{
    $basename = 'TSEXPORTER';
    $settings = Configuration::getMultiple(array (
        $basename.'_default_carrier', $basename.'_delivery_schedule', $basename.'_categories'
    ));

    $settings['categories'] = unserialize($settings[$basename.'_categories']);
    if ($settings['categories'] === false) {
        return true;
    }

    foreach ($settings['categories'] as $lang => $category) {
        $settings['categories'][$lang]['currency_id'] = $settings['categories'][$lang]['currency'];
        unset($settings['categories'][$lang]['currency']);

        $settings['categories'][$lang]['delivery_schedule'] = $settings[$basename.'_delivery_schedule'];
        $settings['categories'][$lang]['carrier_id'] = $settings[$basename.'_default_carrier'];
    }

    Configuration::deleteByName($basename.'_default_carrier');
    Configuration::deleteByName($basename.'_delivery_schedule');

    Configuration::updateValue($basename.'_categories', serialize($settings['categories']));

    return true;
}
