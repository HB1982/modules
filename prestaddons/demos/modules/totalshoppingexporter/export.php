<?php
/**
 * Shopping Exporter
 *
 * This modules export your products catalog
 *
 * If you find errors, bugs or if you want to share some improvments, feel free to contact at contact@prestaddons.net ! :)
 * Si vous trouvez des erreurs, des bugs ou si vous souhaitez tout simplement partager un conseil ou une amélioration,
 * n'hésitez pas à me contacter à contact@prestaddons.net
 *
 * @author    Cyril Nicodème <contact@prestaddons.net>
 * @copyright Copyright (C) June 2014 cnicodeme.com <email:contact@prestaddons.net>. All rights reserved.
 * @license   Nicodème Cyril
 * @since     2014-06-18
 * @package   modules
 * @version   2.6.5
 */

require_once(dirname(__FILE__).'/generator.php');
$generator = new TotalShoppingExporterGenerator();

if (!$generator->isValidToken(Tools::getIsset('shop_id') ? Tools::getValue('shop_id') : null)) {
    exit('Invalid token given.');
}

$engine = Tools::getValue('engine', null);
if (is_null($engine)) { /* retro compatibility */
    $engine = Tools::getValue('comparator', null);
}

$generator->get($engine, Tools::getValue('shop_id', null), Tools::getValue('lang_id', null));
exit(0);
