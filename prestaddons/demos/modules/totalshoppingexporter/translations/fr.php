<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{totalshoppingexporter}prestashop>totalshoppingexporter_e1ab0d36eef23085864ed1f71ffd381c'] = 'Exportez votre catalogue de produit vers plusieurs comparateurs en ligne tel que Google Shopping, Bing Shopping, Amazon, Shopping.com, Shopzilla et d\'autres.';
$_MODULE['<{totalshoppingexporter}prestashop>totalshoppingexporter_876f23178c29dc2552c0b48bf23cd9bd'] = 'Êtes-vous sûr de vouloir désinstaller ?';
$_MODULE['<{totalshoppingexporter}prestashop>totalshoppingexporter_470fa92effa8872a1e8b909bb9d3e740'] = 'Vous devez configurer quelles catégories vous souhaitez exporter afin que ce module fonctionne correctement.';
$_MODULE['<{totalshoppingexporter}prestashop>totalshoppingexporter_f31e888f74495d31848322f406fd7b3c'] = 'Votre dossier /cache/ doit être accessible en écriture pour que ce module fonctionne correctement.';
$_MODULE['<{totalshoppingexporter}prestashop>totalshoppingexporter_be9055c9433667d6178f46f0116a0d91'] = 'Vos options ont bien été mises à jours.';
$_MODULE['<{totalshoppingexporter}prestashop>totalshoppingexporter_ae7e7bbaa8d9fdd5ecc91fdb7fd31720'] = 'Vous devez avoir l\'option "allow_url_fopen" d\'activée pour que ce module fonctionne correctement.';

$_MODULE['<{totalshoppingexporter}prestashop>totalshoppingexporter_c7d60198277d653af4dde3baffe3c694'] = 'Moteur:';
$_MODULE['<{totalshoppingexporter}prestashop>totalshoppingexporter_0885f0c211f74834f0109c5abaf4cdc4'] = 'Langue :';
$_MODULE['<{totalshoppingexporter}prestashop>totalshoppingexporter_77295c7d814e7397c55f64ec06313984'] = 'Devise :';
$_MODULE['<{totalshoppingexporter}prestashop>totalshoppingexporter_f8617a92ba0a0a4eabee724eab7c9f48'] = 'Transporteur :';
$_MODULE['<{totalshoppingexporter}prestashop>totalshoppingexporter_9cc1969d759af648f6019cbcfdc82605'] = 'Délai de livraison :';
$_MODULE['<{totalshoppingexporter}prestashop>totalshoppingexporter_ff43bac1bfde87120f1ab38554a77db8'] = 'Temps de livraison en jours.';
$_MODULE['<{totalshoppingexporter}prestashop>totalshoppingexporter_5a330fdb0ceca2c0503e8b52ae27ee06'] = 'Catégories a exporter :';
$_MODULE['<{totalshoppingexporter}prestashop>totalshoppingexporter_a425983104706206c69346f3cfbbd132'] = 'Sélectionnez quelles catégories principales vous souhaitez exporter. NOTE:  Seulement les produits qui ont la catégorie principale de sélectionné seront exportés.';
$_MODULE['<{totalshoppingexporter}prestashop>totalshoppingexporter_a0bfb8e59e6c13fc8d990781f77694fe'] = 'Continuer';
$_MODULE['<{totalshoppingexporter}prestashop>totalshoppingexporter_61a28facead994aa4ee9f903c6453238'] = 'Veuillez sélectionner un délai de livraison valide (en jours).';
$_MODULE['<{totalshoppingexporter}prestashop>totalshoppingexporter_9f746b166748b020d08262e37e163e92'] = 'Veuillez sélectionner au moins une catégorie.';

$_MODULE['<{totalshoppingexporter}prestashop>totalshoppingexporter_48c7c41b72e1d678923ce3571aa65b2d'] = 'Étape';
$_MODULE['<{totalshoppingexporter}prestashop>totalshoppingexporter_bb4d139e34957abc2f2795a505f4b789'] = 'Mise à jour de la langue';
$_MODULE['<{totalshoppingexporter}prestashop>totalshoppingexporter_eed61f806041057d16183df98495691e'] = 'Ajout d\'une langue';

$_MODULE['<{totalshoppingexporter}prestashop>settings_0db377921f4ce762c62526131097968f'] = 'Général';
$_MODULE['<{totalshoppingexporter}prestashop>settings_b61d8b2fb87fa20bbd2ee212b2ca582d'] = 'Comparateurs';
$_MODULE['<{totalshoppingexporter}prestashop>settings_0fa5d0a46badce14fa2cbcc99bb7c91f'] = 'Catégories et configuration';
$_MODULE['<{totalshoppingexporter}prestashop>settings_dae8ace18bdcbcc6ae5aece263e14fe8'] = 'Options';
$_MODULE['<{totalshoppingexporter}prestashop>settings_287234a1ff35a314b5b6bc4e5828e745'] = 'Déclinaisons';
$_MODULE['<{totalshoppingexporter}prestashop>settings_1b793db6c00482d633b29701c7bcb08c'] = 'Tâche planifiée';
$_MODULE['<{totalshoppingexporter}prestashop>settings_6a26f548831e6a8c26bfbbd9f6ec61e0'] = 'Aide';
$_MODULE['<{totalshoppingexporter}prestashop>settings_3dce6aae291c546244425fed453711b2'] = 'Une fois que vous aurez configuré ce module correctement, vous pourrez envoyer l\'url générée aux comparateurs.';
$_MODULE['<{totalshoppingexporter}prestashop>settings_99a71eaf4f016e08cf22188eab79a924'] = 'Afin d\'avoir de bonne performance lors de la génération, nous vous recommandons de mettre en place une tâche CRON qui re-générera les fichiers.';
$_MODULE['<{totalshoppingexporter}prestashop>settings_4a928190d375a3ded8096efde97a072b'] = 'Pour plus de détails, allez dans l\'onglet \"Tâche planifiée\".';
$_MODULE['<{totalshoppingexporter}prestashop>settings_13f61335bd6ed8ee82ae1b0a112c5442'] = 'Ajouter une nouvelle langue à exporter';
$_MODULE['<{totalshoppingexporter}prestashop>settings_bad4d882ab9d64947c364d68b62d3a51'] = 'Cliquez-ici pour ajouter une nouvelle langue a exporter.';
$_MODULE['<{totalshoppingexporter}prestashop>settings_7713b50b26e653ec667076b2287a3faa'] = 'Mettre à jour les paramètres';
$_MODULE['<{totalshoppingexporter}prestashop>settings_3e7acc5bf1ea6f600c963d4f36a0537d'] = 'CLiquez-ici pour mettre à jour les paramètres concernant cette langue.';
$_MODULE['<{totalshoppingexporter}prestashop>settings_a422fc5a56d6fcbe1a7bc87c196713d7'] = 'Cliquez-ici pour supprimer cet export.';
$_MODULE['<{totalshoppingexporter}prestashop>settings_f2a6c498fb90ee345d997f888fce3b18'] = 'Supprimer';

$_MODULE['<{totalshoppingexporter}prestashop>settings_386c339d37e737a436499d423a77df0c'] = 'Devise';
$_MODULE['<{totalshoppingexporter}prestashop>settings_914419aa32f04011357d3b604a86d7eb'] = 'Transporteur';


$_MODULE['<{totalshoppingexporter}prestashop>settings_a7186c203b3aee025e5e4ace31a31a74'] = 'Vous n\'avez pas encore configuré d\'export. Vous devez d\'abord cliquer sur le bouton bleu en haut à droite pour configurer un export de vos produits par langue.';
$_MODULE['<{totalshoppingexporter}prestashop>settings_3fd6f07f6cc2d4251abcee8304c70afb'] = 'Vous pouvez configurer un export par langue avec certaines options, tel que la devise, le transporteur et son délai de livraison.';
$_MODULE['<{totalshoppingexporter}prestashop>settings_86f198e82a394fa3807d52314147bbe7'] = 'Vous pourrez aussi configurer quelles catégories vous souhaitez exporter, et ainsi sélectionner quelle catégorie vous souhaitez exporter par langue.';

$_MODULE['<{totalshoppingexporter}prestashop>settings_016e87115c271ab98fb22cd0c3756154'] = 'Vous n\'avez pas encore défini de comparateur.';
$_MODULE['<{totalshoppingexporter}prestashop>settings_fef855f192afafeb4d2a0d9847437cb8'] = 'Vous devez tout d\'abord aller dans l\'onglet \"Comparateur\" et cocher les moteurs vers lesquels vous souhaitez exporter vos produits.';
$_MODULE['<{totalshoppingexporter}prestashop>settings_e6060efcd1ac00df05b6e3f759b7942e'] = 'Ensuite, allez dans l\'onglet \"Catégorie\" pour sélectionner les catégories que vous souhaitez exporter.';
$_MODULE['<{totalshoppingexporter}prestashop>settings_8bc50e4b8d0cdd1a44b310215723650e'] = 'Une fois que vous aurez effectué ceci, vous aurez les adresse URL disponible ici que vous pourrez indiquer aux différents comparateurs.';
$_MODULE['<{totalshoppingexporter}prestashop>settings_e93c33bd1341ab74195430daeb63db13'] = 'Nom de la boutique';
$_MODULE['<{totalshoppingexporter}prestashop>settings_c94cc4dc2a3a88d8beaf6808f7e111c7'] = 'URLs';
$_MODULE['<{totalshoppingexporter}prestashop>settings_df60dc7cae25b4010f0ccfe7523486d7'] = 'Cliquez ici pour voir le flux de produits';
$_MODULE['<{totalshoppingexporter}prestashop>settings_b373a5888ed34ef01c16a2027b68e2ed'] = 'Enregistrer les changements';
$_MODULE['<{totalshoppingexporter}prestashop>settings_0e9cd2c3bccec1128f4406a0aedd98d0'] = 'Enregistrer les relations';
$_MODULE['<{totalshoppingexporter}prestashop>settings_4980751027bd3a1294dfeb4926b65e33'] = 'Exporter les produits s\'il n\'y en a plus en stock.';
$_MODULE['<{totalshoppingexporter}prestashop>settings_b55197a49e8c4cd8c314bc2aa39d6feb'] = 'Indisponible';
$_MODULE['<{totalshoppingexporter}prestashop>settings_5be8b098f9b79cf3b18767ffc5ecd169'] = 'Pré-commande';
$_MODULE['<{totalshoppingexporter}prestashop>settings_f2149c422ab7577f063b69a2884d17f0'] = 'Disponbile à la commande';
$_MODULE['<{totalshoppingexporter}prestashop>settings_2d766821b5ee545f19181e81aa290fa3'] = 'Exporter les produits sans description';
$_MODULE['<{totalshoppingexporter}prestashop>settings_0b5f9e3c62c759901f727dac6e6d82a7'] = 'Exporter les produits n\'ayant pas d\'EAN13 ni d\'UPC';
$_MODULE['<{totalshoppingexporter}prestashop>settings_733a44efc876aec3865555a1f1a5643d'] = 'Exporter les produits qui n\'ont pas de Marque';
$_MODULE['<{totalshoppingexporter}prestashop>settings_e3167900f9db8d4015f0ffa39f1fe4db'] = 'Exporter les déclinaisons des produits';
$_MODULE['<{totalshoppingexporter}prestashop>settings_8394f0347c184cf156ac5924dccb773b'] = 'Longue';
$_MODULE['<{totalshoppingexporter}prestashop>settings_30bb747c98bccdd11b3f89e644c4d0ad'] = 'Courte';
$_MODULE['<{totalshoppingexporter}prestashop>settings_03c2e7e41ffc181a4e84080b4710e81e'] = 'Nouveau';
$_MODULE['<{totalshoppingexporter}prestashop>settings_019d1ca7d50cc54b995f60d456435e87'] = 'Usée';
$_MODULE['<{totalshoppingexporter}prestashop>settings_6da03a74721a0554b7143254225cc08a'] = 'Remis à neuf';
$_MODULE['<{totalshoppingexporter}prestashop>settings_fff5ded394f337325201b9c5eda56eeb'] = '72H par défaut';
$_MODULE['<{totalshoppingexporter}prestashop>settings_621ff9fd4508e487131e296f7a01c524'] = '3 jours par défaut';
$_MODULE['<{totalshoppingexporter}prestashop>settings_5dd70176875bfeae3a5d344c0a28b4ed'] = 'Afin d\'améliorer la performance générale de la génération du flux produits, nous vous conseillons d\'utiliser une tâche planifiée (CRON).';
$_MODULE['<{totalshoppingexporter}prestashop>settings_9ae7f418e526890c6acf0100ae8f89b8'] = 'En d\'autres termes :';
$_MODULE['<{totalshoppingexporter}prestashop>settings_ec5fb60adff1727f3b7b061c66902469'] = 'Sans tâche planifiée';
$_MODULE['<{totalshoppingexporter}prestashop>settings_e544670fe0850294d3a2c083e10d36df'] = 'lorsqu\\\'un comparateur chargera votre liste de produit, notre module chargera votre base de donnée et génèrera le fichier. Plus vous aurez de produits, plus la génération sera longue.';
$_MODULE['<{totalshoppingexporter}prestashop>settings_9e5c5e21ff550a9cd96604be5a365d48'] = 'Par ailleurs, si vous avez beaucoup de produits, cela pourrait aboutir à un temps de chargement trop long et au final à une erreur.';
$_MODULE['<{totalshoppingexporter}prestashop>settings_f9edcbf30b33a4be755dd612d6e23cb2'] = 'Avec une tâche planifiée';
$_MODULE['<{totalshoppingexporter}prestashop>settings_241d63df68eecdead55c3ecfc8ca68a1'] = 'la tâche va générer le fichier de manière asynchrône et s\\\'assurera que le fichier sera correctement créer et sauvegardé dans le cache de Prestashop.';
$_MODULE['<{totalshoppingexporter}prestashop>settings_23322b2323034ee43b76ad56791d0c11'] = 'Quand un comparateur chargera votre flux de produits, notre module ne chargera alors pas votre base de donnée, mais le fichier en cache, ce qui se fera de manière quasi instantannée, et avec quasiment aucune ressource serveur.';
$_MODULE['<{totalshoppingexporter}prestashop>settings_1d0eaa3d97752922ee0c23574a444753'] = 'La tâche planifiée peut-être executée entre toute les heures jusqu\'à une fois par semaines, en fonction de la fréquence des changements des produits dans votre base de donnée.';
$_MODULE['<{totalshoppingexporter}prestashop>settings_989ec7521ae0a9a81ee867fc719e42a7'] = 'Bien entendu, plus vous appelez le script, plus les résultats seront précis, mais plus votre base de donnée sera impactée en terme de performance pendant la génération du fichier.';
$_MODULE['<{totalshoppingexporter}prestashop>settings_a6fc43a25f5055b5c0030ca21cedc6d4'] = 'Nous recommandons que vous adaptiez la fréquence en fonction de votre hébergeur.';
$_MODULE['<{totalshoppingexporter}prestashop>settings_2249d0128d0467959f3cdf13f1caa9f4'] = 'Nous recommandons d\'exécuter la tâche planifiée toute les 6 heures, ce qui veux dire que le flux produit sera rafraichit 4 fois par jours, ce qui est plus que suffisant pour les comparateurs.';
$_MODULE['<{totalshoppingexporter}prestashop>settings_40bfc4e7b3501faf01baa0399fffb22b'] = 'Voici l\'adresse URL pour la tâche planifiée :';
$_MODULE['<{totalshoppingexporter}prestashop>settings_2588db254f60694863dc706583cc6a0f'] = 'Et voici la ligne à ajouter dans votre fichier Crontab :';
$_MODULE['<{totalshoppingexporter}prestashop>settings_9495d693c5abf81296f37c7cb520da2e'] = '(NOTE : Il se peux que vous ailliez à remplacer \"php\" par \"php5\", \"php-cli\" ou \"php5-cli\" en fonction de l\'installation de votre serveur.)';
$_MODULE['<{totalshoppingexporter}prestashop>settings_923c9ce3ca1bc1bde9f2da0146314608'] = 'Vous pouvez choisir l\'un des deux, en fonction de vos besoins.';
$_MODULE['<{totalshoppingexporter}prestashop>settings_e2ec802fa95f6386fe82778618ac484d'] = 'Vous devez définir un mapping pour chaque language vous souhaitez exporter afin d\'obtenir un meilleur résultat dans les comparateurs.';
$_MODULE['<{totalshoppingexporter}prestashop>settings_85a9647f82eee80d6c3734abef5b1c56'] = 'Rechercher une catégorie à joindre';
$_MODULE['<{totalshoppingexporter}prestashop>settings_4994a8ffeba4ac3140beb89e8d41f174'] = 'Langue';
$_MODULE['<{totalshoppingexporter}prestashop>settings_e6fe2cb291ace4c56d3f9481b3b963af'] = 'Sélectionnez une langue';
$_MODULE['<{totalshoppingexporter}prestashop>settings_93ec6008c24122850b4c63bdb0ddfda5'] = 'Vous devez indiquer une langue dans laquelle vous souhaitez exporter votre catalogue.';
$_MODULE['<{totalshoppingexporter}prestashop>settings_a8aa26309afe36e9d960fe6a5b5397f3'] = 'L\'inconvénient d\'avoir plusieurs comparateurs d\'activé est qu\'ils ont leurs propres catégories.';
$_MODULE['<{totalshoppingexporter}prestashop>settings_8e7660f729ce87381c4e5e2f7fe00248'] = 'Vous devrez donc effectuer le mapping pour chaque comparateur vers lequel vous souhaitez exporter votre catalogue.';
$_MODULE['<{totalshoppingexporter}prestashop>settings_cf258d612e89d058db454714a3ea8cca'] = 'C\'est un travail fastidieux, mais heureusement, c\'est quelque chose que vous n\'aurez qu\'a faire qu\'une seule fois :)';
$_MODULE['<{totalshoppingexporter}prestashop>settings_bff3cfa6cb1a8c7e75bbf8bd61de0de3'] = 'Stock a afficher quand vide';
$_MODULE['<{totalshoppingexporter}prestashop>settings_31746d9e526ddd03512aee370140a9d5'] = 'Taille de la description';
$_MODULE['<{totalshoppingexporter}prestashop>settings_c8b67271027c00996a585be47a0c0967'] = 'Taille des images';
$_MODULE['<{totalshoppingexporter}prestashop>settings_b893d4c0e3d049d0caca26a4107dae16'] = 'Condition du produit';
$_MODULE['<{totalshoppingexporter}prestashop>settings_04b4cdff1f90ea73fc97e350c3869cf0'] = 'Durée de mise en cache';
$_MODULE['<{totalshoppingexporter}prestashop>settings_fc3b50c94ad40a59741d6af9f0c735c7'] = 'La durée maximale que le flux de produit généré est gardé en cache. Défaut à 72H';
$_MODULE['<{totalshoppingexporter}prestashop>settings_b0847c27b882e9989702790086d2508c'] = 'Inclure Genre (sexe)';
$_MODULE['<{totalshoppingexporter}prestashop>settings_f2ef60cff2ce71205809e8b806062f53'] = 'Inclure la couleur';
$_MODULE['<{totalshoppingexporter}prestashop>settings_07249395f92e18fb08fdb7d75fd74f88'] = 'Inclure la taille';
$_MODULE['<{totalshoppingexporter}prestashop>settings_03670fc4a04eff75184b7d15fa4172de'] = 'Inclure le motif';
$_MODULE['<{totalshoppingexporter}prestashop>settings_ca3d7ea313437592e9690ccff7699677'] = 'Inclure le matériaux';
$_MODULE['<{totalshoppingexporter}prestashop>settings_ee5ebd05906cdf6ae230483207dd4f89'] = 'Group d\'age';
$_MODULE['<{totalshoppingexporter}prestashop>settings_98f770b0af18ca763421bac22b4b6805'] = 'Caractéristiques';
$_MODULE['<{totalshoppingexporter}prestashop>settings_3b0649c72650c313a357338dcdfb64ec'] = 'Note';
$_MODULE['<{totalshoppingexporter}prestashop>settings_9ca71196b138a1067dd8fbaaba984ac9'] = 'Vous devez être compatible avec les';
$_MODULE['<{totalshoppingexporter}prestashop>settings_f8ed54944df2dee54bc2c51d1aa3581e'] = 'règles de Google Shopping';
$_MODULE['<{totalshoppingexporter}prestashop>settings_d356939fd638bfa6c181a1595f49c054'] = 'concernant la déclinaison "Group d\'age"';

$_MODULE['<{totalshoppingexporter}prestashop>settings_703d43e1437966ede0de1d06c824c3de'] = 'Produits à exclure';
$_MODULE['<{totalshoppingexporter}prestashop>settings_510eaefbd60bae7431c48c10503223e7'] = 'Vous pouvez indiquer ici les ID des produits que vous souhaitez exclure de l\'export.';
$_MODULE['<{totalshoppingexporter}prestashop>settings_18eb3cee9dd5b6ae3703c4c167242950'] = 'Veuillez indiquer un ID par ligne.';
$_MODULE['<{totalshoppingexporter}prestashop>settings_198579b21921de6eb994ac4ad6fdfa87'] = 'Exclure ces produits';
$_MODULE['<{totalshoppingexporter}prestashop>settings_0738d28e6560aad5c40639ad73eaabbb'] = 'Veuillez indiquer un ID produit par ligne que vous souhaitez exclure de l\'export.';
$_MODULE['<{totalshoppingexporter}prestashop>settings_'] = '';

$_MODULE['<{totalshoppingexporter}prestashop>settings_fe5497271b540b72471fb176acdccaa4'] = 'Si vous rencontrez des problèmes ou des bugs, n\'hésitez pas à nous contactez via le site Addons de Prestashop';
$_MODULE['<{totalshoppingexporter}prestashop>settings_298b1b245b0fab5c72e316eb73679d14'] = 'Nous ferons notre mieux pour y répondre le plus rapidement possible';
$_MODULE['<{totalshoppingexporter}prestashop>settings_f35e5691db1494c04a0d5887d8065b38'] = 'Cliquez-ici pour nous contacter.';

$_MODULE['<{totalshoppingexporter}prestashop>settings_68f31affe752519f3198119eabcdf374'] = 'Si nécessaire, vous pouvez re-générer un nouveau token sécurisé.';
$_MODULE['<{totalshoppingexporter}prestashop>settings_6844fa40d8f1cf81d63b3f64422dd673'] = 'IMPORTANT: Si vous avez déjà partagé votre lien d\'export vers le moteur de comparaison, vous devrez renvoyer le nouveau lien car l\'ancien ne fonctionnera plus et sera considéré comme invalide.';
$_MODULE['<{totalshoppingexporter}prestashop>settings_b4e3781d20fbc92a4f46ee6cdc2e750f'] = '(C\'est une mesure de sécurité)';
$_MODULE['<{totalshoppingexporter}prestashop>settings_60adfcd59119433b68011a5ea495d12c'] = 'Regénérer un nouveau token';

$_MODULE['<{totalshoppingexporter}prestashop>shoppingexportergenerator_e0858a97eebba9c42dacc0fa9fc6b32f'] = 'Aucun produit n\'a été trouvé avec cette configuration. Les catégories que vous avez configuré ont-elles bien des produits? Avez-vous coché les bonnes options en rapprt avec votre catalogue de produits?';

$_MODULE['<{totalshoppingexporter}prestashop>step2_a2cd251ae208086d2d2ec1d9ac41d408'] = 'Afin d\'optimiser votre visibilitée dans le moteur de recherche, veuillez effectuer le mapping de vos catégories avec celles du moteur de comparaison.';
$_MODULE['<{totalshoppingexporter}prestashop>step2_bb34a159a88035cce7ef1607e7907f8f'] = 'Nom de la catégorie';
$_MODULE['<{totalshoppingexporter}prestashop>step2_0014f8b96d996135614c21355a053994'] = 'Mapping';
