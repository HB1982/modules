<?php
/**
 * Total Shopping Exporter
 *
 * This modules export your products catalog to many comparison websites like Google shopping, LeGuide, Shopzilla and many others.
 *
 * If you find errors, bugs or if you want to share some improvments, feel free to contact at contact@prestaddons.net ! :)
 * Si vous trouvez des erreurs, des bugs ou si vous souhaitez tout simplement partager un conseil ou une amélioration,
 * n'hésitez pas à me contacter à contact@prestaddons.net
 *
 * @author    Cyril Nicodème <contact@prestaddons.net>
 * @copyright Copyright (C) June 2014 cnicodeme.com <email:contact@prestaddons.net>. All rights reserved.
 * @license   Nicodème Cyril
 * @since     2014-06-18
 * @package   modules
 * @version   2.6.5
 */

class SimpleXMLExtendedExporter extends SimpleXMLElement
{
    public function addCData($node_name, $value, $ns = null)
    {
        $node = $this->addChild($node_name, null, $ns);
        $node = dom_import_simplexml($node);
        $no   = $node->ownerDocument;
        $node->appendChild($no->createCDATASection($value));
    }
}
