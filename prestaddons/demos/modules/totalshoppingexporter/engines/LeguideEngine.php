<?php
/**
 * Total Shopping Exporter
 *
 * This modules export your products catalog to many comparison websites like Google shopping, LeGuide, Shopzilla and many others.
 *
 * If you find errors, bugs or if you want to share some improvments, feel free to contact at contact@prestaddons.net ! :)
 * Si vous trouvez des erreurs, des bugs ou si vous souhaitez tout simplement partager un conseil ou une amélioration,
 * n'hésitez pas à me contacter à contact@prestaddons.net
 *
 * @author    Cyril Nicodème <contact@prestaddons.net>
 * @copyright Copyright (C) June 2014 cnicodeme.com <email:contact@prestaddons.net>. All rights reserved.
 * @license   Nicodème Cyril
 * @since     2014-06-18
 * @package   modules
 * @version   2.6.5
 */

class LeguideEngine
{
    public static function getName()
    {
        return "LeGuide Shopping";
    }

    public static function hasTaxonomies()
    {
        return false;
    }

    public static function setHeaders($shopId)
    {
        $shopName = Configuration::get('PS_SHOP_NAME', null, null, $shopId);
        $shopName = Tools::strtolower(trim($shopName));
        $shopName = preg_replace('/[^a-z0-9-]/', '_', $shopName);
        $shopName = preg_replace('/_+/', "_", $shopName);

        header('Content-Type: text/xml; name="'.$shopName.'_leguide.xml"');
        header('Content-Disposition: inline; filename="'.$shopName.'_leguide.xml"');
    }

    public static function export($filepath, $shop_id, $lang_id, $products, $options)
    {
        $root = new SimpleXMLExtendedExporter('<?xml version="1.0" encoding="UTF-8" ?><products></products>');
        $root->addAttribute('lang', Tools::strtoupper(Context::getContext()->language->iso_code));
        $root->addAttribute('date', date('Y-m-d H:i'));
        $gmt = date('P');
        $root->addAttribute('GMT', Tools::substr($gmt, 0, strpos($gmt, ':')));

        $product_place = 1;
        $conditionState = array('new', 'used', 'refurbished');
        foreach ($products as $product) {
            if (count($product['images']) === 0) {
                $product['images'] = array ('N/A');
            }

            $item = $root->addChild('product');
            $item->addCData('offer-id', $product['id']);
            $item->addCData('title', $product['title']);
            $item->addCData('product-url', $product['link']);
            $item->addCData('landing-page-url', $product['link']);
            $item->addCData('price', $product['price']);
            $item->addChild('currency', $product['currency']['code']);
            $item->addCData('brand', $product['brand']);
            $item->addCData('description', Tools::substr(strip_tags($product['description']), 0, 300)); // Max 300, no HTML tags
            $item->addCData('image-url', $product['images'][0]);
            foreach ($product['image'] as $key=>$img) {
                if ($key === 0) continue;
                $item->addCData('image-url-' + ($key + 1), $product['images'][$key]);

                if ($key >= 4) break;
            }

            $item->addCData('ean', $product['ean13']);
            $item->addCData('mpn', $product['mpn']);
            $item->addCData('merchant-category', $product['category_name']);
            $item->addChild('kelkoo-category-id', '');

            if ($product['availability'] === 'in stock') {
                $item->addCData('availability', 1);
            }
            else if ($product['availability'] === 'available for order') {
                $item->addCData('availability', 5);
            } else if ($product['availability'] === 'preorder') {
                $item->addCData('availability', 4);
            }

            $item->addCData('stock-quantity', $product['quantity']);
            $item->addCData('delivery-cost', $product['shipping']['price']);
            $item->addCData('delivery-time', ((int)$options['delivery_schedule'] * 24).' hours');

            $item->addCData('condition', array_search($product['condition'], $conditionState));
            $item->addCData('ecotax', $product['ecotax']);
            $item->addChild('shipping-weight', $product['shipping_weight'].' '.$product['weight_unit']);

            if ($product['original_price'] != $product['price']) {
                $item->addCData('price-no-rebate', $product['original_price']);
            }
        }

        file_put_contents($filepath, $root->asXML());
        return true;
    }
}
