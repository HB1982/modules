<?php
/**
 * Total Shopping Exporter
 *
 * This modules export your products catalog to many comparison websites like Google shopping, LeGuide, Shopzilla and many others.
 *
 * If you find errors, bugs or if you want to share some improvments, feel free to contact at contact@prestaddons.net ! :)
 * Si vous trouvez des erreurs, des bugs ou si vous souhaitez tout simplement partager un conseil ou une amélioration,
 * n'hésitez pas à me contacter à contact@prestaddons.net
 *
 * @author    Cyril Nicodème <contact@prestaddons.net>
 * @copyright Copyright (C) June 2014 cnicodeme.com <email:contact@prestaddons.net>. All rights reserved.
 * @license   Nicodème Cyril
 * @since     2014-06-18
 * @package   modules
 * @version   2.6.5
 */

class GoogleEngine
{
    public static function getName()
    {
        return "Google Shopping";
    }

    public static function hasTaxonomies()
    {
        return true;
    }

    public static function getCategories($lang)
    {
        $lang = Language::getLanguageCodeByIso(Language::getIsoById($lang));
        $lang = Tools::substr($lang, 0, 3).Tools::strtoupper(Tools::substr($lang, 3, 2)); // lower-UPPER

        $headers = @get_headers('http://www.google.com/basepages/producttype/taxonomy.'.$lang.'.txt');
        if ($headers !== false && strpos($headers[0], '404') !== false) {
            $lang = 'en-US';
        }

        $file_path = _PS_CACHE_DIR_.'tse_google_shopping_taxonomies_'.$lang.'.txt';
        if (!is_file($file_path)) {
            if (ini_get('allow_url_fopen')) {
                if (file_put_contents($file_path, Tools::file_get_contents('http://www.google.com/basepages/producttype/taxonomy.'.$lang.'.txt')) === false) {
                    exit(Tools::jsonEncode('An error occured while trying to download the taxonomies.'));
                }

                @chmod($file_path, 0644);
            } else {
                exit('You need to enable allow_url_fopen to be able to download the taxonomies.');
            }
        }

        $content = Tools::file_get_contents($file_path);
        $parts = explode("\n", $content);
        array_shift($parts);

        $result = array ();
        foreach ($parts as $part) {
            $result[] = array (
                'id' => $part,
                'text' => $part
            );
        }

        return $result;
    }

    public static function setHeaders($shopId)
    {
        $shopName = Configuration::get('PS_SHOP_NAME', null, null, $shopId);
        $shopName = Tools::strtolower(trim($shopName));
        $shopName = preg_replace('/[^a-z0-9-]/', '_', $shopName);
        $shopName = preg_replace('/_+/', "_", $shopName);

        header('Content-Type: text/xml; name="'.$shopName.'_google.xml"');
        header('Content-Disposition: inline; filename="'.$shopName.'_google.xml"');
    }

    public static function export($filepath, $shop_id, $lang_id, $products, $options)
    {
        $root = new SimpleXMLExtendedExporter('<?xml version="1.0" encoding="UTF-8" ?><rss></rss>');
        $root->addAttribute('version', '2.0');
        $root->addAttribute('xmlns:xmlns:g', 'http://base.google.com/ns/1.0');
        $channel = $root->addChild('channel');
        $channel->addChild('title', 'Google Shopping export for PrestaShop');

        foreach ($products as $product) {
            $item = $channel->addChild('item');
            $item->addChild('g:id', $product['id'], 'http://base.google.com/ns/1.0');
            $item->addCData('title', $product['title']);
            $item->addCData('link', $product['link']);
            $item->addCData('description', $product['description']);

            $item->addChild('g:quantity', (int)$product['quantity'], 'http://base.google.com/ns/1.0');
            $item->addCData('g:availability', $product['availability'], 'http://base.google.com/ns/1.0');

            $item->addChild('g:price', $product['price'].' '.$product['currency']['code'], 'http://base.google.com/ns/1.0');

            $item->addCData('g:product_type', $product['product_type'], 'http://base.google.com/ns/1.0');
            $item->addCData('g:google_product_category', $product['google_category'], 'http://base.google.com/ns/1.0');

            $item->addChild('g:shipping_weight', $product['shipping_weight'].' '.$product['weight_unit'], 'http://base.google.com/ns/1.0');
            $item->addChild('g:online_only', $product['online_only'], 'http://base.google.com/ns/1.0');

            $item->addChild('g:condition', htmlspecialchars($product['condition']), 'http://base.google.com/ns/1.0');
            $item->addChild('g:brand', (is_null($product['brand']) ? 'unknown' : htmlspecialchars($product['brand'])), 'http://base.google.com/ns/1.0');

            $item->addChild('g:mpn', !is_null($product['mpn']) ? $product['mpn'] : '', 'http://base.google.com/ns/1.0');

            if (!is_null($product['ean13'])) {
                $item->addChild('g:gtin', $product['ean13'], 'http://base.google.com/ns/1.0');
            } elseif (!is_null($product['upc'])) {
                $item->addChild('g:gtin', $product['upc'], 'http://base.google.com/ns/1.0');
            } else {
                $item->addChild('g:gtin', '', 'http://base.google.com/ns/1.0');
            }

            if (is_null($product['brand']) || is_null($product['mpn']) || is_null($product['ean13'])) {
                $item->addChild('g:identifier_exists', 'FALSE');
            }

            if (count($product['images']) > 0) {
                $image_index = 0;
                foreach ($product['images'] as $image) {
                    if ($image_index === 0) {
                        $item->addCData('g:image_link', $image, 'http://base.google.com/ns/1.0');
                    } else {
                        $item->addCData('g:additional_image_link', $image, 'http://base.google.com/ns/1.0');
                    }

                    $image_index++;
                }
            }

            // Attributes :
            if (isset($product['attributes']['color'])) {
                $item->addCData('g:color', $product['attributes']['color'], 'http://base.google.com/ns/1.0');
            }

            if (isset($product['attributes']['gender'])) {
                $gender = Tools::strtolower($product['attributes']['gender']);
                if (!in_array($gender, array('male', 'female', 'unisex'))) {
                    if (Tools::substr($gender, 0, 1) === 'f') {
                        $gender = 'female';
                    } elseif (Tools::substr($gender, 0, 1) === 'm') {
                        $gender = 'male';
                    } else {
                        $gender = 'unisex';
                    }
                }

                $item->addCData('g:gender', $gender, 'http://base.google.com/ns/1.0');
            }

            if (isset($product['attributes']['size'])) {
                $item->addCData('g:size', $product['attributes']['size'], 'http://base.google.com/ns/1.0');
            }

            if (isset($product['attributes']['pattern'])) {
                $item->addCData('g:pattern', $product['attributes']['pattern'], 'http://base.google.com/ns/1.0');
            }

            if (isset($product['attributes']['material'])) {
                $item->addCData('g:material', $product['attributes']['material'], 'http://base.google.com/ns/1.0');
            }

            if (isset($product['attributes']['agegrp'])) {
                $item->addCData('g:age_group', Tools::strtolower($product['attributes']['agegrp']), 'http://base.google.com/ns/1.0');
            }

            $ship = $item->addChild('g:shipping', null, 'http://base.google.com/ns/1.0');
            $ship->addChild('g:country', $product['shipping']['country'], 'http://base.google.com/ns/1.0');
            $ship->addCData('g:service', $product['shipping']['service'], 'http://base.google.com/ns/1.0');
            $ship->addChild('g:price', $product['shipping']['price'].' '.$product['currency']['code'], 'http://base.google.com/ns/1.0');
        }

        file_put_contents($filepath, $root->asXML());
        return true;
    }
}
