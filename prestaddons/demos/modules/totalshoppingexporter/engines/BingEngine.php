<?php
/**
 * Total Shopping Exporter
 *
 * This modules export your products catalog to many comparison websites like Google shopping, LeGuide, Shopzilla and many others.
 *
 * If you find errors, bugs or if you want to share some improvments, feel free to contact at contact@prestaddons.net ! :)
 * Si vous trouvez des erreurs, des bugs ou si vous souhaitez tout simplement partager un conseil ou une amélioration,
 * n'hésitez pas à me contacter à contact@prestaddons.net
 *
 * @author    Cyril Nicodème <contact@prestaddons.net>
 * @copyright Copyright (C) June 2014 cnicodeme.com <email:contact@prestaddons.net>. All rights reserved.
 * @license   Nicodème Cyril
 * @since     2014-06-18
 * @package   modules
 * @version   2.6.5
 */

class BingEngine
{
    public static function getName()
    {
        return "Bing Shopping";
    }

    public static function hasTaxonomies()
    {
        return true;
    }

    public static function getCategories($lang)
    {
        $content = Tools::file_get_contents(dirname(__FILE__).'/../assets/bing.txt');
        $parts = explode("\n", $content);
        array_shift($parts);

        $result = array ();
        foreach ($parts as $part) {
            $result[] = array (
                'id' => $part,
                'text' => $part
            );
        }

        return $result;
    }

    public static function setHeaders($shopId)
    {
        $shopName = Configuration::get('PS_SHOP_NAME', null, null, $shopId);
        $shopName = Tools::strtolower(trim($shopName));
        $shopName = preg_replace('/[^a-z0-9-]/', '_', $shopName);
        $shopName = preg_replace('/_+/', "_", $shopName);

        header('Content-Type: text/csv; name="'.$shopName.'_bing.txt"');
        header('Content-Disposition: inline; filename="'.$shopName.'_bing.txt"');
    }

    private static function cleanCsv($value)
    {
        $value = strip_tags($value);
        $value = str_replace("\n", '', $value);
        $value = str_replace("\r", '', $value);

        return $value;
    }

    public static function export($filepath, $shop_id, $lang_id, $products, $options)
    {
        $columns = array ('id', 'title', 'brand', 'link', 'price', 'description', 'image_link', 'mpn', 'gtin', 'product_category', 'shipping');

        $resource = fopen($filepath, 'wb');
        fputcsv($resource, $columns, "\t");

        foreach ($products as $product) {
            if (count($product['images']) === 0) {
                $product['images'] = array ('N/A');
            }

            $line = array (
                $product['id'],
                self::cleanCsv($product['title']),
                $product['brand'],
                $product['link'],
                $product['price'],
                self::cleanCsv($product['description']),
                $product['images'][0],
                $product['mpn'],
                (!empty($product['upc']) ? $product['upc'] : $product['ean13']),
                $product['bing_category'],
                $product['shipping']['price']
            );

            fputcsv($resource, $line, "\t");
        }

        fclose($resource);
        return true;
    }
}
