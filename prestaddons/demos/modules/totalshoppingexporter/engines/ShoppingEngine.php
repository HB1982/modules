<?php
/**
 * Total Shopping Exporter
 *
 * This modules export your products catalog to many comparison websites like Google shopping, LeGuide, Shopzilla and many others.
 *
 * If you find errors, bugs or if you want to share some improvments, feel free to contact at contact@prestaddons.net ! :)
 * Si vous trouvez des erreurs, des bugs ou si vous souhaitez tout simplement partager un conseil ou une amélioration,
 * n'hésitez pas à me contacter à contact@prestaddons.net
 *
 * @author    Cyril Nicodème <contact@prestaddons.net>
 * @copyright Copyright (C) June 2014 cnicodeme.com <email:contact@prestaddons.net>. All rights reserved.
 * @license   Nicodème Cyril
 * @since     2014-06-18
 * @package   modules
 * @version   2.6.5
 */

class ShoppingEngine
{
    public static function getName()
    {
        return "Shopping.com";
    }

    public static function hasTaxonomies()
    {
        return true;
    }

    public static function getCategories($lang)
    {
        $taxonomies = include_once(dirname(__FILE__).'/../assets/shopping.php');

        $result = array ();
        foreach ($taxonomies as $taxonomy) {
            $result[] = array (
                'id' => $taxonomy['id'].'-'.$taxonomy['name'],
                'text' => $taxonomy['name']
            );
        }

        return $result;
    }

    public static function setHeaders($shopId)
    {
        $shopName = Configuration::get('PS_SHOP_NAME', null, null, $shopId);
        $shopName = Tools::strtolower(trim($shopName));
        $shopName = preg_replace('/[^a-z0-9-]/', '_', $shopName);
        $shopName = preg_replace('/_+/', "_", $shopName);

        header('Content-Type: text/xml; name="'.$shopName.'_shopping.com.xml"');
        header('Content-Disposition: inline; filename="'.$shopName.'_shopping.com.xml"');
    }

    public static function export($filepath, $shop_id, $lang_id, $products, $options)
    {
        $root = new SimpleXMLExtendedExporter('<?xml version="1.0" encoding="UTF-8" ?><Products></Products>');

        foreach ($products as $product) {
            if (count($product['images']) === 0) {
                $product['images'] = array ('N/A');
            }

            $item = $root->addChild('Product');
            $item->addChild('Merchant_SKU', $product['id']);
            $item->addCData('Product_Name', $product['title']);
            $item->addCData('Product_URL', $product['link']);
            $item->addCData('Image_URL', $product['images'][0]);
            $item->addChild('Current_Price', $product['currency']['sign'].$product['price']);
            $item->addChild('Original_Price', $product['currency']['sign'].$product['price']);

            $item->addChild('Shipping_Rate', $product['currency']['sign'].$product['shipping']['price']);
            $item->addCData('Stock_Availability', ($product['quantity'] > 0 ? 'Yes' : $product['availability']));
            $item->addCData('Condition', $product['condition']);
            $item->addChild('UPC', $product['upc']);
            $item->addChild('EAN', $product['ean13']);
            $item->addChild('MPN', $product['mpn']);
            $item->addCData('Manufacturer', $product['brand']);
            $item->addCData('Product_Description', $product['description']);
            $item->addCData('Product_Type', $product['category']);

            $category = explode('-', $product['shopping_category'], 2);
            $item->addCData('Category', $category[1]);
            $item->addCData('Category_ID', $category[0]);

            if (isset($product['attributes']['color'])) {
                $item->addCData('color', $product['attributes']['color']);
            }

            if (isset($product['attributes']['size'])) {
                $item->addCData('size', $product['attributes']['size']);
            }

            if (isset($product['attributes']['material'])) {
                $item->addCData('material', $product['attributes']['material']);
            }
        }

        file_put_contents($filepath, $root->asXML());
        return true;
    }
}
