<?php
/**
 * Total Shopping Exporter
 *
 * This modules export your products catalog to many comparison websites like Google shopping, LeGuide, Shopzilla and many others.
 *
 * If you find errors, bugs or if you want to share some improvments, feel free to contact at contact@prestaddons.net ! :)
 * Si vous trouvez des erreurs, des bugs ou si vous souhaitez tout simplement partager un conseil ou une amélioration,
 * n'hésitez pas à me contacter à contact@prestaddons.net
 *
 * @author    Cyril Nicodème <contact@prestaddons.net>
 * @copyright Copyright (C) June 2014 cnicodeme.com <email:contact@prestaddons.net>. All rights reserved.
 * @license   Nicodème Cyril
 * @since     2014-06-18
 * @package   modules
 * @version   2.6.5
 */

class ShopzillaEngine
{
    public static function getName()
    {
        return "Shopzilla";
    }

    public static function hasTaxonomies()
    {
        return true;
    }

    public static function getCategories($lang)
    {
        $taxonomies = include_once(dirname(__FILE__).'/../assets/shopping.php');

        $result = array ();
        foreach ($taxonomies as $taxonomy) {
            $result[] = array (
                'id' => $taxonomy['id'].'-'.$taxonomy['name'],
                'text' => $taxonomy['name']
            );
        }

        return $result;
    }

    public static function setHeaders($shopId)
    {
        $shopName = Configuration::get('PS_SHOP_NAME', null, null, $shopId);
        $shopName = Tools::strtolower(trim($shopName));
        $shopName = preg_replace('/[^a-z0-9-]/', '_', $shopName);
        $shopName = preg_replace('/_+/', "_", $shopName);

        header('Content-Type: text/csv; name="'.$shopName.'_shopzilla.tab"');
        header('Content-Disposition: inline; filename="'.$shopName.'_shopzilla.tab"');
    }

    private static function cleanCsv($value)
    {
        $value = strip_tags($value);
        $value = str_replace("\n", '', $value);
        $value = str_replace("\r", '', $value);

        return $value;
    }

    public static function export($filepath, $shop_id, $lang_id, $products, $options)
    {
        $columns = array ('Category ID', 'Manufacturer', 'Title', 'Description', 'Product URL', 'Image URL', 'SKU', 'Availability', 'Condition', 'Ship Weight', 'Ship Cost', 'Bid', 'Promotional Code', 'UPC', 'Price');

        $resource = fopen($filepath, 'wb');
        fputcsv($resource, $columns, "\t");

        foreach ($products as $product) {
            if (count($product['images']) === 0) {
                $product['images'] = array ('N/A');
            }

            $categories = explode('-', $product['shopzilla_category']);

            $line = array (
                $categories[0],
                $product['brand'],
                self::cleanCsv($product['title']),
                self::cleanCsv($product['description']),
                $product['link'],
                $product['images'][0],
                $product['id'],
                $product['availability'],
                $product['condition'],
                '',
                '',
                '',
                '',
                $product['upc'],
                $product['price'],
            );

            fputcsv($resource, $line, "\t");
        }

        fclose($resource);
        return true;
    }
}
