Installazione
============

L'installazione di questo modulo è abbastanza facile.
Tutto quello che dovete fare è andare nel vostro moduli elenco nel tuo backoffice Prestashop, fate clic su "Aggiungi un nuovo modulo", selezionare l'archivio e voilà! :)

Una volta installato, sarà necessario configurarlo.
Normalmente, si dovrebbe essere reindirizzati alla pagina di configurazione di questo modulo, ma nel caso non lo è,
andare a moduli, cercare "Total Shopping esportatore" e fare clic su "Configura collegamento".

Configurazione
=============

La pagina di configurazione viene visualizzata con più schede.
Ogni linguette rappresentano un aspetto della configurazione. Andremo attraverso ogni schede.


Generico
-------

Questa scheda elenca gli URL da copiare / incollare il catalogo prodotti per i siti web di confronto.

Dopo la prima installazione, questa scheda dovrebbe essere abbastanza vuoto.
Sarà necessario configurare questo modulo per avere questa scheda mostrano maggiori informazioni.

Per configurare un nuovo esportazione, è sufficiente fare clic sul pulsante che si trova a destra,
"Aggiungere una nuova lingua da esportare", e seguire le istruzioni (spiegheremo loro dopo).


Aggiungere una nuova lingua da esportare 1/2
--------------------------------

Il modulo permette di scegliere quale motore utilizzare, e molti altri campi da configurare.
È possibile scegliere la lingua, la moneta, il trasportatore e il numero di giorni necessari per questo
vettore per spedire i vostri prodotti.
Poi, si dovrà scegliere quali categorie si desidera esportare.

IMPORTANTE: Si prega di tenere presente che il modulo di esportare i prodotti che hanno selezionato "categoria principale".
Ad esempio, se il prodotto è nelle categorie "IPod" e "Accessori", con i "Ipod" è la categoria principale,
se si seleziona solo "Accessori", questo prodotto non sarà esportato, perché è categoria principale non è "Accessori"
ma "IPod"!


Aggiungere una nuova lingua da esportare 2/2
--------------------------------
La seconda pagina è la mappatura. E 'una parte importante perché sarà mappare i categorie per le categorie del motore.
Ad esempio, si può avere una categoria chiamata "Laptop", ma, a Google Shopping per esempio, hanno "portatili Mac" e "computer portatili PC".
Al fine di aumentare le vendite, è meglio indirizzare la categoria giusta al motore di shopping.

Questa è la ragione del mapping: bisogna indicare in quali categorie dal motore negozi, le vostre categorie in forma migliore.
È possibile scegliere di ignorare questo punto, qualche motore di shopping non li richiedono, eccetto Google Shopping (per exemple).

Ora, è sempre meglio mappare le categorie: le vendite aumenteranno!


Opzioni
-------

In questa scheda, si avrà la possibilità di specificare quali prodotti da esportare, come esclusi i prodotti senza descrizione, riferimento, marca, ecc
Avrete anche la possibilità di indicare l'elemento portante di default, in modo da lasciare il nostro modulo di calcolare correttamente il prezzo di spedizione del prodotto.


Attributi
----------

I vostri prodotti hanno attributi, come il colore, dimensione, materiale. Questi attributi sono utilizzati in alcuni motori di confronto come Google lo shopping, ed è meglio usarli.
Noi li consideriamo importante! Questo il motivo per cui abbiamo incluso questa scheda. Potrai definire quale attribuiscono mappare la funzione corrispondente per migliorare i risultati delle perquisizioni di confronto.


Prodotti da escludere
-------------------

Questa sezione permette di indicare alcuni prodotti ID non si desidera esportare per qualsiasi motivo.
Qualsiasi ID che è indicata qui verrà ignorato dal nostro strumento esportatore.


Cron
----

Il nostro modulo è dotato di uno script che genera tutti i file nella cache Prestashop al fine di ridurre la carica del server quando il motore di comparazione ha colpito l'url dei vostri prodotti feed.
Questa scheda è qui per spiegarvi come funziona e perché lo deve usare.

Vi consigliamo vivamente di implementare un processo di cron per questo modulo perché sarà chiaramente migliorare le prestazioni generali.


Contatto
=======

Si trova un bug, avete alcuni problemi la configurazione / installazione / rendendo Questo modulo funziona?
Non esitate a contattarmi via Prestashop: https://addons.prestashop.com/en/write-to-developper?alternative=1&id_product=17163

Farò del mio meglio per aiutarvi.


Grazie!
========

Hai acquistato il mio modulo, e per questo, ho voluto grazie!
Spero che troverete quello che vi serve.
