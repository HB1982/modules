Installation
============

L'installation de ce module est assez facile.
Tout ce que vous avez à faire est d'aller dans votre fiche modules dans votre backoffice de Prestashop, cliquez sur "Ajouter un nouveau module", sélectionnez l'archive et voilà! :)

Une fois installé, vous aurez à configurer.
Normalement, vous devriez être redirigé vers la page de configuration de ce module, mais dans le cas contraire,
Allez dans Modules, recherchez «montant de la commande Exporter" et cliquez sur le lien "Configurer".


Configuration
=============

La page de configuration apparaît avec plusieurs onglets.
Chaque onglets constituent un aspect de la configuration. Nous allons passer en revue tous les onglets.


Général
-------

Cet onglet liste les URL à copier / coller votre catalogue de produits pour les sites de comparaison.

Après la première installation, cet onglet devrait être assez vide.
Vous devrez configurer ce module afin d'avoir cet onglet montrer plus d'informations.

Pour configurer une nouvelle exportation, cliquez simplement sur le bouton situé à droite,
"Ajouter une nouvelle langue à exporter", et suivez les instructions (nous allons les expliquer après).


Ajouter une nouvelle langue à exporter 1/2
--------------------------------

Le formulaire vous permet de choisir quel moteur à utiliser, et de nombreux autres domaines à configurer.
Vous pouvez choisir la langue, la monnaie, le transporteur et le nombre de jours qu'il faut pour cette
transporteur pour expédier vos produits.
Ensuite, vous aurez à choisir les catégories que vous souhaitez exporter.

IMPORTANT: S'il vous plaît gardez à l'esprit que le module va exporter les produits qui ont la "catégorie principale" sélectionné.
Par exemple, si votre produit est dans les catégories "iPods" et "Accessoires", avec "iPods" est la catégorie principale,
Si vous sélectionnez uniquement "Accessoires", ce produit ne sera pas exportée, car il est la catégorie principale est pas "Accessoires"
mais "iPods"!


Ajouter une nouvelle langue à exporter 2/2
--------------------------------
La deuxième page est le mapping. Il est une partie importante car elle permettra de mapper vos catégories pour les catégories du moteur.
Par exemple, vous pouvez avoir une catégorie appelée "ordinateur portable", mais, à Google Shopping par exemple, ils ont des "ordinateurs portables Mac" et "PC portables".
Afin d'augmenter vos ventes, il est préférable de cibler la bonne catégorie au moteur de shopping.

Ceci est la raison du mapping: vous devez indiquer dans quelles catégories du moteur de shopping, vos catégories sont les plus adaptés.
Vous pouvez choisir d'ignorer cette étape, certains moteurs ne les oblige pas, à l'exception Google Shopping (par exemple).

Maintenant, il est toujours préférable de mapper vos catégories: vos ventes vont augmenter!


Options
-------

Dans cet onglet, vous aurez la possibilité de spécifier les produits à exporter, comme l'exclusion des produits sans description, référence, marque, etc.
Vous aurez également la possibilité de spécifier le support par défaut, afin de laisser notre module calculer le prix d'expédition de votre produit correctement.


Attributs
----------

Vos produits ont des attributs, comme la couleur, la taille, le matériel. Ces attributs sont utilisés dans certains moteurs de comparaison comme Google shopping, et il vaut mieux les utiliser.
Nous les considérons important! Ce pourquoi nous avons inclus cet onglet.
Vous allez définir quel attribut mapper la fonction correspondante pour améliorer les résultats dans les moteurs de comparaison.


Produits à exclure
-------------------

Cette section vous permet d'indiquer certains produits ID vous ne souhaitez pas exporter pour toutes les raisons.
Tous les ID qui est indiqué ici sera ignoré par notre outil de l'exportateur.


Cron
----

Notre module est livré avec un script qui générera tous les fichiers dans le cache Prestashop afin de réduire la charge de votre serveur lorsque le moteur de comparaison frappé l'url de vos produits de Flux.
Cet onglet est là pour vous expliquer comment cela fonctionne et pourquoi vous devriez l'utiliser.

Nous recommandons fortement de mettre en œuvre un cron pour ce module, car il permettra d'améliorer nettement la performance globale.


Contact
=======

Vous trouvez un bug, vous avez des problèmes de configuration / installation / mise Ce module fonctionne?
Sentez-vous libre de me contacter par Prestashop: https://addons.prestashop.com/en/write-to-developper?alternative=1&id_product=17163

Je ferai de mon mieux pour vous aider.


Merci !
========

Vous avez acheté mon module, et pour cela, je voulais vous remercier!
Je l'espère, vous trouverez ce qu'il vous faut.
