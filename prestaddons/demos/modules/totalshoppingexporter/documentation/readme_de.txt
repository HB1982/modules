Installation
============

Die Installation dieses Moduls ist recht einfach.
Alles was Sie tun müssen, ist, in Ihre Module Auflistung in Ihrem Prestashop Backoffice gehen, klicken Sie in "Hinzufügen eines neuen Moduls", wählen Sie das Archiv und voilà! :)

Einmal installiert, werden Sie haben, um es zu konfigurieren.
Normalerweise sollte man auf die Konfigurationsseite des Moduls umgeleitet werden, aber in dem Fall nicht so ist,
gehen Sie zu Module, suchen Sie nach "Total Einkaufs Exporteur" und klicken Sie auf die Schaltfläche "Configure Link".

Configuration
=============

Die Konfigurationsseite erscheint mit mehreren Registerkarten.
Jede Registerkarten stellen einen Aspekt der Konfiguration. Wir werden durch alle Registerkarten zu gehen.


Allgemein
-------

Diese Registerkarte eine Liste der URLs zu Ihren Produktkatalog zu den Vergleich Websites kopieren / einfügen.

Nach der ersten Installation sollte dieses Register ziemlich leer sein.
Sie müssen dieses Modul, um haben diese Registerkarte zeigen weitere Informationen zu konfigurieren.

Um einen neuen Export zu konfigurieren, einfach auf den Button auf der rechten Seite klicken,
Und folgen Sie den Anweisungen "eine neue Sprache zu exportieren Add" (wir werden sie nach der Erklärung).


Eine neue Sprache hinzufügen exportieren 1/2
--------------------------------

Das Formular können Sie auswählen, welche Engine zu verwenden, und vielen anderen Bereichen zu konfigurieren.
Sie können die Sprache, die Währung, den Träger und wie viele Tage es dauert, diese wählen
Träger, um Ihre Produkte zu versenden.
Dann müssen Sie entscheiden, welche Kategorien, die Sie exportieren möchten.

WICHTIG: Bitte beachten Sie, dass das Modul werden die Produkte, die die "Hauptkategorie" gewählt haben, zu exportieren.
Zum Beispiel, wenn Sie Ihr Produkt ist in den Kategorien "iPods" und "Zubehör", mit "iPods" ist die Hauptkategorie,
wenn Sie "Zubehör" wählen, wird dieses Produkt nicht exportiert werden, weil es Hauptkategorie ist nicht "Zubehör"
aber "iPods"!


Eine neue Sprache hinzufügen exportieren 2/2
--------------------------------
Die zweite Seite ist die Abbildung. Es ist ein wichtiger Teil, weil es Ihre Kategorien in die Kategorien des Motors zuzuordnen.
Zum Beispiel können Sie eine Kategorie namens "Laptop", aber bei Google Shopping zum Beispiel, haben sie "Mac-Laptops" und "PC-Laptops".
Um Ihren Umsatz zu steigern, ist es besser, die richtige Kategorie im Einkaufsmotor Ziel.

Dies ist der Grund der Zuordnung: Sie müssen in welche Kategorien von der Einkaufs Motor, passen Sie Ihren besten Kategorien anzuzeigen.
Sie können wählen, um diesen Schritt, einige Shopping-Engine erfordern nicht, sie zu ignorieren, ausgenommen Google Shopping (zum Beispiel).

Nun, es ist immer besser, Ihre Kategorien zuzuordnen: Ihren Umsatz zu steigern!


Optionen
-------

In diesem Register wird Ihnen die Möglichkeit, festzulegen, welche Produkte für den Export, wie ausgenommen Waren ohne Beschreibung, Referenz, Marke, etc. haben
Sie haben auch die Möglichkeit, die Standard-Träger specifiy, um unser Modul berechnen Sie die Versandkosten für Ihr Produkt richtig zu lassen.


Attribute
----------

Ihre Produkte haben Eigenschaften, wie Farbe, Größe, Material. Diese Attribute werden in einigen Vergleichs Suchmaschinen wie Google Shopping verwendet, und es ist besser, sie zu benutzen.
Wir halten sie für wichtig! Deshalb haben wir inklusive des Inhalts. Sie definieren, welche Karte die entsprechende Funktion für die Verbesserung der Ergebnisse im Vergleich searchs zuzuschreiben.


Produkte ausschließen
-------------------

Dieser Abschnitt ermöglicht es Ihnen, einige Artikel ID Sie nicht wollen, aus irgendwelchen Gründen den Export anzugeben.
Jede ID, die hier angegeben ist, wird von unseren Exporteur Werkzeug ignoriert.


Cron
----

Unser Modul kommt mit einem Skript, das alle Dateien in den Prestashop-Cache, um die Ladung des Servers zu reduzieren erzeugen wird, wenn die Vergleichsmaschine traf die URL Ihres Feeds Produkte.
Diese Registerkarte ist hier, um Ihnen zu erklären, wie es funktioniert und warum Sie es verwenden.

Möchten wir Ihnen besonders empfehlen Ihnen, einen Cron-Job für dieses Modul zu implementieren, weil es deutlich die Gesamtleistung zu verbessern.


Berührung
=======

Sie einen Fehler finden, haben Sie einige Probleme bei der Konfiguration / Installation / machen dieses Modul funktioniert?
Fühlen Sie sich frei, mit mir über Prestashop an: https://addons.prestashop.com/en/write-to-developper?alternative=1&id_product=17163

Ich werde mein Bestes tun, um Ihnen zu helfen.


Vielen Dank !
========

Sie kaufte meine Modul, und dafür wollte ich Ihnen danken!
Ich hoffe, Sie finden, was Sie brauchen.
