Instalación
============

La instalación de este módulo es bastante fácil.
Todo lo que tienes que hacer es ir en su perfil módulos en su backoffice Prestashop, haga clic en "Agregar un nuevo módulo", seleccione el archivo y voilà! :)

Una vez instalado, usted tendrá que configurarlo.
Normalmente, usted debe ser redirigido a la página de configuración de este módulo, pero en el caso no lo es,
ir a los módulos, busque "Compra Total Exportador" y haga clic en el "vínculo Configurar".

Configuración
=============

Aparece la página de configuración con múltiples pestañas.
Cada pestañas representan un aspecto de la configuración. Vamos a ir a través de cada pestañas.


General
-------

Esta ficha enumera las URL que copiar / pegar su catálogo de productos a los sitios web de comparación.

Después de la primera instalación, esta ficha debe ser bastante vacío.
Usted tendrá que configurar este módulo con el fin de tener esta pestaña mostrar más información.

Para configurar una nueva exportación, simplemente haga clic en el botón situado a la derecha,
"Añadir un nuevo idioma a la exportación", y siga las instrucciones (explicaremos después).


Añadir un nuevo idioma a exportar media
--------------------------------

El formulario le permite elegir qué motor usar, y muchos otros campos de configurar.
Usted puede elegir el idioma, la moneda, el transportista y el número de días que se necesita para este
soporte para enviar sus productos.
A continuación, tendrá que elegir qué categorías desea exportar.

IMPORTANTE: Por favor, tenga en cuenta que el módulo exportar los productos que tienen la categoría de "principal" seleccionado.
Por ejemplo, si el producto se encuentra en las categorías "IPods" y "Accesorios", con "iPods" es la categoría principal,
si selecciona sólo "Accesorios", este producto no se exportará, porque es la categoría principal es no "Accesorios"
pero "iPods"!


Añadir un nuevo idioma a exportar 2.2
--------------------------------
La segunda página es la cartografía. Es una parte importante porque va a asignar sus categorías a las categorías del motor.
Por ejemplo, usted puede tener una categoría denominada "portátil", pero, en la compra de Google por ejemplo, ellos tienen "MAC Portátiles" y "Laptops".
Con el fin de aumentar sus ventas, es mejor dirigirse a la categoría correcta en el motor de compras.

Esta es la razón de la cartografía: hay que indicar en qué categorías desde el motor de compras, sus categorías encajan mejor.
Usted puede optar por ignorar este paso, algún motor de compras no requieren ellos, exceptuado Google Shopping (por exemple).

Ahora, es siempre mejor para mapear sus categorías: sus ventas aumentarán!


Opciones
-------

En esta pestaña, usted tendrá la posibilidad de especificar los productos de exportación, como la exclusión de los productos sin descripción, referencia, marca, etc.
También tendrá la posibilidad de specifiy el operador por defecto, con el fin de dejar que nuestro módulo calcular el precio del envío de su producto correctamente.


Atributos
----------

Sus productos tienen atributos, como el color, tamaño, material. Estos atributos se utilizan en algunos motores de comparación de compras como Google, y es mejor usarlos.
Nosotros consideramos importante! ¿Por eso hemos incluido esta ficha. Va a definir qué atribuyen asignar la función correspondiente para la mejora de los resultados en los searchs comparación.


Productos para excluir
-------------------

Esta sección le permite indicar algunos productos de identificación no desea exportar por cualquier motivo.
Cualquier ID que se indica aquí será ignorado por nuestra herramienta exportador.


Cron
----

Nuestro módulo viene con un script que va a generar todos los archivos en la memoria caché de Prestashop con el fin de reducir la carga de su servidor cuando el comparador golpeó la url de sus productos alimenta.
Esta ficha está aquí para explicar a usted cómo funciona y por qué usted debe usarlo.

Estamos altamente recommand a implementar un trabajo cron para este módulo porque mejorará claramente el rendimiento general.


Contacto
=======

Usted encuentra un error, usted tiene algunos problemas de configurar / instalar / hacer este módulo obras?
No dude en ponerse en contacto conmigo a través de Prestashop: https://addons.prestashop.com/en/write-to-developper?alternative=1&id_product=17163

Voy a hacer mi mejor esfuerzo para ayudarle.


Gracias !
========

Usted compró mi módulo, y para eso, yo quería darle las gracias!
Espero que encuentre lo que necesita.
