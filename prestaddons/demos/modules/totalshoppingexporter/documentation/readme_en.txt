Installation
============

The installation of this module is pretty easy.
All you have to do is to go in your modules listing in your Prestashop backoffice, click in "Add a new module", select the archive and voilà ! :)

Once installed, you will have to configure it.
Normally, you should be redirected to the configuration page of this module, but in the case it isn't,
go to Modules, search for "Total Shopping Exporter" and click on the "Configure link".

Configuration
=============

The configuration page appears with multiple Tabs.
Each tabs represent a aspect of the configuration. We will go through every tabs.


General
-------

This tab list the urls to copy/paste your product catalog to the comparison websites.

After the first installation, this tab should be quite empty.
You will have to configure this module in order to have this tab show more informations.

To configure an new export, simply click on the button located at the right,
"Add a new language to export", and follow the instructions (we will explain them after).


Add a new language to export 1/2
--------------------------------

The form let you choose which engine to use, and many other fields to configure.
You can choose the language, the currency, the carrier and how many days it takes for this
carrier to ship your products.
Then, you will have to choose which categories you want to export.

IMPORTANT : Please keep in mind that the module will export the products that have the "main category" selected.
For example, if your product is in categories "IPods" and "Accessories", with "IPods" is the main category,
if you select only "Accessories", this product will not be exported, because it's main category is not "Accessories"
but "IPods"!


Add a new language to export 2/2
--------------------------------
The second page is the mapping. It's an important part because it will map your categories to the categories of the engine.
For example, you may have a category called "Laptop", but, at Google Shopping for example, they have "MAC Laptops" and "PC Laptops".
In order to increase your sales, it's better to target the right category at the shopping engine.

This is the reason of the mapping : you have to indicate in what categories from the shopping engine, your categories fit best.
You can choose to ignore this step, some shopping engine don't require them, excepted Google Shopping (for exemple).

Now, it's always better to map your categories : your sales will increase!


Options
-------

In this tab, you will have the possibility to specify which products to export, like excluding products without description, reference, brand, etc.
You will also have the possibility to specifiy the default carrier, in order to let our module calculate the shipping price of your product correctly.


Attributes
----------

Your products have attributes, like color, size, material. These attributes are used in some comparison engines like Google shopping, and it's better to use them.
We consider them important ! That why we included this tab. You will define which attribute map the corresponding feature for improving the results in the comparison searchs.


Products to exclude
-------------------

This section allow you to indicate some products ID you don't want to export for any reasons.
Any ID that is indicated here will be ignored by our exporter tool.


Cron
----

Our module comes with a script that will generate all the files in the Prestashop cache in order to reduce the charge of your server when the comparison engine hit the url of your feeds products.
This tab is here to explain to you how it works and why you should use it.

We highly recommand you to implement a CRON job for this module because it will clearly improve the overall performance.


Contact
=======

You find a bug, you have some problems configuring/installing/making this module works?
Feel free to contact me via Prestashop : https://addons.prestashop.com/en/write-to-developper?alternative=1&id_product=17163

I'll do my best to help you.


Thanks !
========

You purchased my module, and for that, I wanted to thank you !
I hope you'll find what you need.
