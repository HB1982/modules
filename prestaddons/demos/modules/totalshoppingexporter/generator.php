<?php
/**
 * Shopping Exporter
 *
 * This modules export your products catalog
 *
 * If you find this->errors, bugs or if you want to share some improvments, feel free to contact at contact@prestaddons.net ! :)
 * Si vous trouvez des erreurs, des bugs ou si vous souhaitez tout simplement partager un conseil ou une amélioration,
 * n'hésitez pas à me contacter à contact@prestaddons.net
 *
 * @author    Cyril Nicodème <contact@prestaddons.net>
 * @copyright Copyright (C) June 2014 cnicodeme.com <email:contact@prestaddons.net>. All rights reserved.
 * @license   Nicodème Cyril
 * @since     2014-06-18
 * @package   modules
 * @version   2.6.5
 */

require_once(dirname(__FILE__).'/../../config/config.inc.php');
if (php_sapi_name() !== 'cli') {
    require_once(dirname(__FILE__).'/../../init.php');
}
require_once(dirname(__FILE__).'/simplexmlextendedexporter.php');

if (!defined('ENT_HTML401')) {
    define('ENT_HTML401', 0);
}

class TotalShoppingExporterGenerator
{
    private static $basename = 'TSEXPORTER';
    private static $engines = null;

    public static function getEngines()
    {
        if (is_null(self::$engines)) {
            $files = array_diff(scandir(dirname(__FILE__).'/engines/'), array('..', '.', 'index.php'));
            foreach ($files as $file) {
                require_once(dirname(__FILE__).'/engines/'.$file);
                $engine = Tools::strtolower(Tools::substr($file, 0, -10));
                self::$engines[$engine] = array(
                    'id'         => $engine,
                    'name'       => call_user_func(array(Tools::substr($file, 0, -4), 'getName')),
                    'taxonomies' => call_user_func(array(Tools::substr($file, 0, -4), 'hasTaxonomies')),
                );
            }
        }

        return self::$engines;
    }

    public static function isEngineTaxonomies($engine)
    {
        return self::$engines[$engine]['taxonomies'];
    }

    public static function deleteFiles($lang = null)
    {
        foreach (Shop::getShops() as $shop) {
            if (is_null($lang)) {
                foreach (Language::getLanguages() as $language) {
                    foreach (self::getEngines() as $engine) {
                        $file = _PS_CACHE_DIR_.self::getFilename($engine['id'], $shop['id_shop'], $language['id_lang']);
                        if (file_exists($file)) {
                            unlink($file);
                        }
                    }
                }
            } else {
                foreach (self::getEngines() as $engine) {
                    $file = _PS_CACHE_DIR_.self::getFilename($engine['id'], $shop['id_shop'], $lang);
                    if (file_exists($file)) {
                        unlink($file);
                    }
                }
            }
        }
    }

    public function isValidToken($shop_id = null)
    {
        if (php_sapi_name() === 'cli') {
            return true;
        }

        if (!is_null($shop_id) && Shop::isFeatureActive()) {
            Shop::setContext(Shop::CONTEXT_SHOP, $shop_id);
            Context::getContext()->shop = new Shop($shop_id);
        }

        return (Tools::getValue('token') === Configuration::get(self::$basename.'_secure_token'));
    }

    public function get($engine, $shop_id = null, $lang_id = null, $silent = false)
    {
        /**
         * Will try to load files in cache
         * And check their age.
         * If age correct, returns them
         * Else (age not correct or file does not exists), generate them and save them!
         */

        if (Shop::isFeatureActive()) {
            Shop::setContext(Shop::CONTEXT_SHOP, $shop_id);
            Context::getContext()->shop = new Shop($shop_id);
        }

        $filename = _PS_CACHE_DIR_.self::getFilename($engine, $shop_id, $lang_id);

        $generate = false;
        if (!is_file($filename)) {
            $generate = true;
        } else {
            // Delta in seconds
            $delta = time() - filemtime($filename);
            $cache_duration = Configuration::get(self::$basename.'_cache_duration', $lang_id, null, $shop_id);
            $max_cache_duration = (empty($cache_duration) ? 72 : (int)$cache_duration) * 60 * 60;

            if ($delta > $max_cache_duration) {
                $generate = true;
            }
        }

        if ($generate || Tools::getIsset('force')) {
            $this->generate($engine, $shop_id, $lang_id);
        }

        if (!$silent) {
            $content = @Tools::file_get_contents($filename);
            if ($content === false) {
                $shop = new Shop($shop_id);
                $lang = new Language($lang_id);
                echo "<h1>Unable to load the export.</h1>\n";
                echo 'Please check that you correctly defined the categories to export and the options for the Shop <strong>'.$shop->name.'</strong> with the language <strong>'.$lang->name.'</strong><br />'."\n";
                echo "<br />\n";
                echo 'Here are some reasons why no products were found:<br/>'."\n";
                echo "<ul>\n";
                echo '<li>Check the options in the module. For example by default, the module ignore the products that don\'t have any description, EAN13/UPC code, no brand, etc.<br />You can change these settings to match your catalog.</li>'."\n";
                echo '<li>'."\n";
                echo 'The module exports the products that have the category you selected to export, as their PRIMARY category :<br />'."\n";
                echo 'For example, if you have a product Z that is in category A, B, C, and have the default category set to B.<br />'."\n";
                echo 'If you select in the module to export the categories A and C, the product won\'t be in the exported data because the category B is the primary and you did not selected it.<br />'."\n";
                echo '</li>'."\n";

                exit();
            }

            self::getEngines();

            ob_end_clean();
            ob_start();
            call_user_func(array(Tools::ucfirst($engine).'Engine', 'setHeaders'), $shop_id);
            echo $content;
            ob_flush();
            ob_end_clean();

            exit(0);
        }
    }

    public function generateAll()
    {
        /**
         * Will Generate all output files
         */
        foreach (Shop::getShops() as $shop) {
            foreach (Language::getLanguages(true) as $language) {
                foreach (self::getEngines() as $engine) {
                    $this->get($engine['id'], $shop['id_shop'], $language['id_lang'], true);
                }
            }
        }
    }

    public function generate($engine, $shop_id = null, $lang_id = null)
    {
        if (!is_writable(_PS_CACHE_DIR_)) {
            exit('You need to make the /cache/ folder writable in order for this module to work.');
        }

        $link = new Link();

        $is_multi_shop_enabled = Shop::isFeatureActive();
        if ($is_multi_shop_enabled) {
            Shop::setContext(Shop::CONTEXT_SHOP, $shop_id);
            Context::getContext()->shop = new Shop($shop_id);
            Context::getContext()->country = new Country(Configuration::get('PS_COUNTRY_DEFAULT'));

            if (method_exists('ShopUrl', 'resetMainDomainCache') !== false) {
                ShopUrl::resetMainDomainCache();
            }
        }

        $options_multiple = Configuration::getMultiple(array(
            'PS_WEIGHT_UNIT', 'PS_NAVIGATION_PIPE', 'PS_LAST_QTIES', 'PS_COUNTRY_DEFAULT', 'PS_SHIPPING_HANDLING',
            'PS_SHIPPING_FREE_PRICE', 'PS_SHIPPING_FREE_WEIGHT',
            self::$basename.'_export_stock', self::$basename.'_stock_state', self::$basename.'_export_no_description',
            self::$basename.'_export_combination', self::$basename.'_export_no_ean13', self::$basename.'_export_no_brand', self::$basename.'_description_size',
            self::$basename.'_product_condition', self::$basename.'_image_size', self::$basename.'_exclude_ids',

            self::$basename.'_categories', self::$basename.'_default_carrier', self::$basename.'_delivery_schedule',

            self::$basename.'_include_color', self::$basename.'_include_color_attr', self::$basename.'_include_gender',
            self::$basename.'_include_gender_attr', self::$basename.'_include_size', self::$basename.'_include_size_attr',
            self::$basename.'_include_pattern', self::$basename.'_include_pattern_attr', self::$basename.'_include_material',
            self::$basename.'_include_material_attr', self::$basename.'_include_agegrp',
            self::$basename.'_include_agegrp_attr'
        ));

        $options = array (
            'export_stock'          => $options_multiple[self::$basename.'_export_stock'],
            'export_no_description' => $options_multiple[self::$basename.'_export_no_description'],
            'export_combination'    => $options_multiple[self::$basename.'_export_combination'],
            'export_no_ean13'       => $options_multiple[self::$basename.'_export_no_ean13'],
            'export_no_brand'       => $options_multiple[self::$basename.'_export_no_brand'],

            'stock_state'           => $options_multiple[self::$basename.'_stock_state'],
            'description_size'      => $options_multiple[self::$basename.'_description_size'],
            'product_condition'     => $options_multiple[self::$basename.'_product_condition'],

            'default_carrier'       => $options_multiple[self::$basename.'_default_carrier'],
            'country_default'       => $options_multiple['PS_COUNTRY_DEFAULT'],
            'shipping_handling'     => $options_multiple['PS_SHIPPING_HANDLING'],
            'shipping_free_price'   => $options_multiple['PS_SHIPPING_FREE_PRICE'],
            'shipping_free_weight'  => $options_multiple['PS_SHIPPING_FREE_WEIGHT'],
            'delivery_schedule'     => $options_multiple[self::$basename.'_delivery_schedule'],
            'image_size'            => $options_multiple[self::$basename.'_image_size'],
            'exclude_ids'           => explode(',', $options_multiple[self::$basename.'_exclude_ids']),
        );

        $attributes = array (
            'color'    => ($options_multiple[self::$basename.'_include_color']    ? $options_multiple[self::$basename.'_include_color_attr']    : null),
            'gender'   => ($options_multiple[self::$basename.'_include_gender']   ? $options_multiple[self::$basename.'_include_gender_attr']   : null),
            'size'     => ($options_multiple[self::$basename.'_include_size']     ? $options_multiple[self::$basename.'_include_size_attr']     : null),
            'pattern'  => ($options_multiple[self::$basename.'_include_pattern']  ? $options_multiple[self::$basename.'_include_pattern_attr']  : null),
            'material' => ($options_multiple[self::$basename.'_include_material'] ? $options_multiple[self::$basename.'_include_material_attr'] : null),
            'agegrp'   => ($options_multiple[self::$basename.'_include_agegrp']   ? $options_multiple[self::$basename.'_include_agegrp_attr']   : null),
        );

        $optionsStockState = 1;
        if ($options['stock_state'] === 'available for order') {
            $optionsStockState = 5;
        } else if ($options['stock_state'] === 'preorder') {
            $optionsStockState = 4;
        }

        $categories = unserialize($options_multiple[self::$basename.'_categories']);

        if (!isset($categories[$lang_id]['engines'][$engine]) || empty($categories[$lang_id]['engines'][$engine])) {
            return false;
        }

        if (!empty($categories[$lang_id]['delivery_schedule'])) {
            $options['delivery_schedule'] = $categories[$lang_id]['delivery_schedule'];
        }

        $weight_unit = $options_multiple['PS_WEIGHT_UNIT'];
        $navigation_pipe = $options_multiple['PS_NAVIGATION_PIPE'];

        if ($categories === false) {
            return false;
        }

        $cats = array();
        foreach ($categories[$lang_id]['engines'][$engine] as $cat_id => $category) {
            if (!empty($cat_id)) {
                $cats[] = $cat_id;
            }
        }

        if (count($cats) === 0) {
            return false;
        }

        $currency = new Currency($categories[$lang_id]['currency_id']);
        Context::getContext()->currency = $currency;

        $products = Db::getInstance()->ExecuteS(
            'SELECT p.id_product, '.($options['export_combination'] ? ($is_multi_shop_enabled ? 'pas' : 'pa').'.id_product_attribute' : ($is_multi_shop_enabled ? 'ps' : 'p').'.id_product').' used_id,
            pl.link_rewrite,  m.name brand, pl.name title,
            '.($options['description_size'] === 'short' ? 'pl.description_short' : 'pl.description').' description,
            '.($options['export_combination'] ? 'IF (IFNULL(pa.`reference`, "") = "", p.`reference`, pa.`reference`)' : 'p.reference').' mpn,
            cl.link_rewrite category_link, cl.name category_name,
            '.($options['export_combination'] ? 'IF (IFNULL(pa.`ean13`, "") = "", p.`ean13`, pa.`ean13`)' : 'p.ean13').' ean13,
            '.($options['export_combination'] ? 'IF (IFNULL(pa.`upc`, "") = "", p.`upc`, pa.`upc`)' : 'p.upc').' upc,

            '.($options['export_combination'] ? ($is_multi_shop_enabled ? 'pas' : 'pa').'.ecotax' : ($is_multi_shop_enabled ? 'ps' : 'p').'.ecotax').' ecotax,
            IFNULL(p.weight, 0) AS product_weight,
            IFNULL('.($options['export_combination'] ? ($is_multi_shop_enabled ? 'pas' : 'pa').'.weight' : '0').', 0) AS attribute_weight,
            '.($is_multi_shop_enabled ? 'ps' : 'p').'.online_only, '.($is_multi_shop_enabled ? 'ps' : 'p').'.condition, '.($is_multi_shop_enabled ? 'ps' : 'p').'.id_category_default

            FROM '._DB_PREFIX_.'product p
            LEFT JOIN '._DB_PREFIX_.'product_lang pl ON (pl.id_product = p.id_product)
            LEFT JOIN '._DB_PREFIX_.'lang l ON (l.id_lang = pl.id_lang AND l.id_lang = '.pSQL((int)$lang_id).')
            LEFT JOIN '._DB_PREFIX_.'manufacturer m ON (m.id_manufacturer = p.id_manufacturer)
            LEFT JOIN '._DB_PREFIX_.'category_lang cl ON (cl.id_category = p.id_category_default AND cl.id_lang = l.id_lang)
            '.($options['export_combination'] ? 'LEFT JOIN '._DB_PREFIX_.'product_attribute pa ON (pa.id_product = p.id_product)' : '').'
            '.($is_multi_shop_enabled
                ?
                'LEFT JOIN '._DB_PREFIX_.'product_shop ps ON (ps.id_product = p.id_product AND ps.id_shop = '.pSQL((int)$shop_id).')'.
                ($options['export_combination'] ? 'LEFT JOIN '._DB_PREFIX_.'product_attribute_shop pas ON (pas.id_product_attribute = pa.id_product_attribute AND pas.id_shop = '.pSQL((int)$shop_id).')' : '')
                :
                ''
            ).'
            WHERE '.($is_multi_shop_enabled ? 'ps' : 'p').'.active = 1 AND l.id_lang = '.pSQL((int)$lang_id).' AND pl.id_lang = '.pSQL((int)$lang_id).($is_multi_shop_enabled ? ' AND pl.id_shop = '.pSQL((int)$shop_id) : '').'
            AND '.($is_multi_shop_enabled ? 'ps' : 'p').'.id_category_default IN ('.pSQL(implode(',', array_map('intval', $cats))).')
            '.($is_multi_shop_enabled ? 'AND ps.id_shop = '.pSQL((int)$shop_id) : '').'
            GROUP BY '.($options['export_combination'] ? 'pa.id_product_attribute, p.id_product' : 'p.id_product').'
            ORDER BY p.id_product'
        );

        if (!$products) {
            return false;
        }

        $exportings = array ();
        if (is_null(Context::getContext()->cart)) {
            Context::getContext()->cart = new Cart();
        }

        foreach ($products as $product) {
            $exporting_id = (is_null($product['used_id']) ? (int)$product['id_product'] : $product['id_product'].'-'.$product['used_id']);
            if (in_array($exporting_id, $options['exclude_ids'])) {
                continue;
            }

            $exporting = array (
                'title' => trim($product['title']),
                'link' => $link->getProductLink((int)$product['id_product'], $product['link_rewrite'], $product['category_link'], $product['ean13'], (int)$lang_id),
                'description' => html_entity_decode(strip_tags($product['description']), ENT_COMPAT | ENT_HTML401, 'UTF-8'),
                'id' => $exporting_id,
                'price' => round((float)Product::getPriceStatic((int)$product['id_product'], true, ($options['export_combination'] ? $product['used_id'] : null), 6), 2),
                'original_price' => (float)Product::getPriceStatic((int)$product['id_product'], true, ($options['export_combination'] ? $product['used_id'] : null), 2, null, false, false),
                'product_type' => str_replace($navigation_pipe, ' > ', strip_tags($this->_getPath((int)$product['id_category_default'], Tools::htmlentitiesUTF8($product['category_name'])))),
                'category_name' => $product['category_name'],
                'quantity' => (($options['export_combination'] && !is_null($product['used_id'])) ? (int)Product::getQuantity((int)$product['id_product'], (int)$product['used_id']) : (int)Product::getQuantity((int)$product['id_product'])),
                'shipping_weight' => (float)$product['product_weight'] + (float)$product['attribute_weight'],
                'weight_unit' => $weight_unit,
                'online_only' => ((int)$product['online_only'] === 0) ? 'n' : 'y',
                'condition' => $product['condition'], /** Test if one of these **/
                'brand' => (empty($product['brand']) ? null : trim($product['brand'])),
                'mpn' => (empty($product['mpn']) ? null : $product['mpn']),
                'ean13' => (empty($product['ean13']) ? null : $product['ean13']),
                'upc' => (empty($product['upc']) ? null : $product['upc']),
                'images' => $this->getImages($link, $product['id_product'], ($options['export_combination'] ? (int)$product['used_id'] : null), $lang_id, $product['link_rewrite'], (int)$shop_id, $options['image_size']),
                'ecotax' => $product['ecotax'],
                'id_product' => $product['id_product'],
                'currency' => array (
                    'sign' => $currency->sign,
                    'code' => $currency->iso_code
                )
            );

            foreach (self::getEngines() as $eng) {
                if (!isset($categories[$lang_id]) || !isset($categories[$lang_id]['engines'][$eng['id']]) || !isset($categories[$lang_id]['engines'][$eng['id']][$product['id_category_default']])) {
                    continue;
                }

                $exporting[$eng['id'].'_category'] = $categories[$lang_id]['engines'][$eng['id']][$product['id_category_default']];
            }

            // Elimination processus
            if (!$options['export_no_description'] && empty($exporting['description'])) {
                continue;
            }
            if (!$options['export_no_ean13'] && (empty($exporting['ean13']) && empty($exporting['upc']))) {
                continue;
            }
            if (!$options['export_no_brand'] && empty($exporting['brand'])) {
                continue;
            }

            if ($exporting['quantity'] <= 0) {
                if (!$options['export_stock']) {
                    continue;
                }

                $exporting['availability'] = $options['stock_state'];
            } else {
                $exporting['availability'] = 'in stock';
            }

            $category = new Category($product['id_category_default']);
            $exporting['category'] = $category->getName($lang_id);

            if (empty($exporting['condition'])) {
                $exporting['condition'] = $options['product_condition'];
            }

            // color, size, pattern, gender, material
            $attributes_ids = array ();
            $features_ids = array ();
            $title_attributes = array();
            foreach ($attributes as $key => $attribute) {
                if (Tools::substr($attribute, 0, 1) === 'a') { // Attribute
                    $attributes_ids[$key] = (int)Tools::substr($attribute, 2);
                } else { // Feature
                    $features_ids[$key] = (int)Tools::substr($attribute, 2);
                }
            }

            // Exporting attributes
            if (count($attributes_ids) > 0) {
                $attribute_lists = Db::getInstance()->ExecuteS('SELECT a.id_attribute_group, al.name'.($options['export_combination'] ? ', agl.name group_name ' : '').'
                                FROM '._DB_PREFIX_.'attribute a
                                LEFT JOIN '._DB_PREFIX_.'attribute_lang al ON (al.id_attribute = a.id_attribute)
                                '.($options['export_combination'] ? 'LEFT JOIN '._DB_PREFIX_.'attribute_group_lang agl ON (agl.id_attribute_group = a.id_attribute_group)' : '').'
                                LEFT JOIN '._DB_PREFIX_.'product_attribute_combination pac ON (pac.id_attribute = al.id_attribute)
                                LEFT JOIN '._DB_PREFIX_.'product_attribute pa ON (pa.id_product_attribute = pac.id_product_attribute)
                                WHERE a.id_attribute_group IN ('.implode(',', $attributes_ids).') AND '.($options['export_combination'] ? 'pac.id_product_attribute = '.pSQL(is_null($product['used_id']) ? (int)$product['id_product'] : (int)$product['used_id']) : 'pa.id_product = '.pSQL((int)$product['id_product'])).'
                                '.($options['export_combination'] ? 'AND agl.id_lang = '.pSQL((int)$lang_id) : '').' AND al.id_lang = '.pSQL((int)$lang_id).' GROUP BY a.id_attribute');

                $exporting['attributes'] = array();
                foreach ($attribute_lists as $attribute) {
                    $exporting['attributes'][array_search($attribute['id_attribute_group'], $attributes_ids)] = $attribute['name'];

                    if ($options['export_combination']) {
                        $title_attributes[] = $attribute['group_name'].' : '.$attribute['name'];
                    }
                }
            }

            // Doing the same for features now
            if (count($features_ids) > 0) {
                $features = Db::getInstance()->ExecuteS('SELECT fp.id_feature, fvl.value value
                        FROM '._DB_PREFIX_.'feature_product fp LEFT JOIN '._DB_PREFIX_.'feature_value_lang fvl ON (fvl.id_feature_value = fp.id_feature_value)
                        WHERE fp.id_product = '.pSQL((int)$product['id_product']).' AND fp.id_feature IN ('.implode(',', $features_ids).') AND fvl.id_lang = '.pSQL((int)$lang_id));

                foreach ($features as $feature) {
                    $exporting['attributes'][array_search($feature['id_feature'], $features_ids)] = $feature['value'];
                }
            }

            if (count($title_attributes) > 0) {
                $exporting['title'] .= ' ('.implode(', ', $title_attributes).')';
            }

            $exporting['shipping'] = $this->getShippings($exporting['shipping_weight'], $exporting['price'], $currency->id, $categories[$lang_id]['carrier_id'], $options);
            $exportings[] = $exporting;
        }

        if (count($exportings) === 0) {
            return false;
        }

        call_user_func_array(array(Tools::ucfirst($engine).'Engine', 'export'), array(
            _PS_CACHE_DIR_.self::getFilename($engine, $shop_id, $lang_id),
            $shop_id, $lang_id, $exportings, $options
        ));

        return true;
    }

    private function _getPath($id_category, $category_name)
    {
        if (Tools::substr(_PS_VERSION_, 0, 3) < '1.7') {
            return Tools::getPath($id_category, $category_name);
        }

        return Tools::getPath($category_name, $id_category);
    }

    private static function getFilename($engine, $shop_id = null, $lang_id = null)
    {
        if (is_null($shop_id) && is_null($lang_id)) {
            return sprintf('tse_%s', $engine);
        } elseif (is_null($shop_id)) {
            return sprintf('tse_%s_l%d', $engine, $lang_id);
        } elseif (is_null($lang_id)) {
            return sprintf('tse_%s_s%d', $engine, $shop_id);
        }

        return sprintf('tse_%s_s%d_l%d', $engine, $shop_id, $lang_id);
    }

    private function getShippings($weight, $price, $currency_id, $carrier_id, $options)
    {
        $country = new Country($options['country_default']);
        $carrier = Carrier::getCarrierByReference($carrier_id);

        if ($carrier === false) {
            exit('Please define a default carrier in the module options tab.');
        }

        $shipping_price = 0;
        if (!empty($options['shipping_free_weight']) && !empty($weight) && $weight >= ((float)$options['shipping_free_weight'])) {
            $shipping_price = 0;
        } else if (!empty($options['shipping_free_price']) && !empty($price) && $price >= ((float)$options['shipping_free_price'])) {
            $shipping_price = 0;
        } else {
            switch ($carrier->getShippingMethod()) {
                case Carrier::SHIPPING_METHOD_FREE:
                    break;
                case Carrier::SHIPPING_METHOD_WEIGHT:
                    if (empty($options['shipping_free_weight']) || $weight <= ((float)$options['shipping_free_weight'])) {
                        $shipping_price = (float)$carrier->getDeliveryPriceByWeight((float)Tools::substr($weight, 0, strpos($weight, ' ')), $country->id_zone);
                    }
                    break;
                case Carrier::SHIPPING_METHOD_PRICE:
                    if (empty($options['shipping_free_price']) || $price <= ((float)$options['shipping_free_price'])) {
                        $shipping_price = (float)$carrier->getDeliveryPriceByPrice($price, $country->id_zone, $currency_id);
                    }
                    break;
            }
        }

        if ($shipping_price !== 0) {
            $address = new Address();
            $address->id_country = $country->id;
            $tax_rate = $carrier->getTaxesRate($address);

            $shipping_price += (((float)$shipping_price * $tax_rate) / 100);

            if ($carrier->shipping_handling === '1') {
                $shipping_price += (float)$options['shipping_handling'];
                $shipping_price += (((float)$options['shipping_handling'] * $tax_rate) / 100);
            }
        }

        return array (
            'country' => $country->iso_code,
            'service' => $carrier->name,
            'price'   => $shipping_price
        );
    }

    // @see https://github.com/PrestaShop/PrestaShop/blob/1.6.1.4/classes/Image.php#L174
    private function getImages($link, $id_product, $id_attribute, $id_lang, $link_rewrite, $id_shop, $size)
    {
        $images = array();
        if (!is_null($id_attribute)) {
            $images = Db::getInstance()->ExecuteS('SELECT i.id_image FROM `'._DB_PREFIX_.'image` i LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image`) LEFT JOIN `'._DB_PREFIX_.'product_attribute_image` ai ON (i.`id_image` = ai.`id_image`) WHERE i.`id_product` = '.(int)$id_product.' AND il.`id_lang` = '.(int)$id_lang.' AND ai.`id_product_attribute` = '.(int)$id_attribute.' ORDER BY i.`position` ASC');
        }

        if (count($images) === 0) {
            $images = Db::getInstance()->ExecuteS('SELECT i.id_image FROM `'._DB_PREFIX_.'image` i LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image`) WHERE i.`id_product` = '.(int)$id_product.' AND il.`id_lang` = '.(int)$id_lang.' ORDER BY i.`position` ASC');
        }

        $results = array ();
        foreach ($images as $image) {
            $results[] = 'http://'.$link->getImageLink($link_rewrite, (int)$id_product.'-'.(int)$image['id_image'], $size);
        }

        return $results;
    }
}
