{**
 * @package   Shopping Exporter
 * @author    Cyril Nicodème <contact@prestaddons.net>
 * @copyright Copyright (C) June 2014 cnicodeme.com <@email:contact@prestaddons.net>. All rights reserved.
 * @since     2014-06-18
 * @version   2.6.5
 * @license   Nicodème Cyril
 *}

<div id="tsexporter" class="bootstrap">
    <h1>Total Shopping Exporter</h1>

    <ul id="tse-tabs" class="tse-menu">
        <li class="active"><a href="#tse-general">{l s='General' mod='totalshoppingexporter'}</a></li>
        <li><a href="#tse-options">{l s='Options' mod='totalshoppingexporter'}</a></li>
        <li><a href="#tse-attributes">{l s='Attributes' mod='totalshoppingexporter'}</a></li>
        <li><a href="#tse-exclude">{l s='Products to exclude' mod='totalshoppingexporter'}</a></li>
        <li><a href="#tse-cron">{l s='Cron' mod='totalshoppingexporter'}</a></li>
        <li><a href="#tse-help">{l s='Help' mod='totalshoppingexporter'}</a></li>
    </ul>

    <div class="tabs">
        <div id="tse-general">
            <p>{l s='Once you will have configured this module to your needs, you will be able to send the feeds url to the comparators.' mod='totalshoppingexporter'}</p>
            <p>
                {l s='In order to have a best performance result, we recommand you to implement a CRON task that will regenerate the files.' mod='totalshoppingexporter'}<br />
                {l s='For more details, go to the "Cron" tab.' mod='totalshoppingexporter'}
            </p>
            <div class="text-right" style="margin-bottom: 20px">
                <a href="{$configure_url|escape:'htmlall':'UTF-8'}&amp;view=categories&amp;step=1" title="{l s='Click here to add a new language to export.' mod='totalshoppingexporter'}" class="btn btn-primary">{l s='Add a new language to export' mod='totalshoppingexporter'}</a>
            </div>

            {foreach $languages as $language}
            {if isset($categories[$language['id']])}
            {foreach $categories[$language['id']]['engines'] as $engine=>$engine}
            {if !empty($categories[$language['id']]['engines'][$engine])}
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>
                            {$engines[$engine]|escape:'html':'UTF-8'} : {$language['name']|escape:'html':'UTF-8'}
                            <small>({l s='Currency' mod='totalshoppingexporter'} : <strong>{$currencies[$categories[$language['id']]['currency_id']]['name']|escape:'htmlall':'UTF-8'}</strong> -
                                {l s='Carrier' mod='totalshoppingexporter'} : <strong>{if isset($categories[$language['id']]['carrier_id']) && isset($carriers[$categories[$language['id']]['carrier_id']]) }{$carriers[$categories[$language['id']]['carrier_id']]['name']|escape:'htmlall':'UTF-8'}{else}N/A{/if}</strong>)</small>
                        </th>
                        <th style="text-align: right">
                            <a href="{$configure_url|escape:'htmlall':'UTF-8'}&amp;view=categories&amp;lang_id={$language['id']|escape:'html':'UTF-8'}&amp;engine={$engine|escape:'html':'UTF-8'}&amp;step=1" title="{l s='Click here to update the settings regarding this language.' mod='totalshoppingexporter'}" class="btn btn-default">{l s='Update the settings' mod='totalshoppingexporter'}</a>
                            -
                            <a href="{$configure_url|escape:'htmlall':'UTF-8'}&amp;view=categories&amp;lang_id={$language['id']|escape:'html':'UTF-8'}&amp;engine={$engine|escape:'html':'UTF-8'}&amp;step=delete" title="{l s='Click here to delete this export.' mod='totalshoppingexporter'}" class="btn btn-danger">{l s='Delete' mod='totalshoppingexporter'}</a>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {foreach $shops as $shop}
                    <tr>
                        <td style="font-weight: bold">{$shop['name']|escape:'html':'UTF-8'}</td>
                        <td><a href="{$base_url|escape:'html':'UTF-8'}export.php?token={$secure_token|escape:'html':'UTF-8'}&amp;shop_id={$shop['id']|escape:'html':'UTF-8'}&amp;lang_id={$language['id']|escape:'html':'UTF-8'}&amp;engine={$engine|escape:'html':'UTF-8'}" title="{l s='Click here to view the feed product' mod='totalshoppingexporter'}" target="_blank">{$base_url|escape:'html':'UTF-8'}export.php?token={$secure_token|escape:'html':'UTF-8'}&amp;shop_id={$shop['id']|escape:'html':'UTF-8'}&amp;lang_id={$language['id']|escape:'html':'UTF-8'}&amp;engine={$engine|escape:'html':'UTF-8'}</a></td>
                    </tr>
                    {/foreach}
                </tbody>
            </table>
            {/if}
            {/foreach}
            {/if}
            {/foreach}

            {if empty($categories)}
            <div class="alert alert-info">
                <p>{l s='You don\'t have any export configured yet. You first need to click on the blue button at the right to configure an export of your products by language.' mod='totalshoppingexporter'}</p>
                <p>{l s='You can configure an export per language with their options, including the currency, the default carrier and the average delivery schedule.' mod='totalshoppingexporter'}</p>
                <p>{l s='You will also be able to configure which category to export, and also select some categories based on the language.' mod='totalshoppingexporter'}</p>
            </div>
            {/if}
        </div>
        <div id="tse-options">
            <form method="post" class="defaultForm form-horizontal">
                <div class="form-group">
                    <label class="checkbox col-lg-offset-3 col-lg-9">
                        <input type="checkbox" name="export_stock" id="options-export_stock" value="true"{if $export_stock} checked{/if} />
                        {l s='Export the product if out of stock.' mod='totalshoppingexporter'}
                    </label>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">{l s='Stock state when empty' mod='totalshoppingexporter'}:</label>
                    <div class="col-lg-3">
                        <select name="stock_state" id="options-stock_state" class="form-control">
                            <option value="out of stock"{if $stock_state == 'out of stock'} selected{/if}>{l s='Out of stock' mod='totalshoppingexporter'}</option>
                            <option value="preorder"{if $stock_state == 'preorder'} selected{/if}>{l s='Preorder' mod='totalshoppingexporter'}</option>
                            <option value="in stock"{if $stock_state == 'in stock'} selected{/if}>{l s='In stock' mod='totalshoppingexporter'}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="checkbox col-lg-offset-3 col-lg-9">
                        <input type="checkbox" name="export_no_description" id="options-export_no_description" value="true"{if $export_no_description} checked{/if} />
                        {l s='Export products with no description.' mod='totalshoppingexporter'}
                    </label>
                </div>
                <div class="form-group">
                    <label class="checkbox col-lg-offset-3 col-lg-9">
                        <input type="checkbox" name="export_no_ean13" id="options-export_no_ean13" value="true"{if $export_no_ean13} checked{/if} />
                        {l s='Export products that does not have EAN13 nor UPC code.' mod='totalshoppingexporter'}
                    </label>
                </div>
                <div class="form-group">
                    <label class="checkbox col-lg-offset-3 col-lg-9">
                        <input type="checkbox" name="export_no_brand" id="options-export_no_brand" value="true"{if $export_no_brand} checked{/if} />
                        {l s='Export products that does not have a brand.' mod='totalshoppingexporter'}
                    </label>
                </div>
                <div class="form-group">
                    <label class="checkbox col-lg-offset-3 col-lg-9">
                        <input type="checkbox" name="export_combination" id="options-export_combination" value="true"{if $export_combination} checked{/if} />
                        {l s='Export multiple products based on their attributes.' mod='totalshoppingexporter'}
                    </label>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label">{l s='Description size' mod='totalshoppingexporter'}:</label>
                    <div class="col-lg-3">
                        <select name="description_size" id="options-description_size" class="form-control">
                            <option value="long"{if $description_size == 'long'} selected{/if}>{l s='Long' mod='totalshoppingexporter'}</option>
                            <option value="short"{if $description_size == 'short'} selected{/if}>{l s='Short' mod='totalshoppingexporter'}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">{l s='Image size' mod='totalshoppingexporter'}:</label>
                    <div class="col-lg-3">
                        <select name="image_size" id="options-image_size" class="form-control">
                            {foreach $image_sizes as $size}
                            <option value="{$size['id']|escape:'html':'UTF-8'}"{if $image_size == $size['id']} selected{/if}>{$size['name']|escape:'htmlall':'UTF-8'}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">{l s='Product condition' mod='totalshoppingexporter'}:</label>
                    <div class="col-lg-3">
                        <select name="product_condition" id="options-product_condition" class="form-control">
                            <option value="new"{if $product_condition == 'new'} selected{/if}>{l s='New' mod='totalshoppingexporter'}</option>
                            <option value="used"{if $product_condition == 'used'} selected{/if}>{l s='Used' mod='totalshoppingexporter'}</option>
                            <option value="refurbished"{if $product_condition == 'refurbished'} selected{/if}>{l s='Refurbished' mod='totalshoppingexporter'}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">{l s='Cache duration' mod='totalshoppingexporter'}:</label>
                    <div class="col-lg-3">
                        <input type="text" name="cache_duration" value="{$cache_duration|escape:'html':'UTF-8'}" class="form-control" placeholder="{l s='72H by default' mod='totalshoppingexporter'}" /> {l s='(in hours)' mod='totalshoppingexporter'} - {l s='Will be the max duration time the generated feeds product will be kept. Default is 72H' mod='totalshoppingexporter'}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">{l s='Shop ID (from LeGuide)' mod='totalshoppingexporter'}:</label>
                    <div class="col-lg-3">
                        <input type="text" name="kelkoo_shop_id" value="{$kelkoo_shop_id|escape:'html':'UTF-8'}" class="form-control" placeholder="{l s='Shop ID provided by LeGuide/Kelkoo to enable tracking.' mod='totalshoppingexporter'}" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-offset-3 col-lg-9">
                        <input type="hidden" name="section" value="options" />
                        <input type="submit" name="Save" value="{l s='Save the changes' mod='totalshoppingexporter'}" class="btn btn-default" />
                    </div>
                </div>
            </form>
            <div class="clearfix"></div>
            <hr />
            <form method="post" class="defaultForm form-horizontal">
                <div class="col-lg-offset-3 col-lg-9">
                    <p>{l s='If you need, you can click the button below to regenerate a new secure token.' mod='totalshoppingexporter'}</p>
                    <p>{l s='PLEASE BE AWARE that if you already shared the export link to the price engine, you will have to re-submit it with the new token because the old link will be considered invalid.' mod='totalshoppingexporter'}</p>
                    <p>{l s='(This is a security measure)' mod='totalshoppingexporter'}</p>
                    <input type="hidden" name="section" value="token" />
                    <input type="submit" name="Save" value="{l s='Regenerate a new token' mod='totalshoppingexporter'}" class="btn btn-danger" />
                </div>
            </form>
            <div class="clearfix"></div>
        </div>
        <div id="tse-attributes">
            <form method="post">
                <table>
                    <tbody>
                        <tr>
                            <td class="col-lg-3">
                                <label class="control-label" for="attributes-include_gender">{l s='Include Gender' mod='totalshoppingexporter'}:</label>
                            </td>
                            <td class="col-lg-2">
                                <input type="checkbox" name="include_gender" value="true"{if $include_gender} checked{/if} />
                            </td>
                            <td class="col-lg-4">
                                <select name="include_gender_attr" class="form-control">
                                    <optgroup label="{l s='Attributes' mod='totalshoppingexporter'}">
                                    {foreach $attributes as $attribute}
                                        <option value="{$attribute['id']|escape:'html':'UTF-8'}"{if $include_gender_attr == $attribute['id']} selected{/if}>{$attribute['name']|escape:'htmlall':'UTF-8'}</option>
                                    {/foreach}
                                    </optgroup>
                                    <optgroup label="{l s='Features' mod='totalshoppingexporter'}">
                                    {foreach $features as $feature}
                                        <option value="{$feature['id']|escape:'html':'UTF-8'}"{if $include_gender_attr == $feature['id']} selected{/if}>{$feature['name']|escape:'htmlall':'UTF-8'}</option>
                                    {/foreach}
                                    </optgroup>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-lg-3">
                                <label class="control-label" for="attributes-include_color">{l s='Include Color' mod='totalshoppingexporter'}:</label>
                            </td>
                            <td class="col-lg-2">
                                <input type="checkbox" name="include_color" value="true"{if $include_color} checked{/if} />
                            </td>
                            <td class="col-lg-4">
                                <select name="include_color_attr" class="form-control">
                                    <optgroup label="{l s='Attributes' mod='totalshoppingexporter'}">
                                    {foreach $attributes as $attribute}
                                        <option value="{$attribute['id']|escape:'html':'UTF-8'}"{if $include_color_attr == $attribute['id']} selected{/if}>{$attribute['name']|escape:'htmlall':'UTF-8'}</option>
                                    {/foreach}
                                    </optgroup>
                                    <optgroup label="{l s='Features' mod='totalshoppingexporter'}">
                                    {foreach $features as $feature}
                                        <option value="{$feature['id']|escape:'html':'UTF-8'}"{if $include_color_attr == $feature['id']} selected{/if}>{$feature['name']|escape:'htmlall':'UTF-8'}</option>
                                    {/foreach}
                                    </optgroup>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-lg-3">
                                <label class="control-label" for="attributes-include_size">{l s='Include Size' mod='totalshoppingexporter'}:</label>
                            </td>
                            <td class="col-lg-2">
                                <input type="checkbox" name="include_size" value="true"{if $include_size} checked{/if} />
                            </td>
                            <td class="col-lg-4">
                                <select name="include_size_attr" class="form-control">
                                    <optgroup label="{l s='Attributes' mod='totalshoppingexporter'}">
                                    {foreach $attributes as $attribute}
                                        <option value="{$attribute['id']|escape:'html':'UTF-8'}"{if $include_size_attr == $attribute['id']} selected{/if}>{$attribute['name']|escape:'htmlall':'UTF-8'}</option>
                                    {/foreach}
                                    </optgroup>
                                    <optgroup label="{l s='Features' mod='totalshoppingexporter'}">
                                    {foreach $features as $feature}
                                        <option value="{$feature['id']|escape:'html':'UTF-8'}"{if $include_size_attr == $feature['id']} selected{/if}>{$feature['name']|escape:'htmlall':'UTF-8'}</option>
                                    {/foreach}
                                    </optgroup>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-lg-3">
                                <label class="control-label" for="attributes-include_pattern">{l s='Include Pattern' mod='totalshoppingexporter'}:</label>
                            </td>
                            <td class="col-lg-2">
                                <input type="checkbox" name="include_pattern" value="true"{if $include_pattern} checked{/if} />
                            </td>
                            <td class="col-lg-4">
                                <select name="include_pattern_attr" class="form-control">
                                    <optgroup label="{l s='Attributes' mod='totalshoppingexporter'}">
                                    {foreach $attributes as $attribute}
                                        <option value="{$attribute['id']|escape:'html':'UTF-8'}"{if $include_pattern_attr == $attribute['id']} selected{/if}>{$attribute['name']|escape:'htmlall':'UTF-8'}</option>
                                    {/foreach}
                                    </optgroup>
                                    <optgroup label="{l s='Features' mod='totalshoppingexporter'}">
                                    {foreach $features as $feature}
                                        <option value="{$feature['id']|escape:'html':'UTF-8'}"{if $include_pattern_attr == $feature['id']} selected{/if}>{$feature['name']|escape:'htmlall':'UTF-8'}</option>
                                    {/foreach}
                                    </optgroup>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-lg-3">
                                <label class="control-label" for="attributes-include_material">{l s='Include Material' mod='totalshoppingexporter'}:</label>
                            </td>
                            <td class="col-lg-2">
                                <input type="checkbox" name="include_material" value="true"{if $include_material} checked{/if} />
                            </td>
                            <td class="col-lg-4">
                                <select name="include_material_attr" class="form-control">
                                    <optgroup label="{l s='Attributes' mod='totalshoppingexporter'}">
                                    {foreach $attributes as $attribute}
                                        <option value="{$attribute['id']|escape:'html':'UTF-8'}"{if $include_material_attr == $attribute['id']} selected{/if}>{$attribute['name']|escape:'htmlall':'UTF-8'}</option>
                                    {/foreach}
                                    </optgroup>
                                    <optgroup label="{l s='Features' mod='totalshoppingexporter'}">
                                    {foreach $features as $feature}
                                        <option value="{$feature['id']|escape:'html':'UTF-8'}"{if $include_material_attr == $feature['id']} selected{/if}>{$feature['name']|escape:'htmlall':'UTF-8'}</option>
                                    {/foreach}
                                    </optgroup>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-lg-3">
                                <label class="control-label" for="attributes-include_material">{l s='Age group' mod='totalshoppingexporter'}:</label>
                            </td>
                            <td class="col-lg-2">
                                <input type="checkbox" name="include_agegrp" value="true"{if $include_agegrp} checked{/if} />
                            </td>
                            <td class="col-lg-4">
                                <select name="include_agegrp_attr" class="form-control">
                                    <optgroup label="{l s='Attributes' mod='totalshoppingexporter'}">
                                        {foreach $attributes as $attribute}
                                        <option value="{$attribute['id']|escape:'html':'UTF-8'}"{if $include_agegrp_attr == $attribute['id']} selected{/if}>{$attribute['name']|escape:'htmlall':'UTF-8'}</option>
                                        {/foreach}
                                    </optgroup>
                                    <optgroup label="{l s='Features' mod='totalshoppingexporter'}">
                                        {foreach $features as $feature}
                                        <option value="{$feature['id']|escape:'html':'UTF-8'}"{if $include_agegrp_attr == $feature['id']} selected{/if}>{$feature['name']|escape:'htmlall':'UTF-8'}</option>
                                        {/foreach}
                                    </optgroup>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-lg-3">&nbsp;</td>
                            <td colspan="2">
                                <strong>{l s='Note' mod='totalshoppingexporter'}</strong>: {l s='You must be compliant with the' mod='totalshoppingexporter'} <a href="https://support.google.com/merchants/answer/188494?hl=" title="{l s='Google Shopping rules' mod='totalshoppingexporter'}" target="_blank">{l s='Google Shopping rules' mod='totalshoppingexporter'}</a> {l s='regarding the Age Group' mod='totalshoppingexporter'}
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="2">
                                <input type="hidden" name="section" value="attributes" />
                                <input type="submit" name="Save" value="{l s='Save the changes' mod='totalshoppingexporter'}" class="btn btn-default" />
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </form>
        </div>
        <div id="tse-exclude">
            <p>{l s='You can enter here your products ID that you wan\'t to remove from the generated list.' mod='totalshoppingexporter'} {l s='Please enter one ID per line.' mod='totalshoppingexporter'}</p>
            <form method="post" class="defaultForm form-horizontal">
                <div class="form-group">
                    <label class="col-lg-3 control-label">{l s='Exclude those products' mod='totalshoppingexporter'}:</label>
                    <div class="col-lg-3">
                        <textarea name="exclude_ids" class="form-control" cols="50" rows="5">{$exclude_ids|escape:'html':'UTF-8'}</textarea>
                        <p class="help-block">{l s='Please indicate one product ID per line that you wan\'t to exclude from the generated list.' mod='totalshoppingexporter'}</p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-offset-3 col-lg-9">
                        <input type="hidden" name="section" value="exclude" />
                        <input type="submit" name="Save" value="{l s='Save the changes' mod='totalshoppingexporter'}" class="btn btn-default" />
                    </div>
                </div>
            </form>
        </div>
        <div id="tse-cron">
            <p>{l s='In order to improve the overall performance of the generation of the feed\'s product, it\'s better to use a CRON Job.' mod='totalshoppingexporter'}</p>
            <p>{l s='In other words :' mod='totalshoppingexporter'}</p>
            <p style="margin-top: 20px">
                <strong>{l s='Without a CRON job' mod='totalshoppingexporter'}</strong>, {l s='when a comparator will hit the url, our module will load your entire database product and generate the file. The more products you have, the longer it will take to generate the file.' mod='totalshoppingexporter'}</p>
            <p>{l s='Moreover, if you have many products, this may lead to a maximum execution time exceeded.' mod='totalshoppingexporter'}</p>
            <p style="margin-top: 20px"><strong>{l s='With a CRON job' mod='totalshoppingexporter'}</strong>, {l s='the task will generate the file asynchronously and make sure it will be successfully created and saved in the cache folder of Prestashop.' mod='totalshoppingexporter'}</p>
            <p>{l s='When a comparator will hit your feeds products, our module won\'t load the database, but the cached file, which will be almost instantly, and with almost no resources.' mod='totalshoppingexporter'}</p>

            <p style="margin-top: 20px">{l s='The CRON job can be executed from every hours to every week, based on the frequency of the changes in your products database.' mod='totalshoppingexporter'}</p>
            <p>{l s='Of course, the more you call it, the more you will have accurate results, but this will also impact in your server performance.' mod='totalshoppingexporter'}</p>
            <p>{l s='We recommand you to fine-tune this regarding where your Prestashop is hosted.' mod='totalshoppingexporter'}</p>

            <p style="margin-top: 20px">{l s='We recommand to execute it every 6 hours, which means the file will be updated 4 times per day, which is more than enough for the comparators.' mod='totalshoppingexporter'}</p>
            <p>{l s='Here is the url for the CRON job :' mod='totalshoppingexporter'}</p>
            <code>{$base_url|escape:'html':'UTF-8'}cron.php?token={$secure_token|escape:'html':'UTF-8'}</code>

            <p style="margin-top: 20px">
                {l s='And the to add in the crontab file :' mod='totalshoppingexporter'}<br />
                {l s='(NOTE: You may have to replace "php" by "php5" or "php-cli" or "php5-cli" depending on your installation)' mod='totalshoppingexporter'}
            </p>
            <code>0 */6 * * * php {$base_path|escape:'html':'UTF-8'}cron.php</code>

            <p style="margin-top: 20px">{l s='You can choose either one, depending on your needs.' mod='totalshoppingexporter'}</p>
        </div>
        <div id="tse-help">
            <p>{l s='If you have any problem or find any bug, feel free to contact us via the Prestashop Addons website.' mod='totalshoppingexporter'}.</p>
            <p>{l s='We will do our best to answer your questions quickly' mod='totalshoppingexporter' }.</p>
            <p style="font-weight: bold; text-decoration: underline"><a href="https://addons.prestashop.com/en/write-to-developper?alternative=1&id_product={$module_id|escape:'html':'UTF-8'}" title="{l s='Click here to contact us.' mod='totalshoppingexporter'}" target="_blank">{l s='Click here to contact us.' mod='totalshoppingexporter'}</a></p>
        </div>
    </div>
</div>
