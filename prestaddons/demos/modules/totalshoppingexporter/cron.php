<?php
/**
 * Shopping Exporter
 *
 * This modules export your products catalog.
 *
 * If you find errors, bugs or if you want to share some improvments, feel free to contact at contact@prestaddons.net ! :)
 * Si vous trouvez des erreurs, des bugs ou si vous souhaitez tout simplement partager un conseil ou une amélioration,
 * n'hésitez pas à me contacter à contact@prestaddons.net
 *
 * @author    Cyril Nicodème <contact@prestaddons.net>
 * @copyright Copyright (C) June 2014 cnicodeme.com <email:contact@prestaddons.net>. All rights reserved.
 * @license   Nicodème Cyril
 * @since     2014-06-18
 * @package   modules
 * @version   2.6.5
 */

require_once(dirname(__FILE__).'/generator.php');
$generator = new TotalShoppingExporterGenerator();

if (!$generator->isValidToken()) {
    exit('Invalid token.');
}

@ini_set('max_execution_time', 0); /* Should suffice; */
@set_time_limit(0);

$generator->generateAll();
exit(0);
