/**
 * @package   modules
 * @author    Cyril Nicodème <contact@prestaddons.net>
 * @copyright Copyright (C) June 2014 cnicodeme.com <@email:contact@prestaddons.net>. All rights reserved.
 * @since     2014-06-18
 * @version   2.5.8
 * @license   Nicodème Cyril
 */

jQuery(function($) {
    var table = $('.table-step2');

    table.find('input').select2({
        'allowClear': false,
        'placeholder': table.data('placeholder'),
        'minimumInputLength': 2,
        'width': '100%',
        'query': function(query) {
            var data = {'results': []};

            for (var i = 0; i < exporter_taxonomies.length; i++) {
                if (exporter_taxonomies[i].text.toLowerCase().indexOf(query.term.toLowerCase()) >= 0) {
                    data.results.push({'id': exporter_taxonomies[i].id, 'text': exporter_taxonomies[i].text})
                    if (data.results.length >= 25) {
                        break;
                    }
                }
            }

            query.callback(data);
        },
        'initSelection': function (element, callback) {
            callback({id: element.val(), text: element.val()});
        }
    });
});
