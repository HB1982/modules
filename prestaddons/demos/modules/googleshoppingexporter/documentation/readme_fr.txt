Installation
============

L'installation de ce module est très simple.
Rendez-vous simplement dans l'administration de votre site Prestashop, onglet "Module", cliquez sur "Ajouter un nouveau module", sélectionnez l'archive et voilà ! :)

Une fois installé, vous devrez le configurer.
Normallement, vous devriez être redirigé sur la page de configuration de ce module, mais si ce n'est pas le cas,
allez dans la partie "Modules", recherchez le terme "Google Shopping Exporter" et cliquez sur "Configurer".


Configuration
=============

La page de configuration se décompose en plusieurs onglets.
Chaque onglets représente un aspect de la configuration.
Nous allons expliquer chaque onglets.


General
-------

Cet onglet liste les URLs à copier/coller de votre catalogue produits vers les sites de comparaison.

Après la première installation, cette page sera vide.
Vous devrez vous rendre sur l'onglet "Catégories" et définir quelles catégories vous souahitez exporter.

Une fois que vous aurez effectué cette confgiuration, cet onglet affichera les liens vers les produits à exporter.


Catégories and configuration
----------------------------

Cette partie est la plus compliquée, mais aussi la plus intéressante.
Vous aurez à définir quelle catégories vous souhaitez exporter, mais aussi vers quelles catégories de chaque comparateurs vous souhaitez exporter.

Nous comprenons que cette tâche est longue et lassante, surtout si vous avez de nombreuses catégories, mais c'est quelque chose que vous n'aurez normallement qu'à faire qu'une seule fois, et le résultat est très important : il a pour but d'améliorer le classement de vos produits et ainsi d'augmenter vos ventes!

Vous pourez aussi dans cette section définir la devise pour chaque langue.


Options
-------

Dans cet onglet, vous aurez la possibilité de spécificier différents paramètres pour l'export, tel que certains paramètres d'exclusion (produits sans description, sans référence, sans marque).
Vous aurez aussi la possibilité de spécifier le transporteur par défaut, afin que notre module calcule les frais de transport correctement.


Attributes
----------

Vos produits ont des attributs, tel que la couleur ou la taille. Ces attributs sont utilisé dans certains comparateurs tel que Google Shopping et c'est important de les prendre en compte lors de l'export afin d'améliorer le classement.
Cet onglet vous permet donc d'indiquer quel attribut correspond aux données attendue par certains moteurs de recherche afin d'optimiser encore plus vos résultats.


Tâche planifiée (CRON)
----------------------

Notre module permet la génération des flux via une tâche planifié (CRON).
Ce script va générer en avance vos flux produits et le stocker dans le cache de Prestashop afin de réduire la charche de votre serveur lorsque les moteurs de recherche iront récupèrer votre catalogue produit.

Nous vous recommandons fortement de mettre en place cette tâche planifiée afin d'optimiser les performances de votre serveur et de ne pas le faire ralentir.

L'onglet "Tâche planifiée" explique clairement et simplement comment mettre en place une tâche planifiée.


Contact
=======

Vous avez découvert un bug, vous avez des problèmes pour installer/configurer/utiliser ce module ?
N'hésitez pas à me contacter via Prestashop : https://addons.prestashop.com/en/write-to-developper?alternative=1&id_product=17627

Je ferai de mon mieux pour vous aider.


Merci !
=======

Vous avez acheté un de mes modules, et pour ca je tenais à vous remercier.
