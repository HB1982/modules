Installazione
============

L'installazione di questo modulo è abbastanza facile.
Tutto quello che dovete fare è andare nel vostro moduli elenco nel tuo backoffice Prestashop, fate clic su "Aggiungi un nuovo modulo", selezionare l'archivio e voilà! :)

Una volta installato, sarà necessario configurarlo.
Normalmente, si dovrebbe essere reindirizzati alla pagina di configurazione di questo modulo, ma nel caso non lo è,
andare a moduli, cercare "Google Shopping Exporter" e fare clic su "Configura collegamento".


Configurazione
=============

La pagina di configurazione viene visualizzata con più schede.
Ogni linguette rappresentano un aspetto della configurazione. Andremo attraverso ogni schede.


Generico
-------

Questa scheda elenca gli URL da copiare / incollare il catalogo prodotti per i siti web di confronto.

Dopo la prima installazione, questa scheda dovrebbe essere abbastanza vuoto.
Sarà necessario configurare questo modulo per avere questa scheda mostrano maggiori informazioni.

Devi andare al "Categorie" per definire quali categorie da esportare.
Una volta che avete fatto questa configurazione, questa scheda mostrerà i link dove esportare il tuo feed.


Categorie e configurazione
----------------------------

Questa è la più complicata ma più interessante sezione.
E 'dove si dovrà definire quali categorie si desidera esportare.
Avrete anche indicare in quale categoria si desidera mappare nel motore selezionato.

Ci rendiamo conto che questo è un lavoro meticoloso, ma si tratta di una grande ricompensa:
i vostri prodotti saranno meglio classificati nei confronti siti web rendendo così più numerosi e migliori vendite!

In questa sezione permetterà anche di definire quale valuta.


Opzioni
-------

In questa scheda, si avrà la possibilità di specificare quali prodotti da esportare, come esclusi i prodotti senza descrizione, riferimento, marca, ecc
Avrete anche la possibilità di indicare l'elemento portante di default, in modo da lasciare il nostro modulo di calcolare correttamente il prezzo di spedizione del prodotto.


Attributi
----------

I vostri prodotti hanno attributi, come il colore, dimensione, materiale. Questi attributi sono utilizzati in alcuni motori di confronto come Google lo shopping, ed è meglio usarli.
Noi li consideriamo importante! Questo il motivo per cui abbiamo incluso questa scheda. Potrai definire quale attribuiscono mappare la funzione corrispondente per migliorare i risultati delle perquisizioni di confronto.


Cron
----

Il nostro modulo è dotato di uno script che genera tutti i file nella cache Prestashop al fine di ridurre la carica del server quando il motore di comparazione ha colpito l'url dei vostri prodotti feed.
Questa scheda è qui per spiegarvi come funziona e perché lo deve usare.

Vi consigliamo vivamente di implementare un processo di cron per questo modulo perché sarà chiaramente migliorare le prestazioni generali.


Contatto
=======

Si trova un bug, avete alcuni problemi la configurazione / installazione / rendendo Questo modulo funziona?
Non esitate a contattarmi via Prestashop: https://addons.prestashop.com/en/write-to-developper?alternative=1&id_product=17627

Farò del mio meglio per aiutarvi.


Grazie!
========

Hai acquistato il mio modulo, e per questo, ho voluto grazie!
Spero che troverete quello che vi serve.
