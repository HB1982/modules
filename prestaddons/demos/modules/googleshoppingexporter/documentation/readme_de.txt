Installation
============

Die Installation dieses Moduls ist recht einfach.
Alles was Sie tun müssen, ist, in Ihre Module Auflistung in Ihrem Prestashop Backoffice gehen, klicken Sie in "Hinzufügen eines neuen Moduls", wählen Sie das Archiv und voilà! :)

Einmal installiert, werden Sie haben, um es zu konfigurieren.
Normalerweise sollte man auf die Konfigurationsseite des Moduls umgeleitet werden, aber in dem Fall nicht so ist,
gehen Sie zu Module, suchen Sie nach "Google Shopping Exporter" und klicken Sie auf die Schaltfläche "Configure Link".


Configuration
=============

Die Konfigurationsseite erscheint mit mehreren Registerkarten.
Jede Registerkarten stellen einen Aspekt der Konfiguration. Wir werden durch alle Registerkarten zu gehen.


Allgemein
-------

Diese Registerkarte eine Liste der URLs zu Ihren Produktkatalog zu den Vergleich Websites kopieren / einfügen.

Nach der ersten Installation sollte dieses Register ziemlich leer sein.
Sie müssen dieses Modul, um haben diese Registerkarte zeigen weitere Informationen zu konfigurieren.

Sie müssen in "Kategorien", um festzulegen, welche Kategorien für den Export gehen.
Sobald Sie diese Konfiguration durchgeführt, wird diese Registerkarte die Links zeigen, wo Ihr Feed exportieren.


Kategorien und Konfiguration
----------------------------

Dies ist der komplizierteste, aber interessantesten Seite.
Es ist, wo Sie müssen festlegen, welche Kategorien, die Sie exportieren möchten.
Sie haben auch in welche Kategorie Sie in der von Ihnen gewählten Motors zuordnen möchten anzuzeigen.

Wir verstehen dies ist eine anspruchsvolle Arbeit, aber es mit einer großen Belohnung kommt:
Ihre Produkte werden besser in den Vergleichen Webseiten damit mehr und bessere Verkaufs kategorisiert werden!

In diesem Abschnitt können Sie außerdem die Währung zu definieren.


Optionen
-------

In diesem Register wird Ihnen die Möglichkeit, festzulegen, welche Produkte für den Export, wie ausgenommen Waren ohne Beschreibung, Referenz, Marke, etc. haben
Sie haben auch die Möglichkeit, die Standard-Träger specifiy, um unser Modul berechnen Sie die Versandkosten für Ihr Produkt richtig zu lassen.


Attribute
----------

Ihre Produkte haben Eigenschaften, wie Farbe, Größe, Material. Diese Attribute werden in einigen Vergleichs Suchmaschinen wie Google Shopping verwendet, und es ist besser, sie zu benutzen.
Wir halten sie für wichtig! Deshalb haben wir inklusive des Inhalts. Sie definieren, welche Karte die entsprechende Funktion für die Verbesserung der Ergebnisse im Vergleich searchs zuzuschreiben.


Cron
----

Unser Modul kommt mit einem Skript, das alle Dateien in den Prestashop-Cache, um die Ladung des Servers zu reduzieren erzeugen wird, wenn die Vergleichsmaschine traf die URL Ihres Feeds Produkte.
Diese Registerkarte ist hier, um Ihnen zu erklären, wie es funktioniert und warum Sie es verwenden.

Möchten wir Ihnen besonders empfehlen Ihnen, einen Cron-Job für dieses Modul zu implementieren, weil es deutlich die Gesamtleistung zu verbessern.


Berührung
=======

Sie einen Fehler finden, haben Sie einige Probleme bei der Konfiguration / Installation / machen dieses Modul funktioniert?
Fühlen Sie sich frei, mit mir über Prestashop an: https://addons.prestashop.com/en/write-to-developper?alternative=1&id_product=17627

Ich werde mein Bestes tun, um Ihnen zu helfen.


Vielen Dank !
========

Sie kaufte meine Modul, und dafür wollte ich Ihnen danken!
Ich hoffe, Sie finden, was Sie brauchen.
