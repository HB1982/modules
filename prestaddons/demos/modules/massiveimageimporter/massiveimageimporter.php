<?php
/**
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

/*
 * TODO Compatibility for 1.5
 * TODO Better UI at some points
 * TODO Pagination images
 * TODO Order by last added
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class Massiveimageimporter extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'massiveimageimporter';
        $this->tab = 'quick_bulk_update';
        $this->version = '1.4.8';
        $this->author = 'Cyril Nicodeme';
        $this->need_instance = 0;
        $this->module_key = 'eda8710c5aad4bcdffb0338560a39674';
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Massive Image Importer');
        $this->description = $this->l('Let you upload and attach many images to products quickly and easily.');
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall this module?');
    }

    public function install()
    {
        if (!is_dir(_PS_UPLOAD_DIR_.'mii-images')) {
            mkdir(_PS_UPLOAD_DIR_.'mii-images');
        }

        return parent::install() &&
            $this->registerHook('backOfficeHeader');
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        if (Tools::isSubmit('search')) {
            echo $this->processSearch(Tools::getValue('search'));
            exit();
        } elseif (Tools::isSubmit('upload')) {
            echo $this->uploadImages();
            exit();
        } elseif (Tools::isSubmit('images')) {
            echo $this->loadImages(1);
            exit();
        } elseif (Tools::isSubmit('affect')) {
            echo $this->affectImages((int)Tools::getValue('id_product'), Tools::getValue('files'), (Tools::getValue('remove') === '1'));
            exit();
        } elseif (Tools::isSubmit('remove')) {
            echo $this->removeImages(Tools::getValue('image'));
            exit();
        }

        $this->setBackOfficeHeader();

        $this->context->smarty->assign('module_dir', $this->_path);
        return $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');
    }

    private function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = Tools::strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

    public function processSearch($query)
    {
        $products = Product::searchByName(Context::getContext()->language->id, $query);
        $result = array();
        foreach ($products as $product) {
            $result[] = array(
                'id' => $product['id_product'],
                'text' => $product['name'].' (Ref: '.$product['reference'].')',
            );
        }
        return Tools::jsonEncode(array('results' => $result));
    }

    public function uploadImages()
    {
        $result = array();

        if (!is_dir(_PS_UPLOAD_DIR_.'mii-images')) {
            mkdir(_PS_UPLOAD_DIR_.'mii-images');
        }

        foreach ($_FILES['files']['name'] as $key => $file) {
            if ($_FILES['files']['error'][$key] !== UPLOAD_ERR_OK) {

                switch($_FILES['files']['error'][$key]) {
                    case UPLOAD_ERR_INI_SIZE:
                        $message = $this->l('The uploaded file exceeds the upload_max_filesize directive in php.ini');
                        break;
                    case UPLOAD_ERR_FORM_SIZE:
                        $message = $this->l('The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form');
                        break;
                    case UPLOAD_ERR_PARTIAL:
                        $message = $this->l('The uploaded file was only partially uploaded');
                        break;
                    case UPLOAD_ERR_NO_FILE:
                        $message = $this->l('No file was uploaded');
                        break;
                    case UPLOAD_ERR_NO_TMP_DIR:
                        $message = $this->l('Missing a temporary folder');
                        break;
                    case UPLOAD_ERR_CANT_WRITE:
                        $message = $this->l('Failed to write file to disk');
                        break;
                    case UPLOAD_ERR_EXTENSION:
                        $message = $this->l('File upload stopped by extension');
                        break;
                }

                return Tools::jsonEncode(array('error' => $message));
            }

            $extension = pathinfo($_FILES['files']['name'][$key], PATHINFO_EXTENSION);
            $filename = $this->slugify(Tools::substr(basename($_FILES['files']['name'][$key]), 0, (Tools::strlen($extension) + 1) * -1));
            $destination = $filename .'.'. $extension;
            $index = 0;

            while (file_exists(_PS_UPLOAD_DIR_.'mii-images'.DIRECTORY_SEPARATOR.$destination)) {
                $index++;
                $destination = $filename.'-'.$index.'.'.$extension;
            }

            if (!@move_uploaded_file($_FILES['files']['tmp_name'][$key], _PS_UPLOAD_DIR_.'mii-images'.DIRECTORY_SEPARATOR.$destination)) {
                return Tools::jsonEncode(array('error' => $this->l('Unable to move the file.')));
            }

            $result[] = array('filename' => $destination, 'url' => _PS_BASE_URL_.__PS_BASE_URI__.'upload/mii-images/'.$destination);
        }

        return Tools::jsonEncode(array('success' => $result));
    }

    public function loadImages($page)
    {
        if (!isset($page) || $page < 1) {
            $page = 1;
        }

        $items = array();
        $counter = 0;
        foreach (new DirectoryIterator(_PS_UPLOAD_DIR_.'mii-images') as $file) {
            if ($file->isFile() && in_array(Tools::strtolower($file->getExtension()), array('jpg', 'jpeg', 'png', 'gif'))) {
                $items[] = array (
                    'filename' => $file->getFilename(),
                    'url' => _PS_BASE_URL_.__PS_BASE_URI__.'upload/mii-images/'.$file->getFilename()
                );
                $counter++;
            }
        }

        $items = array_slice($items, 0, 100);
        usort($items, function ($a, $b) {
            return strcmp($a['filename'], $b['filename']);
        });

        return Tools::jsonEncode(array('items' => $items));
    }

    public function affectImages($id_product, $images, $remove = true)
    {
        foreach ($images as $image) {
            $tmpName = _PS_UPLOAD_DIR_.'mii-images'.DIRECTORY_SEPARATOR.$image;
            if (!file_exists($tmpName)) {
                return Tools::jsonEncode(array('error' => sprintf($this->l('An error occurred while copying image: %s'), Tools::stripslashes($tmpName))));
            }

            $image = new Image();
            $image->id_product = $id_product;
            $image->position = Image::getHighestPosition($id_product) + 1;
            $image->cover = !Image::hasImages(Context::getContext()->language->id, $id_product); // True if first image
            $image->add();

            $image->createImgFolder();
            $new_path = $image->getPathForCreation();

            if (!ImageManager::resize($tmpName, $new_path.'.'.$image->image_format, null, null, $image->image_format)) {
                return Tools::jsonEncode(array('error' => sprintf($this->l('An error occurred while copying image: %s'), Tools::stripslashes($tmpName))));
            }

            $imagesTypes = ImageType::getImagesTypes('products');
            foreach ($imagesTypes as $imageType) {
                if (!ImageManager::resize($tmpName, $new_path.'-'.Tools::stripslashes($imageType['name']).'.'.$image->image_format, $imageType['width'], $imageType['height'], $image->image_format)) {
                    return Tools::jsonEncode(array('error' => sprintf($this->l('An error occurred while copying image: %s'), Tools::stripslashes($imageType['name']))));
                }
            }

            if ($remove) {
                if (@unlink($tmpName) === false) {
                    return Tools::jsonEncode(array('error' => $this->l('An error occurred while removing the image : %s'), Tools::stripslashes($tmpName)));
                }
            }
        }

        return Tools::jsonEncode(array('success' => true));
    }

    public function removeImages($name)
    {
        if (empty($name)) {
            return;
        }

        $filename = basename($name); // We ensure security measures
        if (@unlink(_PS_UPLOAD_DIR_.'mii-images'.DIRECTORY_SEPARATOR.$filename)) {
            return Tools::jsonEncode(array('success' => true));
        } else {
            return Tools::jsonEncode(array('success' => false));
        }
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function setBackOfficeHeader()
    {
        $this->context->controller->addJS($this->_path.'assets/select2-4.0.1/js/select2.full.min.js');
        $this->context->controller->addJS($this->_path.'assets/jquery-file-upload-9-11-2/js/jquery.iframe-transport.js');
        $this->context->controller->addJS($this->_path.'assets/jquery-file-upload-9-11-2/js/jquery.fileupload.js');
        $this->context->controller->addJS($this->_path.'assets/jquery-file-upload-9-11-2/js/jquery.fileupload-process.js');
        $this->context->controller->addJS($this->_path.'assets/jquery-file-upload-9-11-2/js/jquery.fileupload-validate.js');
        $this->context->controller->addJS($this->_path.'views/js/back.js');

        $this->context->controller->addCSS($this->_path.'views/css/back.css');
        $this->context->controller->addCSS($this->_path.'assets/select2-4.0.1/css/select2.min.css');
    }
}
