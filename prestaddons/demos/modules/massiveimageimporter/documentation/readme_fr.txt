Installation
============

L'installation de ce module est très simple.
Rendez-vous simplement dans l'administration de votre site Prestashop, onglet "Module", cliquez sur "Ajouter un nouveau module", sélectionnez l'archive et voilà ! :)

Une fois installé, vous n'aurez qu'à cliquer sur le bouton "Configurer" et vous accéderer à l'interface du Massive Importer.

Utilisation
===========

Nous avons développés ce module en gardant à l'esprit que le module soit très simple d'usage.
L'interface est agréable à utiliser et intuitive.

Tout d'abord, vous devez uploader des images. Pour ce faire, vous avez trois options :

1. En cliquant sur le bouton bleu en haut à droite. Vous aurez une fenêtre d'explorateur de fichier qui apparaîtra. Sélectionnez les images à envoyer (vous pouvez en choisir plusieurs) et elles seront sauvegardées sur le dossier de destination.
2. Vous pouvez aussi directement ouvrir l'explorateur de fichier depuis votre ordinateur, et drag&dropper les images depuis votre explorateur directement sur la page du module. Le module se chargera alors automatiquement de charger l'image.
3. Ou vous pouvez aussi utiliser votre logiciel FTP. Allez dans le dossier /upload/mii-images/ et envoyez toutes les images dans ce dossier. Après rafraichissement, le module affichera toutes les images compatibles.

Nous recommandons d'utiliser la methode n°3 si vous avez beaucoup d'images à uploader car c'est la façon la plus simple.

Une fois que vous avez des images visibles sur le module, rechercher votre produit dans le moteur de recherche présent sur la page du module et cliquez sur celui que vous voulez mettre à jour.

Ensuite, vous n'avez qu'à cliquer sur les images à appliquer au module. Leur arrière-plan deviendra vert pour vous indiquer que l'image est sélectionnée.
Une fois que vous avez finis de sélectionner les images, cliquez sur le bouton "Appliquer X images" et les images seront appliquées à votre produit.

Vous pouvez choisir de supprimer les images de l'explorateur ou de les garder.
En général, comme une image n'appartiens qu'à un seul produit, la suppression est automatique, mais si vous souhaitez appliquer une image à plusieurs produits, vous n'avez qu'à décocher la case à côté du bouton vert et l'image restera présente sur le serveur.

Contact
=======

Vous avez découvert un bug, vous avez des problèmes pour installer/configurer/utiliser ce module ?
N'hésitez pas à me contacter via Prestashop .

Je ferai de mon mieux pour vous aider.


Merci !
=======

Vous avez acheté un de mes modules, et pour ca je tenais à vous remercier.
