Installation
============

The installation of this module is pretty easy.
All you have to do is to go in your modules listing in your Prestashop backoffice, click in "Add a new module", select the archive and voilà ! :)

Once installed, all you have to do is click on the "Configure" button and you will access the Massive Importer interface.

Usage
=====

We developed this module by keeping in mind to make it absolutely easy to use.
The interface is really simple and we hope you will find it easy to use.

First, you need to upload some images. You have three possibilities to do so :

1. By clicking on the blue button, you will have a "File explorer" that will be shown, and you will be able to select images from your computer.
2. You can also open an Explorer window from your computer, go in the folder containing the images and drag and drop them on the module. They will be automatically uploaded.
3. Or finally you can access your FTP server, in the /upload/mii-images/ folder, and all the images that you place here will be visible on the module's page.

We recommand to use the method n°3 if you have many images to upload, it's the simplest way to go.

Once you have images available, search for your product by entering it's name in the search engine.
Click on the product you want and a green button will appear "Affect images.".

Now, all you have to do is click on the images you want to affect, their background will turn green to let you know they have been selected.
Then, click on the green button to upload them, and you will be notified the upload has been successful.

You can choose if you want to remove the files from the explorer or if you want to keep them.
In general, you will be better by removing them, but if some image is re-used in many products, you should uncheck the checkbox.


Contact
=======

You find a bug, you have some problems configuring/installing/making this module works?
Feel free to contact me via Prestashop

I'll do my best to help you.


Thanks !
========

You purchased my module, and for that, I wanted to thank you !
I hope you'll find what you need.
