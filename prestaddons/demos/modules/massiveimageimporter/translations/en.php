<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{massiveimageimporter}prestashop>massiveimageimporter_218edcb6753e56578619c94cfa1dbeac'] = 'Massive Image Importer';
$_MODULE['<{massiveimageimporter}prestashop>massiveimageimporter_5b0fef1ad0e1873a18bed3cef84e1ab4'] = 'Let you upload and attach many images to products quickly and easily.';
$_MODULE['<{massiveimageimporter}prestashop>massiveimageimporter_bb8956c67b82c7444a80c6b2433dd8b4'] = 'Are you sure you want to uninstall this module?';
$_MODULE['<{massiveimageimporter}prestashop>massiveimageimporter_2368e33e3e01d53abb9b60061ab83903'] = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
$_MODULE['<{massiveimageimporter}prestashop>massiveimageimporter_a10ef376d9f4c877ac86d8e4350116bf'] = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
$_MODULE['<{massiveimageimporter}prestashop>massiveimageimporter_f8fe8cba1625eaf8e5c253b041d57dbd'] = 'The uploaded file was only partially uploaded';
$_MODULE['<{massiveimageimporter}prestashop>massiveimageimporter_298883b17e36ee3a18d73e835c0b44fc'] = 'No file was uploaded';
$_MODULE['<{massiveimageimporter}prestashop>massiveimageimporter_2384d693d9af53b4727c092af7570a19'] = 'Missing a temporary folder';
$_MODULE['<{massiveimageimporter}prestashop>massiveimageimporter_f5985b2c059d5cc36968baab7585baba'] = 'Failed to write file to disk';
$_MODULE['<{massiveimageimporter}prestashop>massiveimageimporter_9e54dfe54e03b0010c1fe70bd65cd5e6'] = 'File upload stopped by extension';
$_MODULE['<{massiveimageimporter}prestashop>massiveimageimporter_e242e5ff8ec8f5b1a8ac6c45d342dbce'] = 'Unable to move the file.';
$_MODULE['<{massiveimageimporter}prestashop>massiveimageimporter_e6f03f7f18489dfa51f615809e034814'] = 'An error occurred while copying image: %s';
$_MODULE['<{massiveimageimporter}prestashop>massiveimageimporter_c3d4b88150d68f7672fc567fa00d84e5'] = 'An error occurred while removing the image : %s';

$_MODULE['<{massiveimageimporter}prestashop>configure_c08fe402e57f77dc319ead8df998a268'] = 'First, select your product via the Dropdown bellow, then click on the image you want to select, and once you are done, click on the \"Apply X images\" to affect them to the selected product.';
$_MODULE['<{massiveimageimporter}prestashop>configure_9023d2d5ae7cccc0775fcc272e72e0e0'] = 'It\'s that simple ! :)';
$_MODULE['<{massiveimageimporter}prestashop>configure_d282703885a2efa9bc71f7137ca2d609'] = 'Add images...';
$_MODULE['<{massiveimageimporter}prestashop>configure_f3e98433c32f199c878cbcd0e0c124ef'] = 'Enter your product\'s name';
$_MODULE['<{massiveimageimporter}prestashop>configure_9160f2f27f42d1c1f373d892ebf6e796'] = 'Apply images';
$_MODULE['<{massiveimageimporter}prestashop>configure_c71e71d18fc16345d54665f8c951e0ed'] = 'Remove the file when affected.';
$_MODULE['<{massiveimageimporter}prestashop>configure_06c5132cc9b405d614adce9206769680'] = 'Showing last 100 images';
$_MODULE['<{massiveimageimporter}prestashop>configure_9d5cc00abd0c859cd4ed3035f38ac5bc'] = 'Drop images here.';
$_MODULE['<{massiveimageimporter}prestashop>configure_140abba5bbbe848d81bd3b4313a8d0ac'] = 'You don\'t have any images availabe yet.';
$_MODULE['<{massiveimageimporter}prestashop>configure_e757027b2093a3587ea430e6b07dd82c'] = 'You can either upload them directly by using the blue button on the right side, drag&dropping them here';
$_MODULE['<{massiveimageimporter}prestashop>configure_ab377cd8814c885ca6cad7b545760d47'] = 'Or you can upload all your images using your FTP software: place the images in /upload/mii-images/ and they will appear here.';
$_MODULE['<{massiveimageimporter}prestashop>configure_6c7ed9863dcbf366c3a39b4cb3dc03e0'] = 'Loading ...';
$_MODULE['<{massiveimageimporter}prestashop>configure_73c8a0a1101c3a8b45d2115a99c7c9c7'] = 'Remove this image';
$_MODULE['<{massiveimageimporter}prestashop>configure_6b1f3df9d7100e5e98dc6d067b343c0e'] = 'Your file has been successfully applied to your product.';
$_MODULE['<{massiveimageimporter}prestashop>configure_e92187106a4e65a220a0de0bbc593edd'] = 'Are you sure you want to remove this image ?';
$_MODULE['<{massiveimageimporter}prestashop>configure_3f1c5b0049a9751e6f5f9e702b74a0dd'] = 'Uploading';
$_MODULE['<{massiveimageimporter}prestashop>configure_f86fd657fc364013743fe73490b8ae2d'] = 'File upload failed.';
$_MODULE['<{massiveimageimporter}prestashop>configure_a8cba35589ed9136863714a6d9b75f2d'] = 'Uploaded!';
