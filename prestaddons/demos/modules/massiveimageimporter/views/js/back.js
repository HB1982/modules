/**
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2016 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*
* Don't forget to prefix your containers with your own identifier
* to avoid any conflicts with others containers.
*/

jQuery(function ($) {
    var base = $('#mii-panel-system');

    function imageRenderer(item) {
        return $('<div />', {'data-name': item.filename, 'class': (item.disabled ? 'disabled' : '')}).append(
            $('<em />', {'text': 'Uploading (0%)', 'style': 'font-size: 1.2em; margin-bottom: 10px'}),
            $('<a />', {'href': 'javascript:;', 'title': MassiveImageUploader.removeImage, 'html': '&times', 'class': 'close'}),
            $('<img />', {'class': 'img-responsive', 'src': item.url}),
            $('<p />').append(
                $('<span />', {'text': item.filename})
            )
        );
    }

    function loadImages() {
        var container = base.find('>.pane-images>.items');
        container.empty().append($('<div />', {'text': MassiveImageUploader.loading}).css({
            'text-align': 'center',
            'font-style': 'italic'
        }));

        $.ajax({
            url: base.data('base-url') + '&images',
            method: 'GET',
            dataType: 'json'
        }).success(function (data) {
            container.empty();
            if (data.items.length >= 100) {
                container.parent().find('>p').show()
            } else {
                container.parent().find('>p').hide()
            }

            if (data.items.length === 0) {
                container.parent().find('>.empty').show();
            } else {
                container.parent().find('>.empty').hide();
                for (var i = 0; i < data.items.length; i++) {
                    data.items[i]['disabled'] = false;
                    container.append(
                        imageRenderer(data.items[i])
                    )
                }
            }
        })
    }
    loadImages(1);

    base.find('form .btn-success').prop('disabled', true);
    base.find('form .toggle-view').hide();

    base.find('form').on('submit', function() {
        var btn = $(this).find('.btn-success'),
            idProductSelected = $('#mii-product-search').val();

        btn.prop('disabled', true);

        var files = [],
            selected = base.find('>.pane-images .selected');

        for (var i = 0; i < selected.length; i++) {
            files.push(selected[i].dataset['name'])
        }

        base.find('#mii-product-search').val('').trigger('change');
        var isToRemove = $('#mii-images-remove').prop('checked');

        selected.removeClass('selected');
        selected.addClass('disabled');

        $.ajax({
            url: base.data('base-url') + '&affect',
            method: 'POST',
            data: {
                'files': files,
                'id_product': idProductSelected,
                'remove': isToRemove ? '1' : '0'
            }
        }).success(function (data) {
            selected.removeClass('disabled');
            if (data.error) {
                jQuery.growl.error({'title': 'Oups!', 'message': data.error});
            } else {
                jQuery.growl.notice({'title': 'Success!', 'message': MassiveImageUploader.success});

                if (isToRemove) {
                    selected.remove();
                }
            }
        })

        return false;
    });

    base.find('#mii-product-search').select2({
        ajax: {
            url: base.data('base-url'),
            dataType: 'json',
            processResults: function (data) {
                return {
                    results: data.results
                };
            },
            delay: 250,
            data: function (params) {
                return {
                    search: params.term
                }
            }
        },
        minimumInputLength: 1
    }).on('change', function () {
        var btnSuccess = base.find('form .btn-success');

        if ($(this).val() === '') {
            base.find('form .toggle-view').hide();
        } else {
            base.find('form .toggle-view').show();
        }
    })

    base.find('>.pane-images>.items').on('click', '>div', function () {
        var element = $(this);
        if (element.hasClass('disabled')) return false;

        element.toggleClass('selected');

        var selectedQuantity = base.find('>.pane-images .selected').length;
        base.find('form .btn-success').prop('disabled', (selectedQuantity === 0)).val('Apply ' + selectedQuantity + ' images.');
    });

    base.find('>.pane-images>.items').on('click', '>div .close', function (e) {
        e.preventDefault();
        e.stopPropagation();

        if (confirm(MassiveImageUploader.removeConfirm)) {
            var parent = $(this).parent();
            $.ajax({
                url: base.data('base-url') + '&remove&image=' + parent.data('name'),
                method: 'POST'
            });
            parent.fadeOut(250, function () {
                $(this).remove()
            })
        }
    });

    if ($.support.fileInput) {
        $('#fileupload').closest('.pull-right').show();
        var uploadIndex = 0;

        $('#fileupload').fileupload({
            url: base.data('base-url') + '&upload',
            dataType: 'json',
            autoUpload: true,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
            maxNumberOfFiles: 5,
            maxFileSize: 999000
        }).on('fileuploadadd', function (e, data) {
            base.find('>.pane-images>.empty').hide();

            data.context = base.find('>.pane-images>.items');
            var reader = new FileReader();

            $.each(data.files, function (index, file) {
                data.index = uploadIndex;
                uploadIndex++;

                var imgTag = imageRenderer({'filename': file.name, 'url': null, 'disabled': true})

                reader.onload = function(e) {
                    imgTag.find('img').attr('src', e.target.result);
                }
                reader.readAsDataURL(file);
                data.context.prepend(imgTag);
            });
        }).on('fileuploadprocessalways', function (e, data) {
            var index = data.index,
                file = data.files[index],
                node = $(data.context.children()[index]);

            if (file.error) {
                node.find('>em').text(file.error).addClass('text-danger');
            }
        }).on('fileuploadprogress', function (e, data) {
            var index = data.index,
                file = data.files[index],
                node = $(data.context.children()[index]),
                progress = parseInt(data.loaded / data.total * 100, 10);

            node.find('>em').text(MassiveImageUploader.uploading + ' (' + progress + '%)');
        }).on('fileuploaddone', function (e, data) {
            var index = data.index,
                file = data.files[index],
                node = $(data.context.children()[index]);

            node.removeClass('disabled');
            if (data.result.error) {
                node.find('>em').text(data.result.error).addClass('text-danger');
            } else {
                node.data('name', data.result.success[0].filename);
                node.find('img').attr('src', data.result.success[0].url);
                node.find('>em').text(MassiveImageUploader.uploaded).addClass('text-success');
            }
        }).on('fileuploadfail', function (e, data) {
            var index = data.index,
                file = data.files[index],
                node = $(data.context.children()[index]);

            node.removeClass('disabled');
            node.find('>em').text(MassiveImageUploader.uploadFailed).addClass('text-danger');
        });

        $(document).on('dragover', function (e) {
            base.find('>.pane-images').addClass('dropping');
        });

        $(document).on('dragend drop', function (e) {
            base.find('>.pane-images').removeClass('dropping');
        });

        $(document).on('drop dragover', function (e) {
            e.preventDefault();
        });
    }
});
