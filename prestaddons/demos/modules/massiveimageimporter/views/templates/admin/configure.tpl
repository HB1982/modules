{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2016 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="panel" id="mii-panel-system" data-base-url="{$smarty.server.REQUEST_URI|escape:'htmlall':'UTF-8'}">
	<div>
		<div class="alert alert-info">
			<p>{l s='First, select your product via the Dropdown bellow, then click on the image you want to select, and once you are done, click on the "Apply X images" to affect them to the selected product.' mod='massiveimageimporter'}</p>
			<p>{l s='It\'s that simple ! :)' mod='massiveimageimporter'}</p>
		</div>
		<div class="pull-right" style="display: none">
			<span class="btn btn-primary fileinput-button">
		        <i class="glyphicon glyphicon-plus"></i>
		        <span>{l s='Add images...' mod='massiveimageimporter'}</span>
		        <!-- The file input field used as target for the file upload widget -->
		        <input id="fileupload" type="file" name="files[]" multiple="">
		    </span>
		</div>

		<form class="form-inline" method="post" action="{$smarty.server.REQUEST_URI|escape:'htmlall':'UTF-8'}">
			<div class="form-group">
				<select class="form-control" id="mii-product-search" placeholder="{l s='Enter your product\'s name' mod='massiveimageimporter'}" style="width:300px">
					<option value="" selected="selected">{l s='Enter your product\'s name' mod='massiveimageimporter'}</option>
				</select>
			</div>
			<input type="submit" name="save" value="{l s='Apply images' mod='massiveimageimporter'}" class="btn btn-success toggle-view" />
			<div class="form-group toggle-view">
				<label>
					<input type="checkbox" name="remove" value="1" id="mii-images-remove" checked /> {l s='Remove the file when affected.' mod='massiveimageimporter'}
				</label>
			</div>
		</form>
	</div>
	<hr />
	<div class="pane-images row auto-clear">
		<p style="font-style: italic; display: none">{l s='Showing last 100 images' mod='massiveimageimporter'}:</p>
		<div class="items"></div>
		<div class="dropmessage">{l s='Drop images here.' mod='massiveimageimporter'}</div>
		<div class="empty" style="display: none">
			<div class="alert alert-warning">
				<p>{l s='You don\'t have any images availabe yet.' mod='massiveimageimporter'}</p>
				<p>{l s='You can either upload them directly by using the blue button on the right side, drag&amp;dropping them here' mod='massiveimageimporter'}</p>
				<p>{l s='Or you can upload all your images using your FTP software: place the images in <strong>/upload/mii-images/</strong> and they will appear here.' mod='massiveimageimporter'}</p>
			</div>
		</div>
	</div>
</div>

<script>
var MassiveImageUploader = {
	'loading': "{l s='Loading ...' mod='massiveimageimporter'}",
	'removeImage': "{l s='Remove this image' mod='massiveimageimporter'}",
	'success': "{l s='Your file has been successfully applied to your product.' mod='massiveimageimporter'}",
	'removeConfirm': "{l s='Are you sure you want to remove this image ?' mod='massiveimageimporter'}",
	'uploading': "{l s='Uploading' mod='massiveimageimporter'}",
	'uploadFailed': "{l s='File upload failed.' mod='massiveimageimporter'}",
	'uploaded': "{l s='Uploaded!' mod='massiveimageimporter'}"
}
</script>
