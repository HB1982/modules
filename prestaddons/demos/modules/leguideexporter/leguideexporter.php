<?php
/**
 * LeGuide Exporter
 *
 * This modules export your products catalog
 *
 * If you find errors, bugs or if you want to share some improvments, feel free to contact at contact@prestaddons.net ! :)
 * Si vous trouvez des erreurs, des bugs ou si vous souhaitez tout simplement partager un conseil ou une amélioration,
 * n'hésitez pas à me contacter à contact@prestaddons.net
 *
 * @author    Cyril Nicodème <contact@prestaddons.net>
 * @copyright Copyright (C) June 2014 cnicodeme.com <@email:contact@prestaddons.net>. All rights reserved.
 * @license   Nicodème Cyril
 * @version   2.6.0
 * @since     2014-06-18
 * @package   modules
 */

/*
 * TODO :
 *      Catégories multiples (https://addons.prestashop.com/en/seller-contact.php?ict=136513)
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class LeguideExporter extends Module
{
    private static $basename = 'LGEXPORTER';

    private $tracker_details = null;

    public function __construct()
    {
        $this->name = 'leguideexporter';
        $this->tab = 'export';
        $this->version = '2.6.0';
        $this->author = 'Cyril Nicodeme';
        $this->need_instance = 1;
        $this->module_key = '4868144c9593d660ef27af329af2cc71';
        $this->bootstrap = true;

        $this->module_id = 17630;
        $this->engine = 'leguide';

        parent::__construct();

        $this->displayName = $this->l('LeGuide Exporter');
        $this->description = $this->l('Export your products catalog to LeGuide comparison website.');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

        if (!is_dir(_PS_CACHE_DIR_) || !is_writable(_PS_CACHE_DIR_)) {
            $this->warning = $this->l('Your /cache/ folder must be writeable to make this module to work correctly.');
        }

        if (!ini_get('allow_url_fopen')) {
            $this->warning = $this->l('You need to have "allow_url_fopen" to on for this module to work.');
        }

        $shopId = Configuration::get(self::$basename.'_shop_id');
        if (!is_null($shopId)) {
            $this->tracker_details = array (
                'version' => '1.3',
                'idshop' => $shopId
            );
        }
    }

    public function install()
    {
        if (!parent::install()) {
            return false;
        }

        if (!$this->registerHook('displayOrderConfirmation')) {
            return false;
        }
        if (!$this->registerHook('displayFooter')) {
            return false;
        }

        Configuration::updateValue(self::$basename.'_engine_leguide', true);
        Configuration::deleteByName(self::$basename.'_secure_token');
        Configuration::updateValue(self::$basename.'_secure_token', uniqid());

        Configuration::updateValue(self::$basename.'_shop_id', null);
        Configuration::updateValue(self::$basename.'_export_stock', false);
        Configuration::updateValue(self::$basename.'_stock_state', 'out of stock'); // available for order ; out of stock ; preorder
        Configuration::updateValue(self::$basename.'_export_no_description', false);
        Configuration::updateValue(self::$basename.'_export_combination', true);
        Configuration::updateValue(self::$basename.'_export_no_ean13', false);
        Configuration::updateValue(self::$basename.'_export_no_brand', false); // false = we do not export product wi no brand
        Configuration::updateValue(self::$basename.'_description_size', 'long'); // long, small
        Configuration::updateValue(self::$basename.'_product_condition', 'new'); // new, used, refurbished

        $imageTypes = ImageTypeCore::getImagesTypes('products');
        if (count($imageTypes) > 0) {
            Configuration::updateValue(self::$basename.'_image_size', $imageTypes[0]['name']);
        } else {
            Configuration::updateValue(self::$basename.'_image_size', null);
        }

        Configuration::updateValue(self::$basename.'_cache_duration', 72);

        Configuration::updateValue(self::$basename.'_include_color', false);
        Configuration::updateValue(self::$basename.'_include_color_attr', false);

        Configuration::updateValue(self::$basename.'_include_gender', false);
        Configuration::updateValue(self::$basename.'_include_gender_attr', false);

        Configuration::updateValue(self::$basename.'_include_size', false);
        Configuration::updateValue(self::$basename.'_include_size_attr', false);

        Configuration::updateValue(self::$basename.'_include_pattern', false);
        Configuration::updateValue(self::$basename.'_include_pattern_attr', false);

        Configuration::updateValue(self::$basename.'_include_material', false);
        Configuration::updateValue(self::$basename.'_include_material_attr', false);

        Configuration::updateValue(self::$basename.'_include_agegrp', false);
        Configuration::updateValue(self::$basename.'_include_agegrp_attr', false);

        Configuration::updateValue(self::$basename.'_exclude_ids', null);

        Configuration::updateValue(self::$basename.'_categories', null); // serialized !

        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall()) {
            return false;
        }

        if (!$this->unregisterHook('displayOrderConfirmation')) {
            return false;
        }
        if (!$this->unregisterHook('displayFooter')) {
            return false;
        }

        Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'configuration` WHERE `name` LIKE "'.self::$basename.'_%";');
        $this->deleteAllGeneratedFiles();

        return true;
    }

    private function deleteAllGeneratedFiles($lang = null)
    {
        require_once(dirname(__FILE__).'/generator.php');
        ShoppingExporterGenerator::deleteFiles($lang);
    }

    private function getEngineCategories($lang)
    {
        switch (Language::getIsoById($lang)) {
            case 'fr':
                $lang = 'fr_FR';
                break;
            case 'sv':
                $lang = 'sv_SE';
                break;
            case 'da':
                $lang = 'da_DK';
                break;
            case 'de':
                $lang = 'de_DE';
                break;
            case 'es':
                $lang = 'es_ES';
                break;
            case 'it':
                $lang = 'it_IT';
                break;
            case 'nl':
                $lang = 'nl_NL';
                break;
            case 'pl':
                $lang = 'pl_PL';
                break;
            default:
                $lang = 'en_GB';
                break;
        }

        $headers = @get_headers('http://selfcategorization.leguide.com:8089/lgexport/thesaurusServices/getThesaurus/CSV/'.$lang);
        if ($headers !== false && strpos($headers[0], '404') !== false) {
            $lang = 'en_GB';
        }

        $file_path = _PS_CACHE_DIR_.'tse_leguide_taxonomies_'.$lang.'.txt';
        if (!is_file($file_path)) {
            if (ini_get('allow_url_fopen')) {
                if (file_put_contents($file_path, Tools::file_get_contents('http://selfcategorization.leguide.com:8089/lgexport/thesaurusServices/getThesaurus/CSV/'.$lang)) === false) {
                    exit(Tools::jsonEncode('An error occured while trying to download the taxonomies.'));
                }

                @chmod($file_path, 0644);
            } else {
                exit($this->l('You need to enable allow_url_fopen to be able to download the taxonomies.'));
            }
        }

        $fp = fopen($file_path, 'r');
        $results = array ();
        while (($data = fgetcsv($fp, 1000, ';')) !== false) {
            $results[] = array (
                'id' => 'leguide#'.$data[0].'-'.$data[1],
                'text' => $data[1]
            );
        }

        return $results;
    }

    private function showStep1($assigns)
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->module = $this;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);
        $helper->default_form_language = $helper->allow_employee_form_lang;

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitStep1';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&view=categorie&step=2';
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $values = array(
            'language'            => null,
            'currency_id'        => null,
            'carrier_id'        => null,
            'delivery_schedule' => null,
            'categories'        => array()
        );

        if (Tools::isSubmit('lang_id')) {
            $helper->currentIndex .= '&lang_id='.Tools::getValue('lang_id');

            $values = array(
                'language'          => $assigns['languages'][Tools::getValue('lang_id')]['name'],
                'currency_id'       => $assigns['categories'][Tools::getValue('lang_id')]['currency_id'],
                'carrier_id'        => $assigns['categories'][Tools::getValue('lang_id')]['carrier_id'],
                'delivery_schedule' => $assigns['categories'][Tools::getValue('lang_id')]['delivery_schedule'],
                'categories'        => array_keys($assigns['categories'][Tools::getValue('lang_id')]['engines'][$this->engine])
            );

            $values['language'] = '<p class="form-control-static" style="font-weight: bold">'.$values['language'].'</p>';
        } else {
            if (isset($assigns['categories']) && is_array($assigns['categories'])) {
                foreach ($assigns['categories'] as $lang => $category) {
                    unset($assigns['languages'][$lang]);
                }
            }
        }

        if (Tools::isSubmit('language')) {
            $values['language'] = Tools::getValue('language');
        }
        if (Tools::isSubmit('currency_id')) {
            $values['currency_id'] = Tools::getValue('currency_id');
        }
        if (Tools::isSubmit('carrier_id')) {
            $values['carrier_id'] = Tools::getValue('carrier_id');
        }
        if (Tools::isSubmit('delivery_schedule')) {
            $values['delivery_schedule'] = Tools::getValue('delivery_schedule');
        }
        if (Tools::isSubmit('selectedCategories')) {
            $values['categories'] = Tools::getValue('selectedCategories');
        }

        $helper->tpl_vars = array('fields_value' => $values);

        $form_structure = array(
            array(
                'form' => array(
                    'tinymce' => false,
                    'input' => array(
                        array(
                            'type' => (Tools::isSubmit('lang_id') ? 'free' : 'select'),
                            'label' => $this->l('Language:'),
                            'name' => 'language',
                            'options' => array(
                                'query' => $assigns['languages'],
                                'id' => 'id',
                                'name' => 'name'
                            ),
                            'class' => 'fixed-width-xxl'
                        ),
                        array(
                            'type' => 'select',
                            'label' => $this->l('Currency:'),
                            'name' => 'currency_id',
                            'options' => array(
                                'query' => $assigns['currencies'],
                                'id' => 'id',
                                'name' => 'name'
                            ),
                            'class' => 'fixed-width-xxl'
                        ),
                        array(
                            'type' => 'select',
                            'label' => $this->l('Carrier:'),
                            'name' => 'carrier_id',
                            'options' => array(
                                'query' => $assigns['carriers'],
                                'id' => 'id',
                                'name' => 'name'
                            ),
                            'class' => 'fixed-width-xxl'
                        ),
                        array(
                            'type' => 'text',
                            'label' => $this->l('Delivery Schedule:'),
                            'name' => 'delivery_schedule',
                            'required' => true,
                            'suffix' => $this->l('days'),
                            'desc' => $this->l('Number of days it generally takes to deliver the product through this carrier.')
                        )
                    ),
                    'submit' => array(
                        'title' => $this->l('Continue'),
                    ),
                ),
            )
        );

        if (class_exists('HelperTreeCategories')) {
            $root = Category::getRootCategory();
            $selectedCategories = new HelperTreeCategories('categories-tree');
            $selectedCategories->setUseCheckBox(false);
            $selectedCategories->setAttribute('is_category_filter', $root->id);
            $selectedCategories->setUseCheckBox(true);
            $selectedCategories->setRootCategory($root->id);
            $selectedCategories->setSelectedCategories($values['categories']);
            $selectedCategories->setUseSearch(true);
            $selectedCategories->setInputName('selectedCategories');

            $form_structure[0]['form']['input'][] = array(
                'type'  => 'categories_select',
                'label' => $this->l('Categories to export:'),
                'desc'    => $this->l('Select which main categories to export. Note: Only products that have the main categories as selected will be exported.'),
                'name'  => 'selectedCategories[]',
                'category_tree'  => $selectedCategories->render()
            );
        } else {
            $form_structure[0]['form']['input'][] = array(
                'type' => 'categories',
                'name' => 'selectedCategories',
                'label' => $this->l('Categories to export:'),
                'tree' => array(
                    'id' => 'categories-tree',
                    'selected_categories' => $values['categories'],
                    'use_search' => true,
                    'use_checkbox' => true
                ),
                'desc' => $this->l('Select which main categories to export. Note: Only products that have the main categories as selected will be exported.'),
                'values' => array (
                    'selected_cat' => $values['categories'],
                    'input_name' => 'selectedCategories[]',
                    'use_radio' => false,
                    'use_search' => true,
                    'top_category' => Category::getTopCategory(),
                    'use_context' => true,
                )
            );
        }

        return $helper->generateForm($form_structure);
    }

    private function processStep1()
    {
        if (!is_numeric(Tools::getValue('delivery_schedule'))) {
            throw new Exception($this->l('Please indicate a valid delivery schedule (in days).'));
        }

        if (!Tools::isSubmit('submitStep2') && Tools::getValue('selectedCategories') === false) {
            throw new Exception($this->l('Please select at least one category.'));
        }

        // ----------------
        // Added because with Kelkoo, there is no need to use the second step anymore!

        $data = array (
            'currency_id'       => Tools::getValue('currency_id'),
            'carrier_id'        => Tools::getValue('carrier_id'),
            'delivery_schedule' => Tools::getValue('delivery_schedule'),
            'language'          => (Tools::isSubmit('language') ? Tools::getValue('language') : Tools::getValue('lang_id')),
            'categories'        => Tools::getValue('selectedCategories')
        );

        $this->deleteAllGeneratedFiles($data['language']);

        $original[$data['language']]['currency_id'] = $data['currency_id'];

        if (isset($original[$data['language']]['currency'])) {
            unset($original[$data['language']]['currency']);
        }

        $original[$data['language']]['carrier_id'] = $data['carrier_id'];
        $original[$data['language']]['delivery_schedule'] = $data['delivery_schedule'];

        $original[$data['language']]['engines'][$this->engine] = array();
        foreach ($data['categories'] as $category_id) {
            $original[$data['language']]['engines'][$this->engine][$category_id] = true;
        }

        Configuration::updateValue(self::$basename.'_categories', serialize($original));
        Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true).'&configure='.$this->name);

        /*
        // Old version =>
        return $data
        */
    }

    private function showStep2($form_data, $defined = array())
    {
        if (Tools::isSubmit('submitStep2')) {
            $form_data['categories'] = explode(',', $form_data['categories']);
        }

        $existing_categories = array();
        foreach (Category::getCategories($form_data['language'], false, false) as $category) {
            $existing_categories[$category['id_category']] = array (
                'id_category' => $category['id_category'],
                'name'        => $category['name']
            );
        }

        $categories = array();
        foreach ($form_data['categories'] as $category_id) {
            $category = array (
                'category_id' => $category_id,
                'name'        => $existing_categories[$category_id]['name'],
                'value'       => null
            );

            if (Tools::isSubmit('mapping_'.$category_id)) {
                $category['value'] = Tools::getValue('mapping_'.$category_id);
            } elseif (isset($defined[$category_id])) {
                $category['value'] = $defined[$category_id];
            }

            $categories[] = $category;
        }

        $form_data['categories'] = implode(',', $form_data['categories']);

        $this->context->smarty->assign(array (
            'form_data' => $form_data,
            'categories' => $categories,
            'mapping' => Tools::jsonEncode($this->getEngineCategories($form_data['language']))
        ));

        return $this->context->smarty->fetch($this->local_path.'views/templates/admin/step2.tpl');
    }

    private function processStep2($data, $original)
    {
        $this->deleteAllGeneratedFiles($data['language']);

        $original[$data['language']]['currency_id'] = $data['currency_id'];

        if (isset($original[$data['language']]['currency'])) {
            unset($original[$data['language']]['currency']);
        }

        $original[$data['language']]['carrier_id'] = $data['carrier_id'];
        $original[$data['language']]['delivery_schedule'] = $data['delivery_schedule'];

        $original[$data['language']]['engines'][$this->engine] = array();
        foreach (explode(',', Tools::getValue('categories')) as $category_id) {
            $original[$data['language']]['engines'][$this->engine][$category_id] = Tools::getValue('mapping_'.$category_id, '');
        }

        Configuration::updateValue(self::$basename.'_categories', serialize($original));
        Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true).'&configure='.$this->name);
    }

    public function getContent()
    {
        $assigns = array();

        $settings = Configuration::getMultiple(array (
            self::$basename.'_export_stock', self::$basename.'_stock_state', self::$basename.'_export_no_description', self::$basename.'_shop_id',
            self::$basename.'_export_combination', self::$basename.'_export_no_ean13', self::$basename.'_export_no_brand', self::$basename.'_description_size',
            self::$basename.'_product_condition', self::$basename.'_include_color', self::$basename.'_include_color_attr',
            self::$basename.'_include_size', self::$basename.'_include_gender', self::$basename.'_include_gender_attr',
            self::$basename.'_include_material', self::$basename.'_include_material_attr', self::$basename.'_include_agegrp', self::$basename.'_include_agegrp_attr',
            self::$basename.'_include_size_attr', self::$basename.'_include_pattern', self::$basename.'_include_pattern_attr',
            self::$basename.'_image_size', self::$basename.'_exclude_ids', self::$basename.'_default_carrier', self::$basename.'_delivery_schedule',

            self::$basename.'_cache_duration', self::$basename.'_categories', self::$basename.'_secure_token'
        ));

        foreach ($settings as $key => $value) {
            $assigns[Tools::substr($key, Tools::strlen(self::$basename) + 1)] = $value;
        }

        $assigns['categories'] = unserialize($assigns['categories']);
        if ($assigns['categories'] === false) {
            $assigns['categories'] = array();
        }

        $languages = array();
        foreach (Language::getLanguages(true) as $language) {
            $languages[$language['id_lang']] = array (
                'id' => $language['id_lang'],
                'name' => $language['name'],
                'code' => $language['language_code'],
            );
        }
        $assigns['languages'] = $languages;

        $currencies = array();
        if (version_compare(Tools::substr(_PS_VERSION_, 0, 5), '1.7.6', '<')) {
            foreach (Currency::getCurrencies(false, true) as $currency) {
                if ((int)$currency['id_shop'] !== $this->context->shop->id) {
                    continue;
                }

                $currencies[$currency['id_currency']] = array (
                    'id' => $currency['id_currency'],
                    'name' => $currency['name']
                );
            }
        }
        else {
            foreach (Currency::findAll(true, false) as $currency) {
                if ((int)$currency['id_shop'] !== $this->context->shop->id) {
                    continue;
                }
    
                $currencies[$currency['id_currency']] = array (
                    'id' => $currency['id_currency'],
                    'name' => $currency['iso_code']
                );
            }
        }
        $assigns['currencies'] = $currencies;

        $carriers = array();
        foreach (Carrier::getCarriers($this->context->language->id, true, false, false, null, Carrier::ALL_CARRIERS) as $carrier) {
            $carriers[$carrier['id_reference']] = array (
                'id' => $carrier['id_reference'],
                'name' => $carrier['name']
            );
        }
        $assigns['carriers'] = $carriers;

        // Retro-compatibility
        if (isset($assigns['categories']) && is_array($assigns['categories'])) {
            foreach ($assigns['categories'] as $key => $value) {
                if (isset($assigns['categories'][$key]['currency'])) {
                    $assigns['categories'][$key]['currency_id'] = $assigns['categories'][$key]['currency'];
                }

                if (!isset($assigns['categories'][$key]['carrier_id'])) {
                    $assigns['categories'][$key]['carrier_id'] = $assigns['default_carrier'];
                }

                if (!isset($assigns['categories'][$key]['delivery_schedule'])) {
                    $assigns['categories'][$key]['delivery_schedule'] = $assigns['delivery_schedule'];
                }
            }
        }
        // EOL Retro-compatibility

        if (Tools::isSubmit('view')) {
            Tools::safePostVars();

            $content = '<div class="pull-right"><h1><small>'.$this->l('Step').' '.Tools::getValue('step').'/2</small></h1></div><div><h1>LeGuide Exporter<br />';
            if (Tools::isSubmit('lang_id')) {
                $content .= '<small>'.$this->l('Updating language').' '.$assigns['languages'][Tools::getValue('lang_id')]['name'];
            } else {
                $content .= '<small>'.$this->l('Adding a new language');
            }

            $content .= '</small></h1></div>';

            switch (Tools::getValue('step')) {
                case '1':
                    return $content.$this->showStep1($assigns);
                case '2':
                    $step1_data = null;
                    try {
                        $step1_data = $this->processStep1();
                    } catch (Exception $e) {
                        $content .= $this->displayError($e->getMessage());

                        return $content.$this->showStep1($assigns);
                    }

                    if (Tools::isSubmit('submitStep2')) {
                        try {
                            return $this->processStep2($step1_data, $assigns['categories']);
                        } catch (Exception $e) {
                            $content .= $this->displayError($e->getMessage());
                        }
                    }

                    $this->context->controller->addCSS($this->_path.'views/libraries/select2-3.5.0/select2.css');
                    $this->context->controller->addCSS($this->_path.'views/libraries/select2-3.5.0/select2-bootstrap.css');

                    $this->context->controller->addJS($this->_path.'views/libraries/select2-3.5.0/select2.min.js');
                    $this->context->controller->addJS($this->_path.'views/libraries/select2-3.5.0/select2_locale_'.$this->context->language->iso_code.'.js');

                    $this->context->controller->addJS($this->_path.'views/js/step2.js');

                    $defined = array();
                    if (isset($assigns['categories']) && isset($assigns['categories'][$step1_data['language']])) {
                        $defined = $assigns['categories'][$step1_data['language']]['engines'][$this->engine];
                    }

                    return $content.$this->showStep2($step1_data, $defined);
            }
        } else {
            $this->context->controller->addJS($this->_path.'views/js/script.js');
            $this->context->controller->addCSS($this->_path.'views/css/styles.css');

            if (Tools::substr(_PS_VERSION_, 0, 3) === '1.5') {
                $this->context->controller->addCSS($this->_path.'views/css/styles-1.5.css');
            }

            $content = '';
            if (!is_dir(_PS_CACHE_DIR_) || !is_writable(_PS_CACHE_DIR_)) {
                $content .= $this->displayError($this->l('Your /cache/ folder must be writeable to make this module to work correctly.'));
            }

            if (!ini_get('allow_url_fopen')) {
                $content .= $this->displayError($this->l('You need to have "allow_url_fopen" to on for this module to work.'));
            }

            if (Tools::isSubmit('section')) {
                $this->deleteAllGeneratedFiles();

                switch (Tools::getValue('section')) {
                    case 'options':
                        Configuration::updateValue(self::$basename.'_shop_id', Tools::getValue('shop_id'));
                        Configuration::updateValue(self::$basename.'_export_stock', (Tools::getValue('export_stock') === 'true'));
                        Configuration::updateValue(self::$basename.'_export_no_description', (Tools::getValue('export_no_description') === 'true'));
                        Configuration::updateValue(self::$basename.'_export_no_ean13', (Tools::getValue('export_no_ean13') === 'true'));
                        Configuration::updateValue(self::$basename.'_export_no_brand', (Tools::getValue('export_no_brand') === 'true'));
                        Configuration::updateValue(self::$basename.'_export_combination', (Tools::getValue('export_combination') === 'true'));

                        Configuration::updateValue(self::$basename.'_stock_state', Tools::getValue('stock_state'));
                        Configuration::updateValue(self::$basename.'_description_size', Tools::getValue('description_size'));
                        Configuration::updateValue(self::$basename.'_product_condition', Tools::getValue('product_condition'));
                        Configuration::updateValue(self::$basename.'_cache_duration', Tools::getValue('cache_duration'));

                        Configuration::updateValue(self::$basename.'_image_size', Tools::getValue('image_size'));
                        break;
                    case 'attributes':
                        if (Tools::getValue('include_color') === 'true') {
                            Configuration::updateValue(self::$basename.'_include_color', true);
                            Configuration::updateValue(self::$basename.'_include_color_attr', Tools::getValue('include_color_attr'));
                        } else {
                            Configuration::updateValue(self::$basename.'_include_color', false);
                            Configuration::updateValue(self::$basename.'_include_color_attr', null);
                        }

                        if (Tools::getValue('include_size') === 'true') {
                            Configuration::updateValue(self::$basename.'_include_size', true);
                            Configuration::updateValue(self::$basename.'_include_size_attr', Tools::getValue('include_size_attr'));
                        } else {
                            Configuration::updateValue(self::$basename.'_include_size', false);
                            Configuration::updateValue(self::$basename.'_include_size_attr', null);
                        }

                        if (Tools::getValue('include_pattern') === 'true') {
                            Configuration::updateValue(self::$basename.'_include_pattern', true);
                            Configuration::updateValue(self::$basename.'_include_pattern_attr', Tools::getValue('include_pattern_attr'));
                        } else {
                            Configuration::updateValue(self::$basename.'_include_pattern', false);
                            Configuration::updateValue(self::$basename.'_include_pattern_attr', null);
                        }

                        if (Tools::getValue('include_gender') === 'true') {
                            Configuration::updateValue(self::$basename.'_include_gender', true);
                            Configuration::updateValue(self::$basename.'_include_gender_attr', Tools::getValue('include_gender_attr'));
                        } else {
                            Configuration::updateValue(self::$basename.'_include_gender', false);
                            Configuration::updateValue(self::$basename.'_include_gender_attr', null);
                        }

                        if (Tools::getValue('include_material') === 'true') {
                            Configuration::updateValue(self::$basename.'_include_material', true);
                            Configuration::updateValue(self::$basename.'_include_material_attr', Tools::getValue('include_material_attr'));
                        } else {
                            Configuration::updateValue(self::$basename.'_include_material', false);
                            Configuration::updateValue(self::$basename.'_include_material_attr', null);
                        }

                        if (Tools::getValue('include_agegrp') === 'true') {
                            Configuration::updateValue(self::$basename.'_include_agegrp', true);
                            Configuration::updateValue(self::$basename.'_include_agegrp_attr', Tools::getValue('include_agegrp_attr'));
                        } else {
                            Configuration::updateValue(self::$basename.'_include_agegrp', false);
                            Configuration::updateValue(self::$basename.'_include_agegrp_attr', null);
                        }

                        break;
                    case 'token':
                        Configuration::updateValue(self::$basename.'_secure_token', uniqid());
                        break;
                    case 'exclude':
                        $ids = Tools::getValue('exclude_ids');
                        $ids = str_replace("\n", ',', $ids);
                        $ids = str_replace("\r", ',', $ids);
                        $ids = str_replace(',,', ',', $ids);
                        $ids = str_replace(' ', '', $ids);
                        Configuration::updateValue(self::$basename.'_exclude_ids', $ids);
                }

                $content .= $this->displayConfirmation($this->l('Your settings has been updated.'));

                // Reload settings:
                $settings = Configuration::getMultiple(array (
                    self::$basename.'_export_stock', self::$basename.'_stock_state', self::$basename.'_export_no_description', self::$basename.'_shop_id',
                    self::$basename.'_export_combination', self::$basename.'_export_no_ean13', self::$basename.'_export_no_brand', self::$basename.'_description_size',
                    self::$basename.'_product_condition', self::$basename.'_include_color', self::$basename.'_include_color_attr',
                    self::$basename.'_include_size', self::$basename.'_include_gender', self::$basename.'_include_gender_attr',
                    self::$basename.'_include_material', self::$basename.'_include_material_attr', self::$basename.'_include_agegrp', self::$basename.'_include_agegrp_attr',
                    self::$basename.'_include_size_attr', self::$basename.'_include_pattern', self::$basename.'_include_pattern_attr',
                    self::$basename.'_image_size', self::$basename.'_exclude_ids',

                    self::$basename.'_cache_duration', self::$basename.'_categories', self::$basename.'_secure_token'
                ));

                foreach ($settings as $key => $value) {
                    $assigns[Tools::substr($key, Tools::strlen(self::$basename) + 1)] = $value;
                }

                $assigns['categories'] = unserialize($assigns['categories']);
            }

            $assigns['exclude_ids'] = implode("\r\n", explode(',', $assigns['exclude_ids']));

            $assigns['image_folder'] = $this->_path.'img/';

            $attributes = array ();
            foreach (AttributeGroup::getAttributesGroups($this->context->language->id) as $attribute) {
                $attributes[] = array (
                    'id' => 'a_'.$attribute['id_attribute_group'],
                    'name' => $attribute['name']
                );
            }

            $features = array();
            foreach (Feature::getFeatures($this->context->language->id) as $feature) {
                $features[] = array (
                    'id' => 'f_'.$feature['id_feature'],
                    'name' => $feature['name']
                );
            }

            $assigns['attributes'] = $attributes;
            $assigns['features'] = $features;

            $assigns['can_add'] = (count($assigns['categories']) < count($assigns['languages']));

            $imageTypes = ImageTypeCore::getImagesTypes('products');
            $image_sizes = array();
            foreach ($imageTypes as $type) {
                $image_sizes[] = array(
                    'id' => $type['name'],
                    'name' => $type['width'].' x '.$type['height'].' ('.$type['name'].')'
                );
            }
            $assigns['image_sizes'] = $image_sizes;

            $shops = array();
            if (Shop::getContext() === Shop::CONTEXT_SHOP) {
                $shops[] = array (
                    'id'   => Context::getContext()->shop->id,
                    'name' => Context::getContext()->shop->name
                );
            } else {
                foreach (Shop::getShops(true) as $shop) {
                    $shops[] = array (
                        'id'   => $shop['id_shop'],
                        'name' => $shop['name']
                    );
                }
            }
            $assigns['shops'] = $shops;

            $assigns['base_url'] = _PS_BASE_URL_.$this->_path;
            $assigns['base_path'] = dirname(__FILE__).'/';
            $assigns['module_id'] = $this->module_id;
            $assigns['configure_url'] = $this->context->link->getAdminLink('AdminModules').'&configure='.$this->name;

            $this->context->smarty->assign($assigns);
            $content .= $this->display(__FILE__, 'views/templates/admin/settings.tpl');

            return $content;
        }
    }

    public function hookDisplayOrderConfirmation($params)
    {
        $order = $params['order'];
        $products = $order->getProducts();
        if (count($products) === 0) {
            return null;
        }

        $export_combination = (Configuration::get(self::$basename.'_export_combination') === '1');
        $this->tracker_details['merchantInfo'] = array (
            array (
                'country' => $this->context->country->iso_code,
                'merchantId' => Configuration::get(self::$basename.'_kelkoo_shop_id')
            )
        );

        $this->tracker_details['orderValue'] = round($order->total_paid_tax_excl, 3);
        $this->tracker_details['orderId'] = $order->id;
        $this->tracker_details['basket'] = array();

        foreach ($products as $product) {
            $exporting_id = (int)$product['product_id'];
            if ($export_combination) {
                $exporting_id = (is_null($product['product_attribute_id']) ? (int)$product['product_id'] : $product['product_id'].'-'.$product['product_attribute_id']);
            }

            $this->tracker_details['basket'][] = array (
                'productname' => $product['product_name'],
                'productid' => $exporting_id,
                'quantity' => $product['product_quantity'],
                'price' => round($product['unit_price_tax_excl'], 3)
            );
        }
    }

    /**
     * hook footer to load JS script for standards actions such as product clicks
     */
    public function hookdisplayFooter()
    {
        if (is_null($this->tracker_details)) {
            return null;
        }
        
        return '<script type="text/javascript">
            window._kkstrack = '.json_encode($this->tracker_details).';
            (function() {
            var s = document.createElement("script");
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://s.kk-resources.com/ks.js";
            var x = document.getElementsByTagName("script")[0];
            x.parentNode.insertBefore(s, x);
            })();
        </script>';
    }
}
