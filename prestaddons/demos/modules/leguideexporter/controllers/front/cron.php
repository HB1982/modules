<?php
/**
 * LeGuide Exporter
 *
 * This modules export your products catalog
 *
 * If you find errors, bugs or if you want to share some improvments, feel free to contact at contact@prestaddons.net ! :)
 * Si vous trouvez des erreurs, des bugs ou si vous souhaitez tout simplement partager un conseil ou une amélioration,
 * n'hésitez pas à me contacter à contact@prestaddons.net
 *
 * @author    Cyril Nicodème <contact@prestaddons.net>
 * @copyright Copyright (C) June 2014 cnicodeme.com <@email:contact@prestaddons.net>. All rights reserved.
 * @license   Nicodème Cyril
 * @version   2.6.0
 * @since     2014-06-18
 * @package   modules
 */

require_once(dirname(__FILE__).'/../../generator.php');
class leguideexportercronModuleFrontController extends ModuleFrontController
{
    /** @var bool If set to true, will be redirected to authentication page */
    public $auth = false;

    /** @var bool */
    public $ajax;

    public function display()
    {
        $this->ajax = 1;
        $generator = new ShoppingExporterGenerator();

        if (!$generator->isValidToken()) {
            exit('Invalid token.');
        }

        if (php_sapi_name() !== 'cli') {
            @ini_set('max_execution_time', 300); /* Should suffice; */
            @set_time_limit(300);
        }

        $cookie = new Cookie('psAdmin', '', (int)Configuration::get('PS_COOKIE_LIFETIME_BO'));
        Context::getContext()->employee = new Employee((int)$cookie->id_employee);

        $generator->generateAll();
        exit(0);
    }
}
