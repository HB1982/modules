Installation
============

The installation of this module is pretty easy.
All you have to do is to go in your modules listing in your Prestashop backoffice, click in "Add a new module", select the archive and voilà ! :)

Once installed, you will have to configure it.
Normally, you should be redirected to the configuration page of this module, but in the case it isn't,
go to Modules, search for "LeGuide Exporter" and click on the "Configure link".

Configuration
=============

The configuration page appears with multiple Tabs.
Each tabs represent a aspect of the configuration. We will go through every tabs.


General
-------

This tab list the urls to copy/paste your product catalog to the comparison websites.

After the first installation, this tab should be quite empty.
You will have to configure this module in order to have this tab show more informations.

You have to go to in "Categories" in order to define which categories to export.
Once you have done this configuration, this tab will show the links where to export your feed.


Categories and configuration
----------------------------

This is the most complicated but most interesting section.
It's where you will have to define which categories you want to export to.
You will also have to indicate into which category you want to map in the engine you selected.

We understand this is a fastidious work, but it comes with a great reward :
	your products will be better categorized in the comparisons websites thus making more and better sales !

This section also let you define which currency.


Options
-------

In this tab, you will have the possibility to specify which products to export, like excluding products without description, reference, brand, etc.
You will also have the possibility to specifiy the default carrier, in order to let our module calculate the shipping price of your product correctly.


Cron
----

Our module comes with a script that will generate all the files in the Prestashop cache in order to reduce the charge of your server when the comparison engine hit the url of your feeds products.
This tab is here to explain to you how it works and why you should use it.

We highly recommand you to implement a CRON job for this module because it will clearly improve the overall performance.


Contact
=======

You find a bug, you have some problems configuring/installing/making this module works?
Feel free to contact me via Prestashop : https://addons.prestashop.com/en/write-to-developper?alternative=1&id_product=17630

I'll do my best to help you.


Thanks !
========

You purchased my module, and for that, I wanted to thank you !
I hope you'll find what you need.
