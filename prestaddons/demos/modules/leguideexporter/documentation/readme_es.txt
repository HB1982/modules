Instalación
============

La instalación de este módulo es bastante fácil.
Todo lo que tienes que hacer es ir en su perfil módulos en su backoffice Prestashop, haga clic en "Agregar un nuevo módulo", seleccione el archivo y voilà! :)

Una vez instalado, usted tendrá que configurarlo.
Normalmente, usted debe ser redirigido a la página de configuración de este módulo, pero en el caso no lo es,
ir a los módulos, busque "Leguide Exporter" y haga clic en el "vínculo Configurar".

Configuración
=============

Aparece la página de configuración con múltiples pestañas.
Cada pestañas representan un aspecto de la configuración. Vamos a ir a través de cada pestañas.


General
-------

Esta ficha enumera las URL que copiar / pegar su catálogo de productos a los sitios web de comparación.

Después de la primera instalación, esta ficha debe ser bastante vacío.
Usted tendrá que configurar este módulo con el fin de tener esta pestaña mostrar más información.

Tienes que ir en "Categorías" para definir las categorías de exportar.
Una vez que haya hecho esta configuración, esta ficha mostrará los enlaces donde exportar su alimentación.


Categorías y configuración
----------------------------

Esta es la sección más complicado pero más interesante.
Es el lugar donde usted tendrá que definir qué categorías desea exportar.
Usted también tendrá que indicar en qué categoría desea asignar en el motor que ha seleccionado.

Entendemos que este es un trabajo exigente, pero viene con una gran recompensa:
sus productos se clasifican mejor en las comparaciones sitios web haciendo así más y mejores ventas!

Esta sección también le permite definir qué moneda.


Opciones
-------

En esta pestaña, usted tendrá la posibilidad de especificar los productos de exportación, como la exclusión de los productos sin descripción, referencia, marca, etc.
También tendrá la posibilidad de specifiy el operador por defecto, con el fin de dejar que nuestro módulo calcular el precio del envío de su producto correctamente.


Cron
----

Nuestro módulo viene con un script que va a generar todos los archivos en la memoria caché de Prestashop con el fin de reducir la carga de su servidor cuando el comparador golpeó la url de sus productos alimenta.
Esta ficha está aquí para explicar a usted cómo funciona y por qué usted debe usarlo.

Estamos altamente recommand a implementar un trabajo cron para este módulo porque mejorará claramente el rendimiento general.


Contacto
=======

Usted encuentra un error, usted tiene algunos problemas de configurar / instalar / hacer este módulo obras?
No dude en ponerse en contacto conmigo a través de Prestashop: https://addons.prestashop.com/en/write-to-developper?alternative=1&id_product=17630

Voy a hacer mi mejor esfuerzo para ayudarle.


Gracias !
========

Usted compró mi módulo, y para eso, yo quería darle las gracias!
Espero que encuentre lo que necesita.
