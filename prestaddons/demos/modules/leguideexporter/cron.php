<?php
/**
 * Shopping Exporter
 *
 * This modules export your products catalog.
 *
 * If you find errors, bugs or if you want to share some improvments, feel free to contact at contact@prestaddons.net ! :)
 * Si vous trouvez des erreurs, des bugs ou si vous souhaitez tout simplement partager un conseil ou une amélioration,
 * n'hésitez pas à me contacter à contact@prestaddons.net
 *
 * @author    Cyril Nicodème <contact@prestaddons.net>
 * @copyright Copyright (C) June 2014 cnicodeme.com <@email:contact@prestaddons.net>. All rights reserved.
 * @license   Nicodème Cyril
 * @version   2.6.0
 * @since     2014-06-18
 * @package   modules
 */

error_reporting(error_reporting() & ~E_NOTICE);

$_GET['fc'] = 'module';
$_GET['module'] = 'leguideexporter';
$_GET['controller'] = 'cron';

require_once(dirname(__FILE__) . '/../../index.php');
exit(0);