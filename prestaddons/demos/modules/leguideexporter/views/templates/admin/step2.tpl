{**
 * @package   Shopping Exporter
 * @author    Cyril Nicodème <contact@prestaddons.net>
 * @copyright Copyright (C) June 2014 cnicodeme.com <@email:contact@prestaddons.net>. All rights reserved.
 * @since     2014-06-18
 * @version   2.6.0
 * @license   Nicodème Cyril
 *}

 <script>
    var exporter_taxonomies = {$mapping};{* HTML CONTENT *}
 </script>

 <div>
     <p>{l s='In order to optimize your visibility in the price engine, you have to map your selected categories, to the categories of the price engine.' mod='leguideexporter'}</p>
 </div>

<form method="post" action="">
    <div class="panel">
        <table class="table table-hover table-step2" data-placeholder="{l s='Search for a category to map' mod='leguideexporter'}">
            <thead>
                <tr>
                    <th>{l s='Category name' mod='leguideexporter'}</th>
                    <th>{l s='Mapping' mod='leguideexporter'}</th>
                </tr>
            </thead>
            <tbody>
                {foreach $categories as $category}
                <tr>
                    <td class="col-sm-3">{$category['name']|escape:'html':'UTF-8'}</td>
                    <td class="col-sm-9"><input type="hidden" name="mapping_{$category['category_id']|escape:'html':'UTF-8'}"{if $category['value'] != null} value="{$category['value']|escape:'quotes':'UTF-8'}"{/if} style="width: 100%"></td>
                </tr>
                {/foreach}
            </tbody>
        </table>

        <input type="hidden" name="currency_id" value="{$form_data['currency_id']|escape:'html':'UTF-8'}" />
        <input type="hidden" name="carrier_id" value="{$form_data['carrier_id']|escape:'html':'UTF-8'}" />
        <input type="hidden" name="delivery_schedule" value="{$form_data['delivery_schedule']|escape:'html':'UTF-8'}" />
        <input type="hidden" name="language" value="{$form_data['language']|escape:'html':'UTF-8'}" />
        <input type="hidden" name="categories" value="{$form_data['categories']|escape:'htmlall':'UTF-8'}" />

        <div class="panel-footer">
            <button type="submit" value="1" name="submitStep2" class="btn btn-default pull-right">
                <i class="process-icon-save"></i> Enregistrer
            </button>
        </div>
    </div>
</form>
