-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : jeu. 07 juil. 2022 à 15:59
-- Version du serveur :  10.5.16-MariaDB
-- Version de PHP : 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `sc2vgfy4843_prestaddons`
--

-- --------------------------------------------------------

--
-- Structure de la table `ps_pfg_fields`
--

CREATE TABLE `ps_pfg_fields` (
  `id_field` int(11) NOT NULL,
  `id_pfg` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `required` tinyint(1) NOT NULL,
  `classname` varchar(255) DEFAULT NULL,
  `style` varchar(255) DEFAULT NULL,
  `extra` varchar(255) DEFAULT NULL,
  `related` enum('email','subject','newsletter') DEFAULT NULL,
  `position` tinyint(4) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ps_pfg_fields`
--

INSERT INTO `ps_pfg_fields` (`id_field`, `id_pfg`, `type`, `name`, `required`, `classname`, `style`, `extra`, `related`, `position`) VALUES
(1, 1, 'text', 'nom', 0, '', '', '', '', 1),
(2, 1, 'text', 'prenom', 1, '', '', '', '', 2),
(3, 1, 'textarea', 'message', 0, '', '', '', 'email', 3),
(4, 1, 'email', 'email', 0, '', '', '', 'email', 4),
(5, 1, 'destinataires', 'destination', 1, '', '', '', '', 5),
(6, 1, 'multicheckbox', 'test', 1, '', '', '', '', 6),
(7, 2, 'text', 'nom', 0, '', '', '', '', 1),
(8, 2, 'textarea', 'message', 0, '', '', '', '', 2),
(9, 2, 'email', 'mail', 0, '', '', '', '', 3);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `ps_pfg_fields`
--
ALTER TABLE `ps_pfg_fields`
  ADD PRIMARY KEY (`id_field`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `ps_pfg_fields`
--
ALTER TABLE `ps_pfg_fields`
  MODIFY `id_field` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
