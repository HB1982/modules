
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">

        <title>We create awesome Prestashop modules | Prestaddons</title>
        <meta name="twitter:title" content="We create awesome Prestashop modules | Prestaddons">
        <meta name="og:title" content="We create awesome Prestashop modules | Prestaddons">
        <meta name="apple-mobile-web-app-title" content="We create awesome Prestashop modules | Prestaddons">

        <meta name="description" content="Prestaddons is an organization that builds Prestashop module tailored to your need as an E-Commerce seller. Trust us on building awesome module that will suit your shop perfectly and increase your sales!">
        <meta name="og:description" content="Prestaddons is an organization that builds Prestashop module tailored to your need as an E-Commerce seller. Trust us on building awesome module that will suit your shop perfectly and increase your sales!">
        <meta data-hid="twitter:description" name="twitter:description" content="Prestaddons is an organization that builds Prestashop module tailored to your need as an E-Commerce seller. Trust us on building awesome module that will suit your shop perfectly and increase your sales!">

        <meta name="author" content="Prestaddons">
        <meta name="og:site_name" content="Prestaddons">
        <meta name="og:locale" content="en_US">
        <meta name="og:url" content="https://prestaddons.net">

        <link href="https://fonts.googleapis.com/css2?family=Playfair+Display:ital@1&family=Raleway&family=Roboto&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="./styles.css">
    </head>
    <body>
        <div class="container">
            <header class="fade-in delay-1">
                <h1>
                    Prestaddons
                </h1>
            </header>
            <main>
                <div class="row description">
                    <p class="fade-in delay-1 col-6 col-offset-3">
                        We build and sell Prestashop modules tailored to your needs as an E-Commerce seller.
                        You can rely on us on creating modules that will suit your shop perfectly, give you more free time to focus on what matter, and increase your sales!
                    </p>
                </div>
                <h2 class="fade-in delay-2">Our users thinks the best of us!</h2>
                <section class="comments fade-in delay-2">
                    <ul class="row">
                        <li class="col-6"><div><p>The developer was very quick and professional in providing support and setting up the module. Highly recommend this product!</p></div></li>
                        <li class="col-6"><div><p>One of the best support I have received since I buy modules on Addons. I highly recommend this seller!!!</p></div></li>
                        <li class="col-6"><div><p>A customer service inbelievable, a serious person that take care of his modules. I highly recommend this.</p></div></li>
                        <li class="col-6"><div><p>Great module and awesome developer, available and ready to help, and mostly highly reactive. You can buy his modules and trust him with your eyes shut.</p></div></li>
                        <li class="col-6"><div><p>Very good modul and fantastic support! Fast and competent! Many thanks for all!</p></div></li>
                        <li class="col-6"><div><p>Excellent module and developer support - highly recommended.</p></div></li>
                    </ul>
                </section>
                <h2 class="fade-in delay-3">Our modules</h2>
                <section class="modules row fade-in delay-3">
                    <article class="col-6">
                        <div>
                            <img src="img/pfg.png" />
                            <div>
                                <h3><a href="https://addons.prestashop.com/en/product.php?id_product=16685">Powerful Form Generator</a></h3>
                                <p>
                                    Implement any kind of form in your website at ease and quickly.<br />
                                    Works with form contact, documentation requesting, survey, anything you want!!
                                </p>
                                <div class="rating">
                                    <ul class="stars star45">
                                        <li><svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"/></svg></li>
                                        <li><svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"/></svg></li>
                                        <li><svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"/></svg></li>
                                        <li><svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"/></svg></li>
                                        <li><svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1250 957l257-250-356-52-66-10-30-60-159-322v963l59 31 318 168-60-355-12-66zm452-262l-363 354 86 500q5 33-6 51.5t-34 18.5q-17 0-40-12l-449-236-449 236q-23 12-40 12-23 0-34-18.5t-6-51.5l86-500-364-354q-32-32-23-59.5t54-34.5l502-73 225-455q20-41 49-41 28 0 49 41l225 455 502 73q45 7 54 34.5t-24 59.5z"/></svg></li>
                                    </ul>
                                    <span class="rates">102 rates</span>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="col-6">
                        <div>
                            <img src="img/tse.png" />
                            <div>
                                <h3><a href="https://addons.prestashop.com/en/product.php?id_product=17163">Total Shopping Exporter</a></h3>
                                <p>
                                    Easily export your products catalog to many comparison websites including Google Shopping, Bing Shopping, LeGuide and many others.
                                </p>
                                <div class="rating">
                                    <ul class="stars star45">
                                        <li><svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"/></svg></li>
                                        <li><svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"/></svg></li>
                                        <li><svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"/></svg></li>
                                        <li><svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"/></svg></li>
                                        <li><svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1250 957l257-250-356-52-66-10-30-60-159-322v963l59 31 318 168-60-355-12-66zm452-262l-363 354 86 500q5 33-6 51.5t-34 18.5q-17 0-40-12l-449-236-449 236q-23 12-40 12-23 0-34-18.5t-6-51.5l86-500-364-354q-32-32-23-59.5t54-34.5l502-73 225-455q20-41 49-41 28 0 49 41l225 455 502 73q45 7 54 34.5t-24 59.5z"/></svg></li>
                                    </ul>
                                    <span class="rates">10 rates</span>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="col-6">
                        <div>
                            <img src="img/gse.png" />
                            <div>
                                <h3><a href="https://addons.prestashop.com/en/product.php?id_product=17627">Google Shopping Exporter</a></h3>
                                <p>
                                    Easily export your products catalog to the Google Shopping comparison website.
                                </p>
                                <div class="rating">
                                    <ul class="stars star4">
                                        <li><svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"/></svg></li>
                                        <li><svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"/></svg></li>
                                        <li><svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"/></svg></li>
                                        <li><svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"/></svg></li>
                                        <li><svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1201 1004l306-297-422-62-189-382-189 382-422 62 306 297-73 421 378-199 377 199zm527-357q0 22-26 48l-363 354 86 500q1 7 1 20 0 50-41 50-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"/></svg></li>
                                    </ul>
                                    <span class="rates">12 rates</span>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="col-6">
                        <div>
                            <img src="img/lse.png" />
                            <div>
                                <h3><a href="https://addons.prestashop.com/en/product.php?id_product=17630">LeGuide Shopping Exporter</a></h3>
                                <p>
                                    Easily export your products catalog to LeGuide comparison website.
                                </p>
                                <div class="rating">
                                    <ul class="stars star45">
                                        <li><svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"/></svg></li>
                                        <li><svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"/></svg></li>
                                        <li><svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"/></svg></li>
                                        <li><svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"/></svg></li>
                                        <li><svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1250 957l257-250-356-52-66-10-30-60-159-322v963l59 31 318 168-60-355-12-66zm452-262l-363 354 86 500q5 33-6 51.5t-34 18.5q-17 0-40-12l-449-236-449 236q-23 12-40 12-23 0-34-18.5t-6-51.5l86-500-364-354q-32-32-23-59.5t54-34.5l502-73 225-455q20-41 49-41 28 0 49 41l225 455 502 73q45 7 54 34.5t-24 59.5z"/></svg></li>
                                    </ul>
                                    <span class="rates">3 rates</span>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="col-6 col-offset-3">
                        <div>
                            <img src="img/ppc.png" />
                            <div>
                                <h3><a href="https://addons.prestashop.com/en/product.php?id_product=17761">Powerful Product Contact</a></h3>
                                <p>
                                    Let your customers contact you for any questions by filling an editable form on any product page you want.
                                </p>
                                <div class="rating">
                                    <ul class="stars star45">
                                        <li><svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"/></svg></li>
                                        <li><svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"/></svg></li>
                                        <li><svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"/></svg></li>
                                        <li><svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"/></svg></li>
                                        <li><svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1250 957l257-250-356-52-66-10-30-60-159-322v963l59 31 318 168-60-355-12-66zm452-262l-363 354 86 500q5 33-6 51.5t-34 18.5q-17 0-40-12l-449-236-449 236q-23 12-40 12-23 0-34-18.5t-6-51.5l86-500-364-354q-32-32-23-59.5t54-34.5l502-73 225-455q20-41 49-41 28 0 49 41l225 455 502 73q45 7 54 34.5t-24 59.5z"/></svg></li>
                                    </ul>
                                    <span class="rates">11 rates</span>
                                </div>
                            </div>
                        </div>
                    </article>
                </section>
            </main>
            <footer class="fade-in delay-4">
                <p>Don't hesitate to reach out to us at <a href="mailto:contact@prestaddons.net">contact@prestaddons.net</a> for any questions!</p>
            </footer>
        </div>
    </body>
</html>

<style>
    
    
/* http://meyerweb.com/eric/tools/css/reset/ 
   v2.0 | 20110126
   License: none (public domain)
*/

html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed, 
figure, figcaption, footer, header, hgroup, 
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
  margin: 0;
  padding: 0;
  border: 0;
  font-size: 100%;
  font: inherit;
  vertical-align: baseline;
}
/* HTML5 display-role reset for older browsers */
article, aside, details, figcaption, figure, 
footer, header, hgroup, menu, nav, section {
  display: block;
}
body {
  line-height: 1;
}
ol, ul {
  list-style: none;
}
blockquote, q {
  quotes: none;
}
blockquote:before, blockquote:after,
q:before, q:after {
  content: '';
  content: none;
}
table {
  border-collapse: collapse;
  border-spacing: 0;
}

strong { font-weight: 600 }
em { font-style: italic }

h1 {
    font-family: 'Roboto', sans-serif;
}

/* General base
–––––––––––––––––––––––––––––––––––––––––––––––––– */
html { box-sizing: border-box; font-size: 62.5%; }
*, *:before, *:after { box-sizing: inherit }

body {
    font-size: 1.5em; /* currently ems cause chrome bug misinterpreting rems on body element */
    line-height: 1.6;
    font-weight: 400;
    font-family: "Raleway", system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Lato, "Helvetica Neue", Helvetica, Arial, sans-serif;
    color: #444;
    letter-spacing: 0;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    -webkit-tap-highlight-color: transparent;
    text-rendering: optimizeLegibility;
    font-kerning: normal;
    font-variant-ligatures: common-ligatures, contextual;
    -moz-font-feature-settings: "kern", "liga", "clig", "calt";
    -ms-font-feature-settings: "kern", "liga", "clig", "alt";
    -webkit-font-feature-settings: "kern", "liga", "clig", "calt";
    font-feature-settings: "kern", "liga", "clig", "calt";
}

h1, h2, h3, h4, h5, h6 { margin-top: 0; margin-bottom: 2rem; font-weight: 300; }
h1 { font-size: 4.0rem; line-height: 1.2;  letter-spacing: -.1rem; }
h2 { font-size: 3.6rem; line-height: 1.25; letter-spacing: -.1rem; }
h3 { font-size: 3.0rem; line-height: 1.3;  letter-spacing: -.1rem; }
h4 { font-size: 2.4rem; line-height: 1.35; letter-spacing: -.08rem; }
h5 { font-size: 1.8rem; line-height: 1.5;  letter-spacing: -.05rem; }
h6 { font-size: 1.5rem; line-height: 1.6;  letter-spacing: 0; }

/* Larger than phablet */
@media (min-width: 550px) {
    h1 { font-size: 5.0rem; }
    h2 { font-size: 4.2rem; }
    h3 { font-size: 3.6rem; }
    h4 { font-size: 3.0rem; }
    h5 { font-size: 2.4rem; }
    h6 { font-size: 1.5rem; }
}

p { margin-top: 0; }

ol, ul { padding-left: 0; margin-top: 0; }
ul ul, ul ol, ol ol, ol ul { margin: 1.5rem 0 1.5rem 3rem; }
pre, blockquote, dl, figure, table, p, ul, ol { margin-bottom: 2.5rem; }

.container {
    width: 120rem;
    margin-right: auto;
    margin-left: auto;
}

.row {
    box-sizing: border-box;
    display: flex;
    flex: 0 1 auto;
    flex-direction: row;
    flex-wrap: wrap;
}

.row .col-1, .row .col-2, .row .col-3, .row .col-4, .row .col-5, .row .col-6, .row .col-7, .row .col-8, .row .col-9,
.row .col-10, .row .col-11, .row .col-12, .row .col-offset-0, .row .col-offset-1, .row .col-offset-2,
.row .col-offset-3, .row .col-offset-4, .row .col-offset-5, .row .col-offset-6, .row .col-offset-7,
.row .col-offset-8, .row .col-offset-9, .row .col-offset-10, .row .col-offset-11, .row .col-offset-12 {
    box-sizing: border-box;
    flex: 0 0 auto;
    padding-right: 1rem;
    padding-left: 1rem;
}

.row .col-1 { flex-basis: 8.33333333%; max-width: 8.33333333%; }
.row .col-2 { flex-basis: 16.66666667%; max-width: 16.66666667%; }
.row .col-3 { flex-basis: 25%; max-width: 25%; }
.row .col-4 { flex-basis: 33.33333333%; max-width: 33.33333333%; }
.row .col-5 { flex-basis: 41.66666667%; max-width: 41.66666667%; }
.row .col-6 { flex-basis: 50%; max-width: 50%; }
.row .col-7 { flex-basis: 58.33333333%; max-width: 58.33333333%; }
.row .col-8 { flex-basis: 66.66666667%; max-width: 66.66666667%; }
.row .col-9 { flex-basis: 75%; max-width: 75%; }
.row .col-10 { flex-basis: 83.33333333%; max-width: 83.33333333%; }
.row .col-11 { flex-basis: 91.66666667%; max-width: 91.66666667%; }
.row .col-12 { flex-basis: 100%; max-width: 100%; }

.row .col-offset-0 { margin-left: 0; }
.row .col-offset-1 { margin-left: 8.33333333%; }
.row .col-offset-2 { margin-left: 16.66666667%; }
.row .col-offset-3 { margin-left: 25%; }
.row .col-offset-4 { margin-left: 33.33333333%; }
.row .col-offset-5 { margin-left: 41.66666667%; }
.row .col-offset-6 { margin-left: 50%; }
.row .col-offset-7 { margin-left: 58.33333333%; }
.row .col-offset-8 { margin-left: 66.66666667%; }
.row .col-offset-9 { margin-left: 75%; }
.row .col-offset-10 { margin-left: 83.33333333%; }
.row .col-offset-11 { margin-left: 91.66666667%; }
.row .offset-center { justify-content: center; justify-self: center }

.text-right { text-align: right }
.text-center { text-align: center }

a {
    color: #231768;
    text-decoration: none;
    outline: none;
    border-bottom: solid 1px transparent;
    transition: border ease-in-out 250ms;
}

a:hover {
    border-bottom: solid 1px #ff0076
}

@keyframes fadeInFromNone {
    from {
        opacity: 0;
        transform: translate(-30px, 0px);
    }
    to {
        opacity: 1;
        transform: translate(0px, 0px);
    }
}

.fade-in {
    animation-name: fadeInFromNone;
    animation-fill-mode: forwards;
    animation-iteration-count: 1;
    animation-timing-function: ease-in-out;
    animation-duration: 0.5s;
    transform: translate(0px, 0px);
    opacity: 0;
}

.fade-in.from-top { animation-name: fadeInTop; }
.fade-in.from-bottom { animation-name: fadeInBottom; }
.fade-in.from-left { animation-name: fadeInLeft; }
.fade-in.from-right { animation-name: fadeInRight; }

.fade-in.delay-1 {animation-delay: .25s;}
.fade-in.delay-2 {animation-delay: .5s;}
.fade-in.delay-3 {animation-delay: .75s;}
.fade-in.delay-4 {animation-delay: 1s;}
.fade-in.delay-5 {animation-delay: 1.25s;}


/* Website
–––––––––––––––––––––––––––––––––––––––––––––––––– */

header {
    text-align: center;
    margin: 50px 0 50px;
    color: #231768
}

header h1 small {
    display: block;
    font-size: .5em;
    font-style: italic;
}

.container .description>p {
    color: #231768
}

.container h2 {
    margin-top: 80px;
    padding-bottom: 10px;
    margin-bottom: 20px;
    border-bottom: solid 1px #eee;
    font-family: 'Roboto', sans-serif;
    color: #888
}

.comments li {
    position: relative;
}

.comments li>div {
    margin: 20px;
    padding: 20px;
    box-shadow: 5px 5px 10px rgba(0, 0, 0, 0.25);
    border-radius: 5px;
}

.comments p {
    font-family: 'Playfair Display', serif;
    color: #555;
    display: block;
    font-size: 18px;
    line-height: 1.25em;
    quotes: "“" "”";
    padding-left: 40px;
}

.comments p::before {
    color: #ff0076;
    content: open-quote;

    font-size: 80px;
    font-weight: bold;
    line-height: 40px;
    left: 40px;
    top: 40px;
    position: absolute;
    width: 90px;
}


.modules article {
    margin-bottom: 40px;
}

.modules article>div {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    box-shadow: 5px 5px 10px rgba(0, 0, 0, 0.25);
    border-radius: 5px;
    padding: 20px;
}

.modules article img {
    width: 64px;
    height: 64px;
}

.modules article>div>div {
    padding-left: 20px;
}

.modules article>div h3 {
    font-size: 3rem;
    margin: 0 0 10px;
    padding: 0;
}

.modules article .rating {
    display: flex;
    justify-content: space-between;
}

.modules article .rating .stars>li {
    list-style-type: none;
    display: inline
}

.modules article .rating .stars svg {
    display: inline;
    fill: #FFD700;
    width: 16px;
}


footer {
    text-align: center;
    font-style: italic;
    margin: 50px 0;
}
</style>